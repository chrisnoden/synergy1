<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  File
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */


// Load the composer autoloader
require_once('../vendor/autoload.php');

// Create a MonoLog logger to handle all log output (debug or above)
$logger = new \Monolog\Logger('my_logger');
$logger->pushHandler(new \Monolog\Handler\StreamHandler('/tmp/synergy.log', \Monolog\Logger::DEBUG));

// optional DI container
$c = new \ChrisNoden\Synergy\Construct\SynergyContainer();
$c->setLogger($logger);

/** @var \ChrisNoden\Synergy\Routing\Router $router */
$router = \ChrisNoden\Synergy\Routing\Router::createFromFile('routes.yml', $logger);
$request = new \ChrisNoden\Synergy\Request\WebRequest();
$request->setUri('https://www.test.com/my/path?link=here#part2');

$route = $router->match($request);

/** @var ChrisNoden\Synergy\View\Response $response */
$response = $route->launchAction();
$response->send();