#!/bin/sh

echo "Configuring packages"
wget -q http://dl.iuscommunity.org/pub/ius/stable/CentOS/6/x86_64/ius-release-1.0-13.ius.centos6.noarch.rpm 2>&1 >/dev/null
wget -q http://dl.iuscommunity.org/pub/ius/stable/CentOS/6/x86_64/epel-release-6-5.noarch.rpm 2>&1 >/dev/null
rpm --quiet -Uvh /home/vagrant/ius-release-1.0-13.ius.centos6.noarch.rpm /home/vagrant/epel-release-6-5.noarch.rpm
yum -y -q install yum-plugin-replace 2>&1 >/dev/null
yum -y -q install mysql 2>&1 >/dev/null
yum -y -q replace mysql --replace-with mysql56u 2>&1 >/dev/null
yum -y -q install gcc git libstdc++-devel gcc-c++ curl-devel telnet make bind-utils php55u php55u-common php55u-cli php55u-mbstring php55u-mcrypt php55u-mysqlnd php55u-opcache php55u-xml php55u-process php55u-pecl-jsonc php55u-pecl-xdebug php55u-pdo httpd httpd-devel httpd-tools subversion unzip mysql56u-devel mysql56u-server 2>&1 >/dev/null

curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin && ln -s /usr/local/bin/composer.phar /usr/local/bin/composer

sed -i -e 's/^memory_limit\ =\ .*/memory_limit = 256M/' /etc/php.ini

chkconfig mysqld on && service mysqld start
mysql -h localhost -u root -e "create database synergy;"
mysql -h localhost -u root -e "grant all privileges on *.* to 'root'@'%';"

cd /opt && vendor/bin/propel sql:insert --config-dir="src/Synergy/Tests" --sql-dir="src/Synergy/Tests/Api/sql" --connection="synergy=mysql:host=127.0.0.1;dbname=synergy;user=root;password="

echo "Finished"
