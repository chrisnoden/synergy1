<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://bitbucket.org/chrisnoden
 */

namespace ChrisNoden\Synergy\Routing;

use ChrisNoden\Synergy\Exception\SynergyFileNotExistsException;
use ChrisNoden\Synergy\Exception\SynergyFileNotReadableException;
use ChrisNoden\Synergy\Exception\SynergyInvalidRouteException;
use ChrisNoden\Synergy\Parser\YamlParser;
use ChrisNoden\Synergy\Request\RequestInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class Router
 * Maintains a collection of Route objects so we can match a Request to a Route easily
 * Can create the collection from a YML file or from an array
 *
 * @category ChrisNoden\Synergy\Web
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class Router
{
    use LoggerAwareTrait;

    /** @var array The individual routes */
    protected $routes = array();


    /**
     * Create a new Router object
     *
     * @param LoggerInterface $logger
     *
     * @return Router
     */
    public static function create(LoggerInterface $logger = null)
    {
        $router = new self();
        if (is_null($logger)) {
            $router->setLogger(new NullLogger());
        } else {
            $router->setLogger($logger);
        }
        return $router;
    }


    /**
     * Create a new Router from a file of route definitions (eg a Yaml file)
     *
     * @param $filename
     * @param LoggerInterface $logger
     *
     * @return Router
     */
    public static function createFromFile($filename, $logger = null)
    {
        $router = new self();
        if (is_null($logger)) {
            $router->setLogger(new NullLogger());
        } else {
            $router->setLogger($logger);
        }
        $router->addRoutesFromFile($filename);

        return $router;
    }


    /**
     * Load a file containing our routes
     *
     * @param string $filename
     *
     * @return $this
     * @throws \ChrisNoden\Synergy\Exception\SynergyFileNotReadableException
     * @throws \ChrisNoden\Synergy\Exception\SynergyFileNotExistsException
     */
    public function addRoutesFromFile($filename)
    {
        if (!file_exists($filename)) {
            throw new SynergyFileNotExistsException($filename . ' not found');
        } elseif (!is_readable($filename)) {
            throw new SynergyFileNotReadableException($filename . ' not readable');
        }

        $this->addRoutesFromArray(YamlParser::YAMLLoad($filename));

        return $this;
    }


    /**
     * Takes an array of routes and builds them into our Router
     *
     * @param array $routeCollection
     *
     * @return $this
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidRouteException
     */
    public function addRoutesFromArray(Array $routeCollection)
    {
        foreach ($routeCollection as $key => $data) {
            if ($data instanceof Route) {
                $this->addRoute($data);
            } elseif (is_array($data)) {
                $route = Route::build(array($key => $data), $this->logger);
                $route->setName($key);
                $this->addRoute($route);
            } else {
                throw new SynergyInvalidRouteException('routeCollection contains invalid entries');
            }
        }

        return $this;
    }


    /**
     * Add a route to the Router
     *
     * @param Route $route
     *
     * @return $this
     * @throws SynergyInvalidRouteException if the route already exists
     */
    public function addRoute(Route $route)
    {
        $name = $route->getName();
        if (!isset($this->routes[$name])) {
            $this->routes[$name] = $route;
        } else {
            throw new SynergyInvalidRouteException('route '.$name.' already added to Router');
        }

        return $this;
    }


    /**
     * Match the request to a route
     *
     * @param RequestInterface $request
     *
     * @return Route
     */
    public function match(RequestInterface $request)
    {
        /** @var Route $route */
        foreach ($this->routes as $route) {
            if ($route->requestMatch($request)) {
                return $route;
            }
        }

        return null;
    }


    /**
     * @return array of Route objects
     */
    public function getRoutes()
    {
        return $this->routes;
    }
}
