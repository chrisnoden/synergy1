<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://bitbucket.org/chrisnoden
 */

namespace ChrisNoden\Synergy\Routing;

use ChrisNoden\Synergy\Controller\ControllerInterface;
use ChrisNoden\Synergy\Controller\ControllerRouter;
use ChrisNoden\Synergy\Exception\SynergyControllerNotFoundException;
use ChrisNoden\Synergy\Exception\SynergyException;
use ChrisNoden\Synergy\Exception\SynergyInvalidControllerException;
use ChrisNoden\Synergy\Exception\SynergyInvalidRouteException;
use ChrisNoden\Synergy\Exception\SynergyInvalidRouteParameterCountException;
use ChrisNoden\Synergy\Exception\SynergyInvalidStringLengthException;
use ChrisNoden\Synergy\Exception\SynergyInvalidControllerMethodException;
use ChrisNoden\Synergy\Request\RequestInterface;
use ChrisNoden\Synergy\Request\WebRequest;
use ChrisNoden\Synergy\Tools\Tools;
use ChrisNoden\Synergy\View\ResponseInterface;
use Eloquent\Enumeration\Exception\UndefinedMemberException;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class Route
 *
 * @category ChrisNoden\Synergy\Routing
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class Route
{
    use LoggerAwareTrait;

    /** @var string name of the route */
    protected $name;
    /** @var array mapping that this route must match to be applied */
    protected $mappings = array();
    /** @var array controller classname and method */
    protected $controller = array();
    /** @var array additional/misc data assigned to the route */
    protected $meta_data = array();
    /** @var string fall-back to this method if not specified in the route */
    protected $defaultMethodName = 'defaultAction';
    /** @var string add this to all methods (if not already set) */
    protected $defaultMethodSuffix = 'Action';
    /** @var array params for the controller arguments */
    protected $param_names = array();
    /** @var array param regexs to match against */
    protected $param_reqs = array();
    /** @var array controller parameters */
    protected $params = array();
    /** @var RequestInterface */
    protected $request;
    /** @var int */
    static protected $routeNum = 1;


    /**
     * Build a single route from an array
     *
     * @param array           $data
     * @param LoggerInterface $logger
     *
     * @return Route
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidRouteException
     */
    public static function build(Array $data, LoggerInterface $logger = null)
    {
        if (count($data) != 1) {
            throw new SynergyInvalidRouteException(__METHOD__ . ' expects single item array with the route data');
        }
        $route = new self();
        if (is_string(key($data))) {
            $route->setName(key($data));
        }
        if (is_null($logger)) {
            $route->setLogger(new NullLogger());
        } else {
            $route->setLogger($logger);
        }

        foreach ($data[key($data)] as $key => $val) {
            $key = preg_replace('/[^0-9a-z]/', '', strtolower($key));

            // is this the controller
            if ($key == 'defaults' && is_array($val)) {
                // legacy controller ?
                if (isset($val['_controller'])) {
                    $route->setController($val['_controller']);
                    continue;
                } elseif (isset($val['controller'])) {
                    $route->setController($val['controller']);
                    continue;
                }
            } elseif ($key == 'controller') {
                // new style controller
                $route->setController($val);
                continue;
            } elseif ($key == 'requirements') {
                $route->setParamRules($val);
                continue;
            }

            try {
                $mapping = MappingType::memberByValue($key, false);
                /** @noinspection PhpParamsInspection */
                $route->addMapping($mapping, $val);
            } catch (UndefinedMemberException $ex) {
                // no MappingType by that name - store as metadata
                $route->addMetaData($key, $val);
            }
        }

        if (!$route->isValid()) {
            throw new SynergyInvalidRouteException('Route '.$route->getName().' is not valid');
        }

        return $route;
    }


    /**
     * Instantiate a new Route object
     */
    public function __construct()
    {
        $this->setName('route'.self::$routeNum++);

        $this->setLogger(new NullLogger());
    }


    /**
     * Add a MappingType rule to the Route
     *
     * @param MappingType $mapping
     * @param mixed   $rule
     *
     * @return $this
     */
    public function addMapping(MappingType $mapping, $rule)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        if ($mapping == MappingType::PATH()) {
            if (preg_match_all('/\{([^0-9\,\}]+)\}/', $rule, $params) > 0) {
                $this->param_names = $params[1];
            }
        }
        $this->mappings[(string)$mapping->value()] = $rule;

        return $this;
    }


    /**
     * all the mappings
     *
     * @return array
     */
    public function getMappings()
    {
        return $this->mappings;
    }


    /**
     * Does the Route match the Request
     *
     * @param WebRequest $request
     *
     * @return bool
     */
    public function requestMatch(WebRequest $request)
    {
        $matched = array();

        $this->request = $request;

        foreach ($this->mappings as $mapping => $match) {
            switch ($mapping) {
                case MappingType::PATH:
                    if ($request->getPath()) {
                        if (!empty($this->param_names)) {
                            $aSrch = array();
                            $aRepl = array();
                            foreach ($this->param_names as $arg) {
                                $aSrch[] = '{'.$arg.'}';
                                if (isset($this->param_reqs[$arg])) {
                                    if ($this->param_reqs[$arg] == '\d') {
                                        $aRepl[] = '('.$this->param_reqs[$arg].'+)';
                                    } else {
                                        $aRepl[] = '(' . $this->param_reqs[$arg] . ')';
                                    }
                                } else {
                                    $aRepl[] = '([^\/]+)';
                                }
                            }

                            $match = '#^'. str_replace($aSrch, $aRepl, $match) . '$#';
                            if (preg_match($match, $request->getPath(), $matches)) {
                                if ($this->logger instanceof LoggerInterface) {
                                    $this->logger->debug(
                                        sprintf(
                                            'PATH %s matched',
                                            $request->getPath()
                                        ),
                                        array('regex' => $match)
                                    );
                                }
                                $matched[] = $mapping;
                                unset($matches[0]);
                                // set the controller params found in the path
                                foreach ($matches as $key => $prm) {
                                    $this->params[$this->param_names[$key-1]] = $prm;
                                }
                            }
                        } elseif ($this->mappingStringMatches($match, $request->getPath())) {
                            if ($this->logger instanceof LoggerInterface) {
                                $this->logger->debug(
                                    sprintf(
                                        'PATH %s matched',
                                        $request->getPath()
                                    )
                                );
                            }
                            $matched[] = $mapping;
                        }
                    }
                    break;

                case MappingType::METHODS:
                    if ($request->getMethod() && is_array($match) && in_array($request->getMethod(), $match)) {
                        if ($this->logger instanceof LoggerInterface) {
                            $this->logger->debug(
                                sprintf(
                                    'METHOD %s matched',
                                    $request->getMethod()
                                ),
                                array('allowed' => $match)
                            );
                        }
                        $matched[] = $mapping;
                    } elseif ($request->getMethod() && $match == $request->getMethod()) {
                        if ($this->logger instanceof LoggerInterface) {
                            $this->logger->debug(
                                sprintf(
                                    'METHOD %s matched',
                                    $request->getMethod()
                                ),
                                array('allowed' => $match)
                            );
                        }
                        $matched[] = $mapping;
                    }
                    break;

                case MappingType::DEVICES:
                    if ($request->getRequestClient()) {
                        $device = $request->getRequestClient();
                        if (is_array($match)) {
                            if (in_array($device->getDevice(), $match)) {
                                if ($this->logger instanceof LoggerInterface) {
                                    $this->logger->debug(
                                        sprintf(
                                            'DEVICE %s matched',
                                            $device->getDevice()
                                        ),
                                        array('allowed' => $match)
                                    );
                                }
                                $matched[] = $mapping;
                            }
                        } else {
                            if ($device->getDevice() == $match) {
                                if ($this->logger instanceof LoggerInterface) {
                                    $this->logger->debug(
                                        sprintf(
                                            'DEVICE %s matched',
                                            $device->getDevice()
                                        ),
                                        array('allowed' => $match)
                                    );
                                }
                                $matched[] = $mapping;
                            }
                        }
                    }
                    break;

                case MappingType::HOST:
                    if ($request->getHost() && $this->mappingStringMatches($match, $request->getHost())) {
                        if ($this->logger instanceof LoggerInterface) {
                            $this->logger->debug(
                                sprintf(
                                    'HOST %s matched',
                                    $request->getHost()
                                ),
                                array('allowed' => $match)
                            );
                        }
                        $matched[] = $mapping;
                    }
                    break;

                case MappingType::SCHEME:
                    if (($match == 'ssl' || $match == 'https') && $request->getSsl()) {
                        if ($this->logger instanceof LoggerInterface) {
                            $this->logger->debug(
                                sprintf(
                                    'SCHEME https matched',
                                    $request->getHost()
                                ),
                                array('allowed' => $match)
                            );
                        }
                        $matched[] = $mapping;
                    } elseif ($match == 'http' && !$request->getSsl()) {
                        if ($this->logger instanceof LoggerInterface) {
                            $this->logger->debug(
                                sprintf(
                                    'SCHEME http matched',
                                    $request->getHost()
                                ),
                                array('allowed' => $match)
                            );
                        }
                        $matched[] = $mapping;
                    }
                    break;

                case MappingType::URL:
                    if ($this->mappingStringMatches($match, $request->getUri())) {
                        if ($this->logger instanceof LoggerInterface) {
                            $this->logger->debug(
                                sprintf(
                                    'URL %s matched',
                                    $request->getUri()
                                )
                            );
                        }
                        $matched[] = $mapping;
                    }
                    break;
            }
        }

        if (count($matched) == count($this->mappings)) {
            if ($this->logger instanceof LoggerInterface) {
                $this->logger->debug(
                    sprintf(
                        'route: %s MATCHED request',
                        $this->name
                    )
                );
            }
            return true;
        }

        return false;
    }


    /**
     * Does the mapping match
     *
     * @param string $needle
     * @param string $haystack
     *
     * @return bool
     */
    protected function mappingStringMatches($needle, $haystack)
    {
        if (Tools::isRegex($needle) === true) {
            if (preg_match($needle, $haystack)) {
                return true;
            }
            return false;
        } else {
            if ($needle == $haystack) {
                return true;
            }
        }

        return false;
    }


    /**
     * Store miscelleneous extra data for the route
     *
     * If the key already exists AND it is currently an array then the data is merged (array_merge) or appended
     *
     * @param string $key  key name to create/replace
     * @param mixed  $data
     *
     * @return $this
     */
    public function addMetaData($key, $data)
    {
        if (isset($this->meta_data[$key])) {
            if (is_array($this->meta_data[$key]) && is_array($data)) {
                $this->meta_data[$key] = array_merge($this->meta_data[$key], $data);
            } elseif (is_array($this->meta_data[$key])) {
                $this->meta_data[$key][] = $data;
            } else {
                $this->meta_data[$key] = $data;
            }
        } else {
            $this->meta_data[$key] = $data;
        }

        return $this;
    }


    /**
     * All the route data that couldn't be matched to routing rules/params
     *
     * @return array
     */
    public function getMetaData()
    {
        return $this->meta_data;
    }


    /**
     * Set the route Controller
     *
     * @param mixed $controller
     *
     * @throws SynergyException
     * @throws SynergyControllerNotFoundException
     * @throws SynergyInvalidControllerMethodException
     */
    public function setController($controller)
    {
        if ($controller instanceof ControllerInterface) {
            throw new SynergyException('Unsupported object passed to setController()');
        } elseif (is_string($controller)) {
            $cr = new ControllerRouter();
            $this->controller = $cr->parseControllerString($controller);
        }
    }


    /**
     * @return array with className, object and methodName keys
     */
    public function getController()
    {
        return $this->controller;
    }


    /**
     * Attempts to instantiate a new object of the controller class
     *
     * @return ControllerInterface|bool false if a controller has not been matched
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidControllerException
     */
    public function getControllerObject()
    {
        if (isset($this->controller['object'])) {
            return $this->controller['object'];
        } elseif (isset($this->controller['className'])) {
            $class_name = $this->controller['className'];

            // Test the class
            $reflectionClass = new \ReflectionClass($class_name);
            if (!($reflectionClass->IsInstantiable())) {
                throw new SynergyInvalidControllerException($class_name . ' is not instantiable');
            }

            // Instantiate the controller object
            $this->controller['object'] = new $class_name();
            if (!$this->controller['object'] instanceof ControllerInterface) {
                throw new SynergyInvalidControllerException($class_name . ' does not implement ControllerInterface');
            }

            // Pass the logger to the controller object
            if ($this->logger instanceof LoggerInterface && method_exists($this->controller['object'], 'setLogger')) {
                call_user_func(array($this->controller['object'], 'setLogger'), $this->logger);
            }

            return $this->controller['object'];
        }

        return false;
    }


    /**
     * Launch (run) the controller action (the method) passing it the parameters identified
     *
     * @return bool|mixed
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidRouteParameterCountException
     *           thrown if the arguments the method expects are not a match for the parameters identified
     *           in your route and path
     */
    public function launchAction()
    {
        if (!isset($this->controller['className']) || !isset($this->controller['methodName'])) {
            return false;
        }

        // Instantiate the Controller object
        $object = $this->getControllerObject();

        // Assign the logger to the controller object
        if ($this->logger instanceof LoggerInterface && method_exists($object, 'setLogger')) {
            call_user_func(array($object, 'setLogger'), $this->logger);
        }

        // Assign the request
        // Pass any meta-data from the route in as parameters to the request
        foreach ($this->getMetaData() as $key => $val) {
            $this->request->addParameter($key, $val);
        }
        $response = call_user_func(array($object, 'setRequest'), $this->request);
        if ($response instanceof ResponseInterface) {
            return $response;
        }

        // Initialise the array of parameters for the action method
        $parameters = array();

        // How many parameters does the target method expect
        $r = new \ReflectionMethod($this->controller['className'], $this->controller['methodName']);
        $classParams = $r->getParameters();
        $missing = array();
        // Populate the parameters for the controller
        foreach ($classParams as $argKey => $oName) {
            $argName = (string)$oName->getName();
            if (isset($this->params[(string)$argName])) {
                $parameters[$argKey] = $this->params[(string)$argName];
            } else {
                $missing[] = (string)$argName;
            }
        }

        // do we have all the parameters
        if (!empty($missing)) {
            throw new SynergyInvalidRouteParameterCountException(
                sprintf(
                    "%s->%s() expects %s parameters, %s missing in Route definition",
                    $this->controller['className'],
                    $this->controller['methodName'],
                    count($classParams),
                    implode(' & ', $missing)
                )
            );
        }

        // Launch any preAction method
        if (method_exists($object, 'preAction')) {
            $response = call_user_func(array(
                $object,
                'preAction'
            ));
            if ($response instanceof ResponseInterface) {
                return $response;
            }
        }

        $methodName = $this->controller['methodName'];

        if ($this->logger instanceof LoggerInterface) {
            $this->logger->debug(
                sprintf(
                    'Calling %s->%s()',
                    get_class($object),
                    $methodName
                ),
                $parameters
            );
        }

        // This is quicker than call_user_func_array
        switch(count($parameters)) {
            case 0:
                $response = $object->{$methodName}();
                break;
            case 1:
                $response = $object->{$methodName}($parameters[0]);
                break;
            case 2:
                $response = $object->{$methodName}($parameters[0], $parameters[1]);
                break;
            case 3:
                $response = $object->{$methodName}($parameters[0], $parameters[1], $parameters[2]);
                break;
            case 4:
                $response = $object->{$methodName}($parameters[0], $parameters[1], $parameters[2], $parameters[3]);
                break;
            case 5:
                $response = $object->{$methodName}($parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4]);
                break;
            default:
                $response = call_user_func_array(array($this->controller['className'], $methodName), $parameters);
        }

        return $response;
    }


    /**
     * @return bool true if the route can be used
     */
    public function isValid()
    {
        if (
            isset($this->controller['className']) &&
            count($this->mappings) > 0 &&
            isset($this->name)
        ) {
            return true;
        } elseif ($this->logger instanceof LoggerInterface) {
            if (!isset($this->controller['classname'])) {
                $this->logger->error('route: '.$this->getName().', controller classname not valid');
            }
            if (count($this->mappings) == 0) {
                $this->logger->error('route: '.$this->getName().', no valid mappings found');
            }
            if (!isset($this->name)) {
                $this->logger->error('route: '.$this->getName().', no route name set');
            }
        }

        return false;
    }


    /**
     * Set a handy name for the Route
     *
     * @param string $name
     *
     * @return $this
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidStringLengthException
     */
    public function setName($name)
    {
        if (strlen($name) < 1 || strlen($name) > 30) {
            throw new SynergyInvalidStringLengthException('Route name must be between 1 and 30 chars long');
        }

        $this->name = $name;

        return $this;
    }


    /**
     * Unique name of the route
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set the regex rules for matching controller arguments
     *
     * @param array $rules
     *
     * @return $this
     */
    public function setParamRules(Array $rules)
    {
        $this->param_reqs = $rules;

        return $this;
    }


    /**
     * @return array associative array of params identified in the path
     */
    public function getParams()
    {
        return $this->params;
    }
}
