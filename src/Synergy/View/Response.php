<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\View;

use ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException;
use ChrisNoden\Synergy\Synergy\Config;
use ChrisNoden\Synergy\Type\ContentType;
use ChrisNoden\Synergy\Type\HttpStatusCode;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;

/**
 * Class Response
 *
 * @category ChrisNoden\Synergy\View
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class Response extends ResponseAbstract implements ResponseInterface
{
    /** @var HttpStatusCode */
    protected $status;
    /** @var array Response headers */
    protected $headers = array();
    /** @var int how long to set the cache timeout in the headers */
    protected $cache_duration = 0;
    /** @var string */
    protected $contentType;
    /** @var bool */
    protected $optionsResponse = false;
    /** @var bool shall we compress the response output */
    protected $compressOutput = true;


    /**
     * Create a new Response object
     *
     * @param mixed $content
     * @param int   $status
     * @param array $headers
     *
     * @return ResponseInterface
     */
    public static function create($content, $status = 200, $headers = array())
    {
        $obj = new static();
        $obj
            ->setContent($content)
            ->setStatus($status)
            ->setHeaders($headers);

        if (Config::getLogger() instanceof LoggerInterface) {
            $obj->setLogger(Config::getLogger());
        }
        return $obj;
    }


    /**
     * Send the headers and body to the client
     *
     * @return $this
     */
    public function send()
    {
        if (!($this->status instanceof HttpStatusCode)) {
            $this->status = HttpStatusCode::HTTP_200();
        }

        $this->sendHeaders();

        // an OPTIONS method means only send the headers
        if ($this->optionsResponse === false) {
            $this->sendContent();
        }

        return $this;
    }


    /**
     * Send the HTTP headers
     *
     * @return $this
     */
    public function sendHeaders()
    {
        if (!headers_sent()) {
            http_response_code(intval(substr($this->status->key(), 5)));
            foreach ($this->getHeaders() as $header) {
                header($header);
            }
        }
        return $this;
    }


    /**
     * The Response header strings
     *
     * @return array
     */
    public function getHeaders()
    {
        $arr = $this->getHeadersArray();
        $headers = array();

        if (isset($_SERVER['SERVER_PROTOCOL'])) {
            $headers[] = $_SERVER['SERVER_PROTOCOL'].' '.substr($this->status->key(), 5).' '.$this->status->value();
        } else {
            $headers[] = 'HTTP/1.1 '.substr($this->status->key(), 5).' '.$this->status->value();
        }

        foreach ($arr as $hdr => $value) {
            if ($value === false) {
                continue;
            }
            $headers[] = sprintf('%s: %s', $hdr, $value);
        }

        return $headers;
    }


    /**
     * The full array of headers as key => value pairs
     *
     * @return array
     */
    public function getHeadersArray()
    {
        $this->headers = array_merge(
            $this->defaultHeaders(),
            $this->headers
        );

        if (
            !isset($this->headers['Content-Type']) ||
            (($this->headers['Content-Type']) && $this->headers['Content-Type'] == null)
        ) {
            $this->headers['Content-Type'] = 'text/plain';
        }

        return $this->headers;
    }


    /**
     * Send the main body of the response
     *
     * @return $this
     */
    public function sendContent()
    {
//        if ($this->compressOutput && extension_loaded('zlib')) {
//            ob_start('ob_gzhandler');
//        }

        echo($this->getContent());

        return $this;
    }


    /**
     * Set the Response HTTP status
     *
     * @param int|HttpStatusCode $status
     *
     * @return $this
     * @throws SynergyInvalidArgumentException if the status is invalid
     */
    public function setStatus($status)
    {
        if (intval($status)) {
            $status = HttpStatusCode::code($status);
        } elseif (!($status instanceof HttpStatusCode)) {
            throw new SynergyInvalidArgumentException('Invalid HTTP Status code');
        }
        $this->status = $status;

        return $this;
    }


    /**
     * @return HttpStatusCode
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Set all the headers (replacing all current headers)
     *
     * @param array $headers
     *
     * @return $this
     */
    public function setHeaders(Array $headers)
    {
        if (!empty($headers)) {
            $this->headers = $headers;
        }

        return $this;
    }


    /**
     * Get a set of headers to use as a default template
     *
     * @return array
     */
    protected function defaultHeaders()
    {
        if ($this->cache_duration == 0) {
            $headers = array(
                'Status'         => substr($this->status->key(), 5).' '.$this->status->value(),
                'Expires'        => date('r', strtotime('Yesterday')),
                'Last-Modified'  => date('r'),
                'Cache-Control'  => 'no-store, no-cache, max-age=0, must-revalidate',
                'Pragma'         => 'no-cache',
            );
        } else {
            $headers = array(
                'Status'         => substr($this->status->key(), 5).' '.$this->status->value(),
                'Expires'        => date('r', strtotime('+' . $this->cache_duration . ' seconds')),
                'Last-Modified'  => date('r'),
                'Cache-Control'  => 'private, max-age=' . $this->cache_duration . ', must-revalidate',
                'Pragma'         => 'private',
            );
        }

        if (!is_null($this->getContent())) {
            $headers['Content-Length'] = strlen($this->getContent());
        } else {
            $headers['Content-Length'] = '0';
        }

        $headers['Content-Type'] = 'text/html';

        return $headers;
    }


    /**
     * Set a new header or replace an existing one with the same header_name
     *
     * @param string $header_name   eg Content-Type
     * @param string $header_value
     *
     * @return $this
     */
    public function addHeader($header_name, $header_value)
    {
        $this->headers[$header_name] = $header_value;

        return $this;
    }


    /**
     * Set the cache_duration to 0 seconds which also sends out the 'no-cache' headers
     *
     * @return $this
     */
    public function setNoCache()
    {
        $this->cache_duration = 0;

        return $this;
    }


    /**
     * How long should the response cache be set to expire in seconds
     *
     * @param int $seconds
     *
     * @return $this
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException
     */
    public function setCacheDuration($seconds)
    {
        if (!is_int($seconds)) {
            throw new SynergyInvalidArgumentException('setCacheDuration requires an integer number of seconds');
        }
        $this->cache_duration = $seconds;

        return $this;
    }


    /**
     * How long is the cache header set to last
     *
     * @return int value of member
     */
    public function getCacheDuration()
    {
        return $this->cache_duration;
    }


    /**
     * Set the content-type header value
     * Only a very basic check is made that the value is valid
     *
     * @param string $content_type eg application/json
     *
     * @return $this
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException
     */
    public function setContentType($content_type)
    {
        if (preg_match('/^[^\/]+\/[^\/]+$/', $content_type)) {
            $this->contentType = $content_type;
            $this->addHeader('Content-Type', $content_type);
        } elseif ($this->contentType = ContentType::getTypeFromExtension($content_type)) {
            $this->addHeader('Content-Type', $this->contentType);
        } else {
            throw new SynergyInvalidArgumentException('Invalid Content-Type specified '.$content_type);
        }

        return $this;
    }


    /**
     * The content-type / mime-type of the content
     *
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }


    /**
     * Value of member optionsResponse
     *
     * @return boolean value of member
     */
    public function getOptionsResponse()
    {
        return $this->optionsResponse;
    }


    /**
     * Set the value of optionsResponse member
     *
     * @param boolean $optionsResponse
     *
     * @return $this
     */
    public function setOptionsResponse($optionsResponse)
    {
        $this->optionsResponse = $optionsResponse;
        return $this;
    }


    /**
     * Shall we compress the output to the browser
     *
     * @param boolean $compressOutput
     *
     * @return $this
     */
    public function setCompressOutput($compressOutput)
    {
        $this->compressOutput = $compressOutput;
        return $this;
    }
}
