<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\View;

use ChrisNoden\Synergy\Exception\SynergyFileNotReadableException;
use ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException;
use Psr\Log\LoggerInterface;

/**
 * Class FileResponse
 * A custom web response designed to deliver a file to the browser rather than a HTML document
 *
 * @category ChrisNoden\Synergy\View
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class FileResponse extends Response implements ResponseInterface
{

    /** @var string */
    protected $filename;
    /** @var string */
    protected $extension = 'html';
    /** @var int how long to set the cache timeout in the headers */
    protected $cache_duration = 300;


    /**
     * Set the Response body content
     *
     * @param string $content a filename or the contents
     *
     * @return $this
     * @throws SynergyInvalidArgumentException if the content is invalid
     * @throws SynergyFileNotReadableException
     */
    public function setContent($content)
    {
        if (null !== $content && !is_string($content) && !is_numeric($content) && !is_callable(array($content, '__toString'))) {
            throw new SynergyInvalidArgumentException('content must be a string or object implementing __toString()');
        }
        if (strstr((string) $content, "\n") === false && strstr((string) $content, DIRECTORY_SEPARATOR) !== false) {
            // it looks like a file
            $this->setFilename((string) $content);
        } else {
            $this->content = (string) $content;
        }

        return $this;
    }


    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return void
     * @throws SynergyFileNotReadableException
     */
    protected function setFilename($filename)
    {
        if (is_readable($filename)) {
            if ($this->logger instanceof LoggerInterface) {
                $this->logger->debug('Loading asset from file: '.$filename);
            }
            $this->filename = $filename;
            $extension      = strtolower(pathinfo($this->filename, PATHINFO_EXTENSION));
            try {
                if ($extension !== '') {
                    $this->setExtension($extension);
                } else {
                    $this->setContentType('text/plain');
                }
                $this->headers['X-Filename']     = basename($filename);
                $this->headers['Last-Modified']  = date('r', filemtime($filename));
                $this->headers['ETag']           = md5(filectime($filename));
                $this->headers['Content-Length'] = filesize($filename);
            } catch (SynergyInvalidArgumentException $ex) {
                // fall-back
                $this->extension = $extension;
                if (function_exists('shell_exec')) {
                    $file = escapeshellarg($filename);
                    $this->contentType = shell_exec('file -bi ' . $file);
                    if (trim($this->contentType) == 'regular file') {
                        $this->contentType = 'text/plain';
                    }
                    $this->headers['Content-Type']   = $this->contentType;
                }
            }
        } else {
            throw new SynergyFileNotReadableException(
                'Asset file not readable'
            );
        }
    }


    /**
     * Value of member filename
     *
     * @return string value of member
     */
    public function getFilename()
    {
        return $this->filename;
    }


    /**
     * Alias of setContentType method
     *
     * @param string $extension
     *
     * @return $this
     * @throws SynergyInvalidArgumentException if the extension is not valid
     */
    public function setExtension($extension)
    {
        $this->setContentType($extension);
        $this->extension = $extension;

        return $this;
    }


    /**
     * The file extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }


    /**
     * Send the body of the response to the client
     *
     * @return $this
     */
    public function sendContent()
    {
        if (isset($this->filename)) {
            $fp = fopen($this->filename, 'rb');
            fpassthru($fp);
        } else {
            parent::sendContent();
        }

        return $this;
    }
}
