<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\View;

use ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException;
use ChrisNoden\Synergy\Request\WebRequest;

/**
 * Class ApiResponseInterface
 *
 * @category ChrisNoden\Synergy\View
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
interface ApiResponseInterface extends ResponseInterface
{
    /**
     * Set the Response body content
     *
     * @param mixed $content Object, Collection or Array of objects
     *
     * @return ApiResponse
     * @throws SynergyInvalidArgumentException if the content is invalid
     */
    public function setContent($content);


    /**
     * Set the Request object
     *
     * @param WebRequest $request
     *
     * @return ApiResponse
     */
    public function setRequest(WebRequest $request);


    /**
     * Add additional data to the Response body
     *
     * @param array $metadata
     *
     * @return $this
     * @throws SynergyInvalidArgumentException if you include the reserved 'data' key in your array
     */
    public function setMetadata(Array $metadata);


    /**
     * Set the value of default_date_format member
     *
     * @param string $default_date_format
     *
     * @return $this
     */
    public function setDefaultDateFormat($default_date_format);


    /**
     * Default cache age passed to the client
     *
     * @param int $max_age seconds
     *
     * @return $this
     */
    public function setMaxAge($max_age);


    /**
     * Set the value of allowed_origin member
     *
     * @param string $allowed_origin valid origin site (http://site.com) or *
     *
     * @return $this
     */
    public function setAllowedOrigin($allowed_origin);


    /**
     * Set the value of allowed_headers member
     *
     * @param array $allowed_headers
     *
     * @return $this
     */
    public function setAllowedHeaders(Array $allowed_headers);


    /**
     * Set the value of allowed_methods member
     *
     * @param array $allowed_methods
     *
     * @return $this
     */
    public function setAllowedMethods(Array $allowed_methods);
}
