<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\View;

use ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException;
use ChrisNoden\Synergy\Template\TemplateInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * Abstract AssetAbstract
 *
 * @category ChrisNoden\Synergy\View
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
abstract class ResponseAbstract
{

    use LoggerAwareTrait;

    /** @var string the Response content */
    protected $content;
    /** @var string */
    protected $cache_dir;


    /**
     * Send the headers and body to the client
     *
     * @return $this
     */
    public function send()
    {
        echo($this->getContent());

        return $this;
    }


    /**
     * @param string $content
     *
     * @return $this
     * @throws SynergyInvalidArgumentException if the content is invalid
     */
    public function setContent($content)
    {
        if (
            null !== $content &&
            !is_string($content) &&
            !is_numeric($content) &&
            !is_callable(array($content, '__toString'))
        ) {
            throw new SynergyInvalidArgumentException('content must be a string or object implementing __toString()');
        }
        $this->content = $content;

        return $this;
    }


    /**
     * The Response body
     *
     * @return string
     */
    public function getContent()
    {
        if ($this->content instanceof TemplateInterface && !is_null($this->cache_dir)) {
            $this->content->setCacheDir($this->cache_dir);
        }
        return (string) $this->content;
    }


    /**
     * @param string $directory
     */
    public function setCacheDirectory($directory)
    {
        $this->cache_dir = $directory;
    }
}
