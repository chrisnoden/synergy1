<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\View;

use ChrisNoden\Synergy\Exception\SynergyApiException;
use ChrisNoden\Synergy\Exception\SynergyApiInvalidContentTypeException;
use ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException;
use ChrisNoden\Synergy\Request\WebRequest;
use ChrisNoden\Synergy\Type\HttpStatusCode;
use Propel\Runtime\Map\ColumnMap;
use Propel\Runtime\Util\PropelModelPager;

/**
 * Class ApiResponse
 * Given a Propel2 Model Object or an ObjectCollection this class formats a WebResponse
 * that represents the Object data
 *
 * @category ChrisNoden\Synergy\View
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class ApiResponse extends Response implements ApiResponseInterface
{
    /** @var WebRequest */
    protected $request;
    /** @var int cache max age of resource for caching */
    protected $max_age = 300;
    /** @var array */
    protected $allowed_methods = array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS');
    /** @var string */
    protected $allowed_origin = '*'; // all origins
    /** @var array */
    protected $allowed_headers = array(
        'accept',
        'origin',
        'Content-Type',
        'Accept-Encoding'
    );
    /** @var string default response type (json, csv, xml, serialize) */
    protected $response_format = 'json';
    /** @var string what date format should be used in the response for DateTime objects */
    protected $default_date_format = DATE_ISO8601;
    /** @var mixed */
    protected $data;
    /** @var array fields to include in the response */
    protected $field_inclusions = array();
    /** @var int should we recurse through ID fields */
    protected $recurse = 0;
    /** @var array */
    protected $metadata = array();
    /** @var bool show the response without nesting below a 'data' key */
    protected $show_raw = false;


    public function __construct()
    {
        $this->status = HttpStatusCode::HTTP_200();
    }


    /**
     * Create a new Response object
     *
     * @param mixed $content
     * @param int   $status
     * @param array $headers
     *
     * @return ApiResponseInterface
     */
    public static function create($content, $status = 200, $headers = array())
    {
        /** @var ApiResponseInterface $obj */
        $obj = new static();
        $obj
            ->setContent($content)
            ->setStatus($status)
            ->setHeaders($headers);

        return $obj;
    }


    /**
     * Set the Response body content
     *
     * @param mixed $content Object, Collection or Array of objects
     *
     * @return ApiResponse
     * @throws SynergyInvalidArgumentException if the content is invalid
     */
    public function setContent($content)
    {
        if (null === $content || is_string($content) || is_numeric($content)) {
            throw new SynergyInvalidArgumentException('content must be an Object, Collection or Array of objects');
        }

        $this->data = $content;
        if (method_exists($content, 'count')) {
            $count = $content->count();
        } elseif (is_array($content) && isset($content['count'])) {
            $count = (int) $content['count'];
        } elseif (is_array($content)) {
            $count = count($content);
        } elseif (is_object($content)) {
            $count = 1;
        } else {
            $count = 1;
        }

        $this->metadata = array_merge(
            $this->metadata,
            array(
                '_metadata' => array(
                    'num_results' => $count
                )
            )
        );

        return $this;
    }


    /**
     * Turn the source data into our response content in the chosen format
     *
     * @return void
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiException
     */
    protected function prepareResponseContent()
    {
        if ($this->data instanceof \IteratorAggregate) {
            $data = array('data' => array());
            foreach ($this->data as $object) {
                $data['data'][] = $this->buildEntityResponseData($object);
            }
        } elseif (is_array($this->data) && isset($this->data['data']) && $this->data['data'] instanceof PropelModelPager) {
            $data = array('data' => array());
            foreach ($this->data['data'] as $object) {
                $data['data'][] = $this->buildEntityResponseData($object);
            }
            if (isset($this->data['_metadata'])) {
                $data['_metadata'] = $this->data['_metadata'];
            }
        } elseif (is_array($this->data) && isset($this->data['data'])) {
            $data = array('data' => array());
            if (is_array($this->data['data'])) {
                foreach ($this->data['data'] as $key => $object) {
                    $data['data'][] = $this->buildEntityResponseData($object);
                }
            } else {
                $data = array('data' => $this->buildEntityResponseData($this->data['data']));
            }
            if (isset($this->data['_metadata'])) {
                $data['_metadata'] = $this->data['_metadata'];
            }
        } elseif (is_array($this->data) && is_object($this->data[0])) {
            $data = array('data' => array());
            foreach ($this->data as $key => $object) {
                if (is_object($object)) {
                    $data['data'][] = $this->buildEntityResponseData($object);
                } elseif ($key == 'count' && is_numeric($object)) {
                    $data['count'] = (int) $object;
                }
            }
        } elseif (!is_null($this->data)) {
            $data = array('data' => $this->buildEntityResponseData($this->data));
        } else {
            $data = array();
        }
        if (!empty($this->metadata)) {
            $data = array_merge_recursive($data, $this->metadata);
        }

        // Format our data
        if (class_exists('ChrisNoden\\Synergy\\Renderer\\'.ucfirst($this->response_format).'Renderer')) {
            if ($this->show_raw === true) {
                $data = $data['data'];
            }
            $this->content = call_user_func(
                'ChrisNoden\\Synergy\\Renderer\\' . ucfirst($this->response_format) . 'Renderer::render',
                $data
            );
        } else {
            throw new SynergyApiException('No handler for '.$this->response_format);
        }
    }


    /**
     * Iterate an individual entity object and return an array of fields and values
     * Reformat, remove or supplement the fields as necessary
     * based on rules in the Model and options set by the API request
     *
     * @param Object $entity
     *
     * @return array
     * @throws SynergyApiException thrown if the entity can't be rendered to an array
     */
    private function buildEntityResponseData($entity, $recurse = false)
    {
        if ($recurse === false) {
            $recurse = $this->recurse;
        }
        $extra_fields = $this->field_inclusions;

        if (is_object($entity) && method_exists($entity, 'toArray')) {
            $arr = $entity->toArray();
        } elseif (is_object($entity)) {
            $arr = (array) $entity;
        } elseif (is_array($entity)) {
            $arr = $entity;
        } elseif (is_string($entity)) {
            return $entity;
        } else {
            throw new SynergyApiException('Unable to render the response');
        }

        if (method_exists($entity, 'apiOptions')) {
            $apiOptions = call_user_func(array($entity, 'apiOptions'));
            if (is_array($apiOptions) && isset($apiOptions['include'])) {
                $extra_fields = array_merge($apiOptions['include'], $extra_fields);
            }
        }

        // We can insert any extra fields that are requested before our final cleansing
        if (!empty($extra_fields)) {
            foreach ($extra_fields as $check_field) {
                if (!isset($arr[$check_field])) {
                    // Set an initial value for this extra field we want
                    $arr[$check_field] = true;
                }
            }
        }

        $data = array();

        // iterate the fields to see which should be included in the response
        foreach ($arr as $fldKey => $fldVal) {
            if ($this->skipField($entity, $fldKey)) {
                // don't include this field in the response
                continue;
            }

            // has the request asked for recursion and is this field a foreignId
            if ($recurse > 0 && strlen($fldKey) > 2 && substr($fldKey, -2) == 'Id' && is_object($entity)) {
                $targetName = substr($fldKey, 0, strlen($fldKey)-2);

                // Is there a foreign key (Propel2) matching this field
                $cl = implode('\\', array_slice(explode('\\', (get_class($entity))), 0, -1)) . '\\Map\\' . join('', array_slice(explode('\\', get_class($entity)), -1)) . 'TableMap';
                if (class_exists($cl)) {
                    $map = new $cl();
                    $keys = call_user_func(array($map, 'getForeignKeys'));
                    /**
                     * @var string $keyName
                     * @var ColumnMap $obj
                     */
                    foreach ($keys as $keyName => $obj) {
                        $fkey = call_user_func(array($obj, 'getPhpName'));
                        $meth = 'get'.preg_replace('/ /', '', ucwords(preg_replace('/[^a-zA-Z0-9]/', ' ', call_user_func(array($obj, 'getRelatedTableName')))));
                        if ($fkey == $fldKey && method_exists($entity, $meth)) {
                            $subEntity = call_user_func(array($entity, $meth));
                            if (method_exists($subEntity, 'toArray')) {
                                $data[lcfirst($targetName)] = $this->buildEntityResponseData($subEntity, $recurse - 1);
                            } else {
                                $data[lcfirst($targetName)] = null;
                            }
                            continue(2);
                        }
                    }
                }
                // attempt to replace any Id fields with their values as a sub-object
                if (method_exists($entity, 'get'.ucfirst($targetName))) {
                    // look for any foreign-key getter in the Model - getForeignId()
                    $subEntity = call_user_func(array($entity, 'get'.ucfirst($targetName)));
                    if (method_exists($subEntity, 'toArray')) {
                        $data[lcfirst($targetName)] = $this->buildEntityResponseData($subEntity, $recurse - 1);
                    } else if (!is_null($subEntity)) {
                        $data[lcfirst($targetName)] = $subEntity;
                    } else {
                        $data[lcfirst($targetName)] = null;
                    }
                    continue;
//                } elseif (class_exists($namespace . '\\' . $targetName)) {
//                    // load a new object for the sub-object (extra db call) from this namespace
//                    $subClassName = $namespace . '\\' . $targetName;
//                    $subEntity = $this->loadRecord($subClassName, array($fldVal));
//                    $data[lcfirst($targetName)] = $this->buildResourceResponseData($subEntity);
//                    continue;
//                } elseif (class_exists($this->namespace . '\\' . $targetName)) {
//                    // load a new object for the sub-object from the Core model namespace (extra db call)
//                    $subClassName = $this->namespace . '\\' . $targetName;
//                    $subEntity = $this->loadRecord($subClassName, array($fldVal));
//                    $data[lcfirst($targetName)] = $this->buildResourceResponseData($subEntity);
//                    continue;
                }
            }

            // Do we have a getter for the field (rather than using the raw data)
            if (method_exists($entity, 'get'.ucfirst($fldKey))) {
                $fldVal = call_user_func(
                    array(
                        $entity,
                        'get'.ucfirst($fldKey)
                    )
                );
            }

            // reformat any DateTime fields
            if ($fldVal instanceof \DateTime) {
                $data[lcfirst($fldKey)] = $fldVal->format($this->default_date_format);
                continue;
            }

            // fall through to just showing the raw foreignId = value style
            $data[lcfirst($fldKey)] = $fldVal;
        }

        return $data;
    }


    /**
     * Should we not return this field in our response
     *
     * @param object $object
     * @param string $fldKey
     *
     * @return bool
     */
    private function skipField($object, $fldKey)
    {
        if (method_exists($object, 'apiOptions')) {
            $apiOptions = call_user_func(array($object, 'apiOptions'));

            if (isset($apiOptions['hidden']) && in_array(lcfirst($fldKey), $apiOptions['hidden'])) {
                // never expose this field
                return true;
            }

            // do we have an inclusion list
//            if (count($this->field_inclusions) > 0) {
//                if (!in_array(lcfirst($fldKey), $this->field_inclusions)) {
//                    return true;
//                }
//            }

            if (isset($apiOptions['explicit'])) {
                // request needs to explicity ask for these fields, eg fields=fieldOne,fieldTwo
                if (in_array(lcfirst($fldKey), $apiOptions['explicit'])) {
                    return true;
                }
            }
        }

        if (!method_exists($object, 'get'.ucfirst($fldKey)) &&
            (method_exists($object, 'toArray') && !in_array($fldKey, $object->toArray()))
        ) {
            return true;
        }

        return false;
    }


    /**
     * The Response body
     *
     * @return string
     */
    public function getContent()
    {
        $this->prepareResponseContent();

        return parent::getContent();
    }


    /**
     * The Response headers
     *
     * @return array
     */
    public function getHeaders()
    {
        $this->setContentType($this->response_format);

        return parent::getHeaders();
    }


    /**
     * Get a set of headers to use as a default template
     *
     * @return array
     */
    protected function defaultHeaders()
    {
        $headers = parent::defaultHeaders();

        $headers = array_merge(
            $headers,
            array(
                'Access-Control-Allow-Origin' => $this->allowed_origin,
                'Access-Control-Allow-Methods' => implode(', ', $this->allowed_methods),
                'Access-Control-Allow-Headers' => implode(', ', $this->allowed_headers),
                'Access-Control-Max-Age' => $this->max_age
            )
        );

        return $headers;
    }


    /**
     * Set the Request object
     *
     * @param WebRequest $request
     *
     * @return ApiResponse
     */
    public function setRequest(WebRequest $request)
    {
        $this->request = $request;

        return $this;
    }


    /**
     * Set the format for the response (json, csv, xml, serialize)
     *
     * @param string $content_type
     *
     * @return ApiResponse
     * @throws SynergyApiInvalidContentTypeException if the format is not supported
     */
    public function setContentType($content_type)
    {
        $content_type = strtolower($content_type);
        if (
            $content_type != 'json' &&
            $content_type != 'csv' &&
            $content_type != 'xml' &&
            $content_type != 'serialize'
        ) {
            throw new SynergyApiInvalidContentTypeException('content_type must be one of json, csv, xml or serialize');
        }
        $this->response_format = $content_type;

        return parent::setContentType($content_type);
    }


    /**
     * Add additional data to the Response body
     *
     * @param array $metadata
     *
     * @return $this
     * @throws SynergyInvalidArgumentException if you include the reserved 'data' key in your array
     */
    public function setMetadata(Array $metadata)
    {
        if (isset($metadata['data'])) {
            throw new SynergyInvalidArgumentException('metadata can not include key called data');
        }
        $this->metadata = $metadata;

        return $this;
    }


    /**
     * Set the value of default_date_format member
     *
     * @param string $default_date_format
     *
     * @return $this
     */
    public function setDefaultDateFormat($default_date_format)
    {
        $this->default_date_format = $default_date_format;

        return $this;
    }


    /**
     * Default cache age passed to the client
     *
     * @param int $max_age seconds
     *
     * @return $this
     */
    public function setMaxAge($max_age)
    {
        $this->max_age = $max_age;

        return $this;
    }


    /**
     * Set the value of allowed_origin member
     *
     * @param string $allowed_origin valid origin site (http://site.com) or *
     *
     * @return $this
     */
    public function setAllowedOrigin($allowed_origin)
    {
        $this->allowed_origin = $allowed_origin;

        return $this;
    }


    /**
     * Set the value of allowed_headers member
     *
     * @param array $allowed_headers
     *
     * @return $this
     */
    public function setAllowedHeaders(Array $allowed_headers)
    {
        $this->allowed_headers = $allowed_headers;

        return $this;
    }


    /**
     * Set the value of allowed_methods member
     *
     * @param array $allowed_methods
     *
     * @return $this
     */
    public function setAllowedMethods(Array $allowed_methods)
    {
        $this->allowed_methods = $allowed_methods;

        return $this;
    }


    /**
     * Set the response to show only the raw data without the 'data' parent key name
     *
     * @param boolean $raw
     *
     * @return $this
     */
    public function setShowRaw($raw)
    {
        if ($raw === true) {
            $this->show_raw = true;
        } else {
            $this->show_raw = false;
        }
        return $this;
    }


    /**
     * Set the fields that will be shown (overriding default)
     *
     * @param Array $val
     *
     * @return $this
     */
    public function setFieldInclusions(Array $val)
    {
        $this->field_inclusions = $val;

        return $this;
    }


    /**
     * @param int $val
     *
     * @return $this
     */
    public function setRecurse($val)
    {
        if ($val === true) {
            $this->recurse = 2;
        } else if (is_numeric($val) && intval($val) == $val) {
            $this->recurse = (int) $val;
        } else {
            $this->recurse = 0;
        }

        return $this;
    }
}
