<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\View;

use Psr\Log\LoggerAwareInterface;

/**
 * Class ResponseInterface
 *
 * @category ChrisNoden\Synergy\View
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
interface ResponseInterface extends LoggerAwareInterface
{
    /**
     * Deliver the full response to the browser
     *
     * @return $this
     */
    public function send();


    /**
     *
     * @param mixed $content
     *
     * @return mixed
     */
    public function setContent($content);


    /**
     * The Response body
     *
     * @return string
     */
    public function getContent();


    /**
     * Send the HTTP headers
     *
     * @return $this
     */
    public function sendHeaders();


    /**
     * The Response headers
     *
     * @return array
     */
    public function getHeaders();


    /**
     * The full array of headers as key => value pairs
     *
     * @return array
     */
    public function getHeadersArray();


    /**
     * Set all the headers (replacing all current headers)
     *
     * @param array $headers
     *
     * @return $this
     */
    public function setHeaders(Array $headers);


    /**
     * Send the main body of the response
     *
     * @return $this
     */
    public function sendContent();


    /**
     * Set the Response HTTP status
     *
     * @param int|\ChrisNoden\Synergy\Type\HttpStatusCode $status
     *
     * @return $this
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException if the status is invalid
     */
    public function setStatus($status);


    /**
     * Set the directory the Response object can use to store temporary files
     *
     * @param  string $directory
     *
     * @return $this
     */
    public function setCacheDirectory($directory);


    /**
     * How long should the response cache be set to expire in seconds
     *
     * @param int $seconds
     *
     * @return $this
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException
     */
    public function setCacheDuration($seconds);


    /**
     * How long is the cache header set to last
     *
     * @return int value of member
     */
    public function getCacheDuration();


    /**
     * Set a new header or replace an existing one with the same header_name
     *
     * @param string $header_name   eg Content-Type
     * @param string $header_value
     *
     * @return $this
     */
    public function addHeader($header_name, $header_value);


    /**
     * Set the cache_duration to 0 seconds which also sends out the 'no-cache' headers
     *
     * @return $this
     */
    public function setNoCache();


    /**
     * Set the content-type header value
     * Only a very basic check is made that the value is valid
     *
     * @param string $content_type eg application/json
     *
     * @return $this
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException
     */
    public function setContentType($content_type);


    /**
     * The content-type / mime-type of the content
     *
     * @return string
     */
    public function getContentType();


    /**
     * Shall we compress the output to the browser
     *
     * @param boolean $compressOutput
     *
     * @return $this
     */
    public function setCompressOutput($compressOutput);
}
