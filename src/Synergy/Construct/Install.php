<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Construct;

use ChrisNoden\Synergy\Tools\Tools;
use Composer\Script\Event;

/**
 * Class Install
 *
 * @category ChrisNoden\Synergy\Construct
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class Install
{
    protected $io;
    /** @var string */
    protected $app_basedir;
    /** @var array */
    protected $extra = array();
    /** @var string */
    protected $synergy_basedir;


    public static function postUpdate(Event $event)
    {
        $obj = new Install($event);
        $obj->configWebStructure();
    }


    public static function postInstall(Event $event)
    {
        $obj = new Install($event);
        $obj->configWebStructure();
    }


    public function __construct(Event $event)
    {
        $this->io = $event->getIO();
        $this->extra = $event->getComposer()->getPackage()->getExtra();
        $this->synergy_basedir = dirname(dirname(dirname(__DIR__)));
    }

    public function configWebStructure()
    {
        $io = $this->io;
        $io->write("  Configuring...");

        $app_basedir = $_SERVER['PWD'];
        $this->app_basedir = $app_basedir;
        $source_file_dir = $this->synergy_basedir . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'files';

        // Config Dir
        $config_dir = $app_basedir . DIRECTORY_SEPARATOR . 'config';
        if (!is_dir($config_dir)) {
            Tools::mkdir($config_dir);
            $io->write("  -> config directory created");
            // copy routes.yml
            $this->installFile(
                $source_file_dir . DIRECTORY_SEPARATOR . 'routes.yml',
                $config_dir . DIRECTORY_SEPARATOR . 'routes.yml'
            );
            $io->write("  -> created config/routes.yml file");

            $this->installFile(
                $source_file_dir . DIRECTORY_SEPARATOR . 'config.yml',
                $config_dir . DIRECTORY_SEPARATOR . 'config.yml'
            );
            $io->write("  -> created config/config.yml file");
        }

        // DB Config Dir
        if (!is_dir($config_dir . DIRECTORY_SEPARATOR . 'db')) {
            Tools::mkdir($config_dir . DIRECTORY_SEPARATOR . 'db');
            $io->write("  -> config/db directory created");
            // copy propel.xml
            $this->installFile(
                $source_file_dir . DIRECTORY_SEPARATOR . 'propel.xml',
                $config_dir . DIRECTORY_SEPARATOR . 'db' . DIRECTORY_SEPARATOR . 'propel.xml'
            );
            $io->write("  -> created config/db/propel.xml file");

            $this->installFile(
                $source_file_dir . DIRECTORY_SEPARATOR . 'DB_README.md',
                $config_dir . DIRECTORY_SEPARATOR . 'db' . DIRECTORY_SEPARATOR . 'README.md'
            );
            $io->write("  -> created config/db/README.md file");

            $this->installFile(
                $source_file_dir . DIRECTORY_SEPARATOR . 'schema.xml',
                $config_dir . DIRECTORY_SEPARATOR . 'db' . DIRECTORY_SEPARATOR . 'schema.xml'
            );
            $io->write("  -> created config/db/schema.xml file");
        }

        // Logs Dir
        if (!is_dir($app_basedir . DIRECTORY_SEPARATOR . 'logs')) {
            Tools::mkdir($app_basedir . DIRECTORY_SEPARATOR . 'logs');
            chmod($app_basedir . DIRECTORY_SEPARATOR . 'logs', 0775);
            $io->write("  -> logs directory created");
        }

        if (!is_dir($app_basedir . DIRECTORY_SEPARATOR . 'templates')) {
            Tools::mkdir($app_basedir . DIRECTORY_SEPARATOR . 'templates');
            chmod($app_basedir . DIRECTORY_SEPARATOR . 'templates', 0755);
            $io->write("  -> templates directory created");
            // copy .htaccess
            $this->copyFile(
                $source_file_dir . DIRECTORY_SEPARATOR . 'index.html.tpl',
                $app_basedir . DIRECTORY_SEPARATOR . 'templates'
            );
            $io->write("  -> created templates/index.html.tpl file");
        }

        if (!is_dir($app_basedir . DIRECTORY_SEPARATOR . 'web')) {
            Tools::mkdir($app_basedir . DIRECTORY_SEPARATOR . 'web');
            chmod($app_basedir . DIRECTORY_SEPARATOR . 'web', 0755);
            $io->write("  -> logs directory created");

            // copy .htaccess
            $this->copyFile(
                $source_file_dir . DIRECTORY_SEPARATOR . '.htaccess',
                $app_basedir . DIRECTORY_SEPARATOR . 'web'
            );
            $io->write("  -> created web/.htaccess file");

            // copy index.php
            $this->copyFile(
                $source_file_dir . DIRECTORY_SEPARATOR . 'index.php',
                $app_basedir . DIRECTORY_SEPARATOR . 'web'
            );
            $io->write("  -> created web/index.php file");
        }


        if (!is_dir($app_basedir . DIRECTORY_SEPARATOR . 'src')) {
            Tools::mkdir($app_basedir . DIRECTORY_SEPARATOR . 'src');
            chmod($app_basedir . DIRECTORY_SEPARATOR . 'src', 0755);
            $io->write("  -> src directory created");

            if (!is_dir($app_basedir . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Controller')) {
                Tools::mkdir($app_basedir . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Controller');
                chmod($app_basedir . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Controller', 0755);
                $io->write("  -> src".DIRECTORY_SEPARATOR."Controller directory created");
            }

            if (!is_dir($app_basedir . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Response')) {
                Tools::mkdir($app_basedir . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Response');
                chmod($app_basedir . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Response', 0755);
                $io->write("  -> src".DIRECTORY_SEPARATOR."Response directory created");
            }

            $this->installFile(
                $source_file_dir . DIRECTORY_SEPARATOR . 'DefaultController.php.template',
                $app_basedir . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Controller' . DIRECTORY_SEPARATOR . 'DefaultController.php'
            );
            $io->write("  -> src".DIRECTORY_SEPARATOR."Controller".DIRECTORY_SEPARATOR."DefaultController.php created");

            $this->installFile(
                $source_file_dir . DIRECTORY_SEPARATOR . 'ApiController.php.template',
                $app_basedir . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Controller' . DIRECTORY_SEPARATOR . 'ApiController.php'
            );
            $io->write("  -> src".DIRECTORY_SEPARATOR."Controller".DIRECTORY_SEPARATOR."ApiController.php created");

            $this->installFile(
                $source_file_dir . DIRECTORY_SEPARATOR . 'ApiResponse.php.template',
                $app_basedir . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Response' . DIRECTORY_SEPARATOR . 'ApiResponse.php'
            );
            $io->write("  -> src".DIRECTORY_SEPARATOR."Response".DIRECTORY_SEPARATOR."ApiResponse.php created");

            $this->installFile(
                $source_file_dir . DIRECTORY_SEPARATOR . 'bootstrap.php',
                $app_basedir . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'bootstrap.php'
            );
            $io->write("  -> src".DIRECTORY_SEPARATOR."bootstrap.php created");
        }
    }


    protected function copyFile($source, $targetDir)
    {
        if (file_exists($source)) {
            copy($source, $targetDir . DIRECTORY_SEPARATOR . basename($source));
            $this->io->write("  -> ".basename($source)." copied");
        } else {
            $this->io->write("  ERROR : $source not found");
        }
    }


    /**
     * Create the targetFilename file using the sourceFilename as a template
     * replacing any variables with their project values
     *
     * @param string $sourceFilename
     * @param string $targetFilename
     *
     * @return bool
     */
    protected function installFile($sourceFilename, $targetFilename)
    {
        if (file_exists($sourceFilename)) {
            $template = file_get_contents($sourceFilename);

            $template = str_replace('{$app_basedir}', $this->app_basedir, $template);
            $template = str_replace('{$synergy_basedir}', $this->synergy_basedir, $template);
            if (isset($this->extra['namespace'])) {
                $namespace = $this->extra['namespace'];
                $template = str_replace('{$base_namespace}', $namespace, $template);
                list($safe, ) = preg_split('/[^a-zA-Z]/', $namespace);
                $safe = strtolower($safe);
                $template = str_replace('{$db_name}', $safe, $template);
            }

            $fp = fopen($targetFilename, 'w');
            fputs($fp, $template);
            @fclose($fp);

            return true;
        }

        return false;
    }
}
