<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Construct;

use ChrisNoden\Synergy\Request\WebRequest;
use ChrisNoden\Synergy\Routing\Route;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class SynergyContainer
 *
 * @category ChrisNoden\Synergy\Construct
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class SynergyContainer extends \Pimple
{
    protected $logger;

    public function __construct()
    {
        parent::__construct();

        // Our Logger
        $this['logger_object'] = new NullLogger();

        /** @noinspection PhpParamsInspection */
        $this['logger'] = function ($c) {
            return $c['logger_object'];
        };


        /**
         * @param $c
         *
         * @return Route
         */
        $this['route'] = function ($c) {
            $obj = new Route();
            $obj->setLogger($c['logger']);
            return $obj;
        };


        /**
         * @param $c
         *
         * @return WebRequest
         */
        $this['request'] = function ($c) {
            $obj = new WebRequest();
            $obj->setType($c['request_type']);
            $obj->setLogger($c['logger']);
            return $obj;
        };
    }


    /**
     * Set the Logger object
     *
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this['logger_object'] = $logger;
    }


    /**
     * Allow access to the container elements as methods
     * eg $this->logger() or $this->getLogger()
     *
     * @param string $method
     * @param array $args
     *
     * @return mixed
     */
    public function __call($method, $args)
    {
        if (isset($this[$method])) {
            return $this[$method];
        }
        if ("get" === substr($method, 0, 3) && $this[lcfirst(substr($method, 3))]) {
            return $this[lcfirst(substr($method, 3))];
        }
        return null;
    }
}
