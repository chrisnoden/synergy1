<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2015 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Helper;

/**
 * Class CollectionAbstract
 *
 * Creates an easy Collection class for when you want to iterate an array
 *
 * @category ChrisNoden\Synergy\Helper
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
abstract class CollectionAbstract implements \Iterator, \Countable
{
    /** @var int */
    protected $coll_pos = 0;
    /** @var array */
    protected $collection = array();


    public function __construct()
    {
        $this->coll_pos = 0;
    }


    /**
     * How many items are in the collection
     *
     * @return int
     */
    public function count()
    {
        return count($this->collection);
    }


    /**
     * Get the collection as a standard array
     *
     * @return array
     */
    public function asArray()
    {
        return $this->collection;
    }


    public function rewind()
    {
        $this->coll_pos = 0;
    }


    public function current()
    {
        return $this->collection[$this->coll_pos];
    }


    public function key()
    {
        return $this->coll_pos;
    }


    public function next()
    {
        ++$this->coll_pos;
    }


    public function valid()
    {
        return isset($this->collection[$this->coll_pos]);
    }
}
