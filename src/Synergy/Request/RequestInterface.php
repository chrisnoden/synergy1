<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Request;

use Psr\Log\LoggerInterface;

/**
 * Class RequestInterface
 *
 * @category ChrisNoden\Synergy\Request
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
interface RequestInterface
{

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger = null);

    /**
     * Build the Request properties
     *
     * @return $this
     */
    public function build();

    /**
     * Build the Request from the PHP Globals
     *
     * @return $this
     */
    public function buildFromGlobals();

    /**
     * @param array $params
     *
     * @return $this
     */
    public function setParameters(Array $params);

    /**
     * Add a parameter (or set a new value of an existing parameter)
     *
     * @param string $param_name
     * @param mixed $param_value
     *
     * @return $this
     */
    public function addParameter($param_name, $param_value);

    /**
     * @return array the request parameters (eg from GET and POST)
     */
    public function getParameters();

    /**
     * Get the value of the request parameter (taken from the GET/POST/arguments or from the route)
     *
     * @param string $param_name
     *
     * @return null|mixed
     */
    public function get($param_name);
}
