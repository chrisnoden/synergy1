<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Request;

use ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException;
use ChrisNoden\Synergy\Exception\SynergyRequestException;
use ChrisNoden\Synergy\Type\ProjectType;
use Psr\Log\LoggerAwareTrait;

/**
 * Class Request
 * Wrapper for incoming requests (eg Web requests or Console requests)
 *
 * @category ChrisNoden\Synergy\Routing
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class WebRequest extends RequestAbstract implements RequestInterface
{
    /** @var bool */
    protected $ssl = false;
    /** @var string */
    protected $scheme = 'http';
    /** @var string WebRequest hostname */
    protected $host;
    /** @var string WebRequest client user-agent */
    protected $user_agent;
    /** @var string WebRequest HTTP method (GET, POST, PUT, etc) */
    protected $method;
    /** @var string WebRequest querystring */
    protected $query_string;
    /** @var string bit after the hash in the request URI */
    protected $fragment;
    /** @var string request path */
    protected $path;
    /** @var string web referer */
    protected $referer;
    /** @var string original request body (if any) */
    protected $request_body;
    /** @var string filename of the script that intercepted the request */
    protected $script_filename;
    /** @var WebClient */
    protected $request_client;
    /** @var string HTTP request body encoding */
    protected $encoding;
    /** @var string boundary for the body parameters */
    protected $boundary;

    /** @var array original array of data which we built the request from */
    protected $source = array();
    /** @var array headers from the web client request */
    protected $request_headers = array();

    /** @var array valid method values */
    protected $valid_methods = array('GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'OPTIONS');


    /**
     * Build the Web Request using the PHP Globals
     *
     * @return $this
     * @throws \ChrisNoden\Synergy\Exception\SynergyRequestException
     */
    public function buildFromGlobals()
    {
        if (isset($_SERVER) && is_array($_SERVER) && isset($_SERVER['SERVER_NAME'])) {
            $this->source = $_SERVER;
        } else {
            throw new SynergyRequestException('Unable to build WebRequest from _SERVER data');
        }

        $this->build();

        return $this;
    }


    /**
     * Build the main Request data from the server data (usually in the $_SERVER super-global)
     *
     * @return $this
     * @throws SynergyRequestException
     */
    public function build()
    {
        if (empty($this->source)) {
            throw new SynergyRequestException('Must set CommandLine before building Request');
        }

        // build our nice array of browser request headers
        $this->request_headers = $this->buildRequestHeaders();

        if (isset($this->source['SERVER_NAME'])) {
            $this->host = $this->source['SERVER_NAME'];
        }
        if (isset($this->source['HTTP_USER_AGENT'])) {
            $this->user_agent = $this->source['HTTP_USER_AGENT'];
        }
        if (isset($this->source['REQUEST_METHOD'])) {
            $this->setMethod($this->source['REQUEST_METHOD']);
        }
        if (isset($this->source['REDIRECT_QUERY_STRING'])) {
            $this->query_string = $this->source['REDIRECT_QUERY_STRING'];
            $this->parseQueryStringParameters($this->query_string);
        } elseif (isset($this->source['QUERY_STRING'])) {
            $this->query_string = $this->source['QUERY_STRING'];
            $this->parseQueryStringParameters($this->query_string);
        }
        if (isset($this->source['FCGI_ROLE']) && $this->source['FCGI_ROLE'] == 'RESPONDER') {
            $full = $this->source['REQUEST_URI'];
            list($this->path, ) = explode('?', $full, 2);
        } elseif (isset($this->source['REDIRECT_URL'])) {
            $this->path = $this->source['REDIRECT_URL'];
        } elseif (isset($this->source['SCRIPT_NAME'])) {
            $this->path = $this->source['SCRIPT_NAME'];
        } elseif (isset($this->source['PHP_SELF'])) {
            $this->path = $this->source['PHP_SELF'];
        }
        if (isset($this->source['HTTP_REFERER'])) {
            $this->referer = $this->source['HTTP_REFERER'];
        }
        if (isset($this->source['SCRIPT_FILENAME'])) {
            $this->script_filename = $this->source['SCRIPT_FILENAME'];
        }
        if ((isset($this->source['HTTPS']) && $this->source['HTTPS'] == 'on') ||
            (isset($this->source['HTTP_X_FORWARDED_PROTO']) && $this->source['HTTP_X_FORWARDED_PROTO'] == 'https')
        ) {
            $this->ssl = true;
        }
        if (isset($this->source['CONTENT_TYPE'])) {
            $this->parseContentType($this->source['CONTENT_TYPE']);
        }

        switch ($this->method) {
            case 'DELETE':
            case 'PUT':
            case 'POST':
                if (function_exists('http_get_request_body')) {
                    $this->setRequestBody(http_get_request_body());
                } elseif (PHP_SAPI !== 'cli') {
                    $this->setRequestBody(file_get_contents('php://input'));
                }
                $this->fetchBodyParameters();

                break;
        }

        // Profile the web client
        $this->request_client = WebClient::buildClientData($this->user_agent, $this->source);

        return $this;
    }


    /**
     * Parse the full, raw HTTP request (header and body)
     * and populate the Request object
     *
     * @param string $raw_request
     *
     * @return $this
     */
    public function parseRawWebRequest($raw_request)
    {
        list($raw_header, $raw_body) = preg_split("/\r\n\r\n/", $raw_request, 2);

        $this->parseRawWebHeader($raw_header);
        $this->parseRawWebBody($raw_body);

        return $this;
    }


    /**
     * Parse the full, raw HTTP request header
     *
     * @param string $raw_header
     *
     * @return $this
     */
    protected function parseRawWebHeader($raw_header)
    {
        // parse the header
        $arr = preg_split("/\r\n/", $raw_header);
        foreach ($arr as $linenum => $hdr_line) {
            if ($linenum == 0) {
                list ($method, $path, $http) = explode(' ', $hdr_line, 3);
                $this->setMethod($method);

                list ($path, $query) = explode('?', $path);
                $this->path = $path;
                if (isset($query)) {
                    $this->query_string = $query;
                    parse_str($query, $parameters);
                    $this->setParameters(array_merge($this->getParameters(), $parameters));
                }

                continue;
            }

            list($key, $val) = preg_split('/:\ /', $hdr_line);

            switch (strtolower($key)) {
                case 'host':
                    $this->host = $val;
                    break;

                case 'content-type':
                    $this->parseContentType($val);
                    break;
            }
        }

        return $this;
    }


    /**
     * Parse the value of the Content-Type request header
     *
     * @param string $contentType
     */
    protected function parseContentType($contentType)
    {
        if (preg_match('/(multipart\/form-data);\ boundary=(.*)/', $contentType, $matches)) {
            $this->encoding = $matches[1];
            $this->boundary = $matches[2];
        }
        if (preg_match('/(application\/x-www-form-urlencoded)/', $contentType)) {
            $this->encoding = 'application/x-www-form-urlencoded';
        }
    }


    /**
     * Parse the raw HTTP request body
     *
     * @param string $raw_body
     *
     * @return $this
     */
    protected function parseRawWebBody($raw_body)
    {
        // parse the body
        if (isset($this->boundary)) {
            $pattern = '/'.$this->boundary.'/';
            $arr = preg_split($pattern, $raw_body);
            foreach ($arr as $raw_param) {
                if (preg_match('/[^\"]+\"([^\"]+)\"[\r\n]+([^\r\n]+)/', $raw_param, $matches)) {
                    $this->addParameter($matches[1], $matches[2]);
                }
            }
        } elseif (isset($this->encoding) && $this->encoding == 'application/x-www-form-urlencoded') {
            $this->parseQueryStringParameters($raw_body);
        }

        return $this;
    }


    /**
     * Look for the web client request headers (prefixed with HTTP_)
     * parse their correct name and return as an associative array
     *
     * @return array
     */
    protected function buildRequestHeaders()
    {
        $arr = array();
        foreach ($this->source as $key => $val) {
            if (substr($key, 0, 5) == 'HTTP_') {
                $name = preg_replace('/ /', '-', ucwords(strtolower(preg_replace('/\_/', ' ', substr($key, 5)))));
                $arr[$name] = $val;
            }
        }

        return $arr;
    }


    /**
     * Add the parameters in the query string to our object parameters
     *
     * @param string $query_string
     */
    protected function parseQueryStringParameters($query_string)
    {
        $arr = array();
        parse_str($query_string, $arr);
        $this->parameters = array_merge($this->parameters, $arr);
    }


    /**
     * Build the request parameters from the $_POST array
     * or from the request_body as appropriate
     *
     * @return void
     */
    protected function fetchBodyParameters()
    {
        if ($this->method == 'POST') {
            foreach ($_POST as $key => $val) {
                $this->addParameter($key, $val);
            }
        } elseif (!is_null($this->request_body)) {
            $this->parseRawWebBody($this->request_body);
        }
    }


    /**
     * Set the request body content (eg from a POST or PUT request)
     *
     * @param string $body_content
     *
     * @return $this
     */
    public function setRequestBody($body_content)
    {
        $this->request_body = $body_content;

        return $this;
    }


    /**
     * The body of the request (from a POST or PUT)
     *
     * @return string
     */
    public function getBody()
    {
        return $this->request_body;
    }


    /**
     * Set the HTTP method of the Request
     *
     * @param string $method one of 'GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'OPTIONS'
     *
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException
     * @return $this
     */
    public function setMethod($method)
    {
        $method = strtoupper($method);
        if (!in_array($method, $this->valid_methods)) {
            throw new SynergyInvalidArgumentException($method.' is not an accepted HTTP method');
        }
        $this->method = $method;

        return $this;
    }


    /**
     * Set the user agent string
     *
     * @param string $user_agent
     *
     * @return $this
     */
    public function setUserAgent($user_agent)
    {
        $this->user_agent = $user_agent;

        // Profile the web client
        $this->request_client = WebClient::buildClientData($this->user_agent, $this->source);

        return $this;
    }


    /**
     * Populate the Request parameters from a URL
     *
     * @param string $uri
     *
     * @return $this
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException
     */
    public function setUri($uri)
    {
        if ($parts = parse_url($uri)) {
            if (isset($parts['scheme']) && $parts['scheme'] == 'https') {
                $this->scheme = 'https';
                $this->ssl = true;
            }
            if (isset($parts['host'])) {
                $this->host = $parts['host'];
            }
            if (isset($parts['path'])) {
                $this->path = $parts['path'];
            } else {
                $this->path = '/';
            }
            if (isset($parts['query'])) {
                $this->query_string = $parts['query'];
                $this->parseQueryStringParameters($this->query_string);
            }
            if (isset($parts['fragment'])) {
                $this->fragment = $parts['fragment'];
            }
            $this->setMethod('GET');

        } else {
            throw new SynergyInvalidArgumentException('Invalid URI');
        }

        return $this;
    }


    /**
     * @param ProjectType $type
     *
     * @return $this
     */
    public function setType(ProjectType $type)
    {
        $this->project_type = $type;

        return $this;
    }


    /**
     * Set the params to use instead of the PHP globals
     *
     * @param array $server_params
     *
     * @return $this
     */
    public function setServerParams(Array $server_params)
    {
        $this->source = $server_params;

        return $this;
    }


    /**
     * @return ProjectType
     */
    public function getType()
    {
        return $this->project_type;
    }


    /**
     * Constructs the requested URI from the components of the Request
     *
     * @return string
     */
    public function getUri()
    {
        $uri = '';
        if (isset($this->host)) {
            if ($this->ssl) {
                $uri .= 'https://'.$this->host;
            } else {
                $uri .= 'http://'.$this->host;
            }
        }
        if (isset($this->path)) {
            $uri .= $this->path;
        }
        if (isset($this->query_string)) {
            $uri .= '?'.$this->query_string;
        }
        if (isset($this->fragment)) {
            $uri .= '#'.$this->fragment;
        }

        return $uri;
    }


    /**
     * @return string Host / Hostname from the request
     */
    public function getHost()
    {
        return $this->host;
    }


    /**
     * @return string the HTTP method (GET, POST, PUT, DELETE, etc)
     */
    public function getMethod()
    {
        return $this->method;
    }


    /**
     * @return string browser user-agent string
     */
    public function getUserAgent()
    {
        return $this->user_agent;
    }


    /**
     * @return string web querystring (if any)
     */
    public function getQueryString()
    {
        return $this->query_string;
    }


    /**
     * @return string request path
     */
    public function getPath()
    {
        return $this->path;
    }


    /**
     * @return WebClient
     */
    public function getRequestClient()
    {
        if (!$this->request_client instanceof WebClient) {
            $this->request_client = new WebClient();
        }
        return $this->request_client;
    }


    /**
     * @return array
     */
    public function getRequestHeaders()
    {
        return $this->request_headers;
    }


    /**
     * Value of the named request header
     *
     * @param string $header_name
     *
     * @return null|string
     */
    public function getRequestHeader($header_name)
    {
        if (isset($this->request_headers[$header_name])) {
            return $this->request_headers[$header_name];
        }

        return null;
    }


    /**
     * @return string
     */
    public function getReferer()
    {
        return $this->referer;
    }


    /**
     * @return bool is this web request using SSL
     */
    public function getSsl()
    {
        return $this->ssl;
    }


    /**
     * Value of member scheme
     *
     * @return string http or https
     */
    public function getScheme()
    {
        return $this->scheme;
    }


    /**
     * @return string the URI fragment after the #
     */
    public function getFragment()
    {
        return $this->fragment;
    }


    /**
     * Set a request header value
     *
     * @param string $name
     * @param string $value
     *
     * @return $this
     */
    public function setHeader($name, $value)
    {
        $this->request_headers[$name] = $value;

        return $this;
    }
}
