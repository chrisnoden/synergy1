<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Synergy\Request;

use ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException;
use Mobile_Detect;
use Psr\Log\LoggerAwareTrait;

/**
 * Class WebClient
 * Container for information about the Web Client
 *
 * @category ChrisNoden\Synergy\Routing
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class WebClient
{

    use LoggerAwareTrait;

    /** @var string */
    protected $user_agent;
    /** @var array */
    protected $request_headers = array();
    /** @var \Mobile_Detect */
    protected $mobile_detect;
    /** @var string */
    protected $device;
    /** @var string */
    protected $platform;
    /** @var string */
    protected $browser;
    /** @var string */
    protected $browser_version;


    /**
     * Profile the Client based on the supplied parameters
     *
     * @param string $user_agent
     * @param array  $request_headers suggested to use the $_SERVER super-global
     *
     * @return WebClient
     */
    public static function buildClientData($user_agent, Array $request_headers)
    {
        $client = new self();
        $client->setRequestHeaders($request_headers);
        $client->setUserAgent($user_agent);
        $client->buildData();

        return $client;
    }


    /**
     * Parse all the data to profile the Client
     *
     * @return $this
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException
     */
    public function buildData()
    {
        if (!isset($this->user_agent) || !isset($this->request_headers)) {
            throw new SynergyInvalidArgumentException('setUserAgent and setRequestHeaders before calling buildData()');
        }

        // parse for mobile/tablet device
        $this->mobile_detect = new \Mobile_Detect($this->request_headers, $this->user_agent);
        // parse the user-agent platform, browser and browser-version
        $arr = $this->parseUserAgent($this->user_agent);
        $this->platform = $arr['platform'];
        $this->browser = $arr['browser'];
        $this->browser_version = $arr['version'];

        // set the device type
        if ($this->mobile_detect->isMobile() && !$this->mobile_detect->isTablet()) {
            $this->device = 'mobile';
        } elseif ($this->mobile_detect->isTablet()) {
            $this->device = 'tablet';
        } elseif (!$this->mobile_detect->isMobile() && !$this->mobile_detect->isTablet()) {
            $this->device = 'desktop';
        }

        // check if it's actually a console (experimental)
        if (stristr($this->getPlatform(), 'playstation')) {
            $this->device = 'console';
        } elseif (stristr($this->getPlatform(), 'xbox')) {
            $this->device = 'console';
        } elseif (stristr($this->getPlatform(), 'nintendo')) {
            $this->device = 'console';
        }

        return $this;
    }


    /**
     * Set the Request Headers
     *
     * @param array $request_headers
     *
     * @return $this
     */
    public function setRequestHeaders(Array $request_headers)
    {
        $this->request_headers = $request_headers;

        return $this;
    }


    /**
     * Set the Browser User Agent string
     *
     * @param string $user_agent
     *
     * @return $this
     */
    public function setUserAgent($user_agent)
    {
        $this->user_agent = $user_agent;

        return $this;
    }


    /**
     * The Web Browser detected in the User-Agent
     *
        Android Browser
        BlackBerry Browser
        Camino
        Kindle / Silk
        Firefox / Iceweasel
        Safari
        MSIE
        IEMobile
        Chrome
        Opera
        Midori
        Lynx
        Wget
        Curl
     *
     * @return string value of member
     */
    public function getBrowser()
    {
        return $this->browser;
    }


    /**
     * The Browser Version detected in the User-Agent
     *
     * @return string value of member
     */
    public function getBrowserVersion()
    {
        return $this->browser_version;
    }


    /**
     * The Platform (Operating System) detected in the User-Agent
     *  Desktop
            Windows
            Linux
            Macintosh
            Chrome OS
        Mobile
            Android
            iPhone
            iPad
            Windows Phone OS
            Kindle
            Kindle Fire
            BlackBerry
            Playbook
        Console
            Nintendo 3DS
            Nintendo Wii
            Nintendo WiiU
            PlayStation 3
            PlayStation 4
            PlayStation Vita
            Xbox 360
     *
     * @return string value of member
     */
    public function getPlatform()
    {
        return $this->platform;
    }


    /**
     * @return bool
     */
    public function isMobile()
    {
        return $this->device == 'mobile';
    }


    /**
     * @return bool
     */
    public function isTablet()
    {
        return $this->device == 'tablet';
    }


    /**
     * @return bool
     */
    public function isDesktop()
    {
        return $this->device == 'desktop';
    }


    /**
     * incomplete Console test
     *
     * @return bool
     */
    public function isConsole()
    {
        return $this->device == 'console';
    }


    /**
     * @return string
     */
    public function getDevice()
    {
        return $this->device;
    }


    /**
     * Parses a user agent string into its important parts
     *
     * @author Jesse G. Donat <donatj@gmail.com>
     * @link https://github.com/donatj/PhpUserAgent
     * @link http://donatstudios.com/PHP-Parser-HTTP_USER_AGENT
     *
     * @param string $u_agent user agent string
     *
     * @return array an array with browser, version and platform keys
     */
    protected function parseUserAgent($u_agent)
    {
        $platform = null;
        $browser  = null;
        $version  = null;

        $empty = array( 'platform' => $platform, 'browser' => $browser, 'version' => $version );

        if( !$u_agent ) return $empty;

        if( preg_match('/\((.*?)\)/im', $u_agent, $parent_matches) ) {

            preg_match_all('/(?P<platform>BB\d+;|Android|CrOS|iPhone|iPad|Linux|Macintosh|Windows(\ Phone)?|Silk|linux-gnu|BlackBerry|PlayBook|Nintendo\ (WiiU?|3DS)|Xbox(\ One)?)
				(?:\ [^;]*)?
				(?:;|$)/imx', $parent_matches[1], $result, PREG_PATTERN_ORDER);

            $priority           = array( 'Android', 'Xbox One', 'Xbox' );
            $result['platform'] = array_unique($result['platform']);
            if( count($result['platform']) > 1 ) {
                if( $keys = array_intersect($priority, $result['platform']) ) {
                    $platform = reset($keys);
                } else {
                    $platform = $result['platform'][0];
                }
            } elseif( isset($result['platform'][0]) ) {
                $platform = $result['platform'][0];
            }
        }

        if( $platform == 'linux-gnu' ) {
            $platform = 'Linux';
        } elseif( $platform == 'CrOS' ) {
            $platform = 'Chrome OS';
        }

        preg_match_all('%(?P<browser>Camino|Kindle(\ Fire\ Build)?|Firefox|Iceweasel|Safari|MSIE|Trident/.*rv|AppleWebKit|Chrome|IEMobile|Opera|OPR|Silk|Lynx|Midori|Version|Wget|curl|NintendoBrowser|PLAYSTATION\ (\d|Vita)+)
			(?:\)?;?)
			(?:(?:[:/ ])(?P<version>[0-9A-Z.]+)|/(?:[A-Z]*))%ix',
            $u_agent, $result, PREG_PATTERN_ORDER);


        // If nothing matched, return null (to avoid undefined index errors)
        if( !isset($result['browser'][0]) || !isset($result['version'][0]) ) {
            return $empty;
        }

        $browser = $result['browser'][0];
        $version = $result['version'][0];

        $find = function ( $search, &$key ) use ( $result ) {
            $xkey = array_search(strtolower($search), array_map('strtolower', $result['browser']));
            if( $xkey !== false ) {
                $key = $xkey;

                return true;
            }

            return false;
        };

        $key = 0;
        if( $browser == 'Iceweasel' ) {
            $browser = 'Firefox';
        } elseif( $find('Playstation Vita', $key) ) {
            $platform = 'PlayStation Vita';
            $browser  = 'Browser';
        } elseif( $find('Kindle Fire Build', $key) || $find('Silk', $key) ) {
            $browser  = $result['browser'][$key] == 'Silk' ? 'Silk' : 'Kindle';
            $platform = 'Kindle Fire';
            if( !($version = $result['version'][$key]) || !is_numeric($version[0]) ) {
                $version = $result['version'][array_search('Version', $result['browser'])];
            }
        } elseif( $find('NintendoBrowser', $key) || $platform == 'Nintendo 3DS' ) {
            $browser = 'NintendoBrowser';
            $version = $result['version'][$key];
        } elseif( $find('Kindle', $key) ) {
            $browser  = $result['browser'][$key];
            $platform = 'Kindle';
            $version  = $result['version'][$key];
        } elseif( $find('OPR', $key) ) {
            $browser = 'Opera Next';
            $version = $result['version'][$key];
        } elseif( $find('Opera', $key) ) {
            $browser = 'Opera';
            $find('Version', $key);
            $version = $result['version'][$key];
        } elseif( $find('Midori', $key) ) {
            $browser = 'Midori';
            $version = $result['version'][$key];
        } elseif( $find('Chrome', $key) ) {
            $browser = 'Chrome';
            $version = $result['version'][$key];
        } elseif( $browser == 'AppleWebKit' ) {
            if( ($platform == 'Android' && !($key = 0)) ) {
                $browser = 'Android Browser';
            } elseif( strpos($platform, 'BB') === 0 ) {
                $browser  = 'BlackBerry Browser';
                $platform = 'BlackBerry';
            } elseif( $platform == 'BlackBerry' || $platform == 'PlayBook' ) {
                $browser = 'BlackBerry Browser';
            } elseif( $find('Safari', $key) ) {
                $browser = 'Safari';
            }

            $find('Version', $key);

            $version = $result['version'][$key];
        } elseif( $browser == 'MSIE' || strpos($browser, 'Trident') !== false ) {
            if( $find('IEMobile', $key) ) {
                $browser = 'IEMobile';
            } else {
                $browser = 'MSIE';
                $key     = 0;
            }
            $version = $result['version'][$key];
        } elseif( $key = preg_grep('/playstation \d/i', array_map('strtolower', $result['browser'])) ) {
            $key = reset($key);

            $platform = 'PlayStation ' . preg_replace('/[^\d]/i', '', $key);
            $browser  = 'NetFront';
        }

        return array( 'platform' => $platform, 'browser' => $browser, 'version' => $version );
    }
}
 