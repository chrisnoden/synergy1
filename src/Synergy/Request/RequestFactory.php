<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Request;

use ChrisNoden\Synergy\Exception\SynergyException;
use ChrisNoden\Synergy\Type\ProjectType;
use Psr\Log\LoggerInterface;

/**
 * Class RequestFactory
 * Instantiate the correct Request object
 *
 * @category ChrisNoden\Synergy\Request
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class RequestFactory
{

    /**
     * Create a new Request object using the PHP super-globals ($_SERVER, etc)
     *
     * @param ProjectType     $type
     * @param LoggerInterface $logger
     *
     * @return RequestInterface
     * @throws \ChrisNoden\Synergy\Exception\SynergyException
     * @throws \ChrisNoden\Synergy\Exception\SynergyRequestException
     */
    public static function create(ProjectType $type, LoggerInterface $logger = null)
    {
        switch ($type) {
            /** @noinspection PhpUndefinedMethodInspection */
            case ProjectType::WEB_PROJECT():
                $request = new WebRequest($logger);
                $request->buildFromGlobals();
                break;

            /** @noinspection PhpUndefinedMethodInspection */
            case ProjectType::CONSOLE_PROJECT():
                $request = new ConsoleRequest($logger);
                $request->buildFromGlobals();
                break;

            default:
                throw new SynergyException('Unsupported RequestType');
        }

        return $request;
    }

}
 