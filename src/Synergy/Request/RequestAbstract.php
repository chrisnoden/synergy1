<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Request;

use Psr\Log\LoggerAwareTrait;
use ChrisNoden\Synergy\Type\ProjectType;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Abstract Class RequestAbstract
 *
 * @category ChrisNoden\Synergy\Routing
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
abstract class RequestAbstract
{
    use LoggerAwareTrait;

    /** @var ProjectType */
    protected $project_type;
    /** @var array */
    protected $parameters = array();


    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger = null)
    {
        if (is_null($logger)) {
            $this->logger = new NullLogger();
        } else {
            $this->logger = $logger;
        }
    }


    /**
     * Build the Request properties
     *
     * @return $this
     */
    abstract public function build();


    /**
     * Build the Request from the PHP Globals
     *
     * @return $this
     */
    abstract public function buildFromGlobals();


    /**
     * @param array $params
     *
     * @return $this
     */
    public function setParameters(Array $params)
    {
        $this->parameters = $params;

        return $this;
    }


    /**
     * Add a parameter (or set a new value of an existing parameter)
     *
     * @param string $param_name
     * @param mixed $param_value
     *
     * @return $this
     */
    public function addParameter($param_name, $param_value)
    {
        $this->parameters[$param_name] = $param_value;

        return $this;
    }


    /**
     * @return array the request parameters (eg from GET and POST)
     */
    public function getParameters()
    {
        return $this->parameters;
    }


    /**
     * Get the value of the request parameter (taken from the GET/POST or request body)
     *
     * @param string $param_name
     *
     * @return null|mixed
     */
    public function get($param_name)
    {
        if (isset($this->parameters[$param_name])) {
            return $this->parameters[$param_name];
        }

        return null;
    }
}
