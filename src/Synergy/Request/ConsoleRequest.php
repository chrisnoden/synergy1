<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Request;

use ChrisNoden\Synergy\Console\Arguments;
use ChrisNoden\Synergy\Exception\SynergyRequestException;

/**
 * Class ConsoleRequest
 *
 * @category ChrisNoden\Synergy\Request
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class ConsoleRequest extends RequestAbstract implements RequestInterface
{
    /** @var Arguments */
    protected $arguments;
    /** @var array */
    protected $args = array();


    /**
     * Parse the command line arguments and store in the arguments property
     *
     * @return $this
     * @throws SynergyRequestException
     */
    public function build()
    {
        if (empty($this->args)) {
            throw new SynergyRequestException('Must set arguments before building Request');
        }
        $this->arguments = new Arguments();
        $this->arguments->parse($this->args);

        return $this;
    }


    /**
     * Build the Request from the PHP super globals
     *
     * @return $this
     */
    public function buildFromGlobals()
    {
        $this->arguments = new Arguments();
        $this->arguments->parse();

        return $this;
    }


    /**
     * Set the command line arguments to use
     *
     * @param array $args
     */
    public function setArgs(Array $args)
    {
        $this->args = $args;
    }


    /**
     * @return Arguments
     */
    public function getArguments()
    {
        return $this->arguments;
    }


    /**
     * Get the value of the command property (taken from the args)
     *
     * @param string $param_name
     *
     * @return null|mixed
     */
    public function get($param_name)
    {
        if ($this->arguments->option($param_name)) {
            return $this->arguments->option($param_name);
        } elseif ($this->arguments->arg($param_name)) {
            return $this->arguments->arg($param_name);
        }

        return null;
    }
}
