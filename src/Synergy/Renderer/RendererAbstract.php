<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Renderer;

/**
 * Class RendererAbstract
 *
 * @category ChrisNoden\Synergy\Renderer
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
abstract class RendererAbstract
{

    /** @var array source data */
    protected $data = array();
    /** @var mixed the output render of the data */
    protected $render;


    /**
     * Render the data as JSON
     *
     * @param array $data
     *
     * @return string
     */
    public static function render(Array $data)
    {
        $obj = new static();

        $obj->setData($data);

        return $obj->getRender();
    }


    /**
     * Render the source data as the target output
     *
     * @return void
     */
    abstract protected function renderData();


    /**
     * @param array $data
     *
     * @return RendererInterface
     */
    public function setData(Array $data)
    {
        $this->data = $data;

        return $this;
    }


    /**
     * Get the source data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }


    /**
     * Get the output render of the data
     *
     * @return mixed
     */
    public function getRender()
    {
        if (is_null($this->render)) {
            $this->renderData();
        }

        return $this->render;
    }
}
