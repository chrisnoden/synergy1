<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Renderer;

/**
 * Class XmlRenderer
 *
 * @category ChrisNoden\Synergy\Renderer
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class XmlRenderer extends RendererAbstract implements RendererInterface
{


    /**
     * Render the source data as the target output
     *
     * @return void
     */
    protected function renderData()
    {
        $this->render = $this->convertArrayToXml($this->data);
    }


    /**
     * @param array $array
     * @param int   $level
     *
     * @return string
     */
    private function convertArrayToXml(Array $array, $level = 0)
    {
        if ($level == 0) {
            $xml = "<?xml version='1.0' standalone='yes'?>\n";
            $xml .= "<data>\n";
        } else {
            $xml = '';
        }

        $prefix = '';
        for ($t=0; $t<$level; $t++) {
            $prefix .= "\t";
        }
        // set initial value for XML string
        foreach ($array as $key => $value) {
            if (preg_match('/^[0-9]+$/', $key)) {
                $key = 'item'.$key;
            }
            if (is_array($value)) {
                $xml .= sprintf($prefix."<%s>\n%s".$prefix."</%s>\n", $key, $this->convertArrayToXml($value, $level+1), $key);
            } else {
                // append to XML string
                $xml .= sprintf($prefix."<%s>%s</%s>\n", $key, htmlentities($value), $key);
            }
        }

        if ($level == 0) {
            $xml .= "</data>";
        }

        // return XML
        return $xml;
    }
}
 