<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Renderer;

/**
 * Class CsvRenderer
 *
 * @category ChrisNoden\Synergy\Renderer
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class CsvRenderer extends RendererAbstract implements RendererInterface
{


    /**
     * Render the source data as the target output
     *
     * @return void
     */
    protected function renderData()
    {
        $this->render = $this->getDataAsCsv($this->data);
    }


    /**
     * Get the request data as CSV
     *
     * @param array  $data
     * @param string $delimiter
     * @param string $enclosure
     *
     * @return string
     */
    public function getDataAsCsv($data, $delimiter = ',', $enclosure = '"')
    {
        $handle = fopen('php://temp', 'r+');
        foreach ($data as $tag => $line) {
            if ($tag === '_metadata') {
                continue;
            }
            if (!is_array($line)) {
                $line = array($line);
            }
            // can't break nested arrays down, so skip those
            foreach ($line as $key => $val) {
                if (is_array($val)) {
                    continue(2);
                }
            }
            fputcsv($handle, $line, $delimiter, $enclosure);
        }
        rewind($handle);
        $contents = '';
        while (!feof($handle)) {
            $contents .= fread($handle, 8192);
        }
        fclose($handle);

        return $contents;
    }
}
