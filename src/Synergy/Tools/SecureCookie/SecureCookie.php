<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Tools\SecureCookie;

use ChrisNoden\Synergy\Exception\SynergyException;
use ChrisNoden\Synergy\Tools\Tools;

/**
 * Class SecureCookie
 *
 * @category ChrisNoden\Synergy\Tools\SecureCookie
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class SecureCookie
{

    /**
     * @var string encryption token
     */
    private static $token = '';
    /** @var string */
    private static $cookie_host;


    /**
     * @param string $cookie_host
     */
    public static function setCookieHost($cookie_host)
    {
        self::$cookie_host = $cookie_host;
    }


    /**
     * @return string
     */
    public static function getCookieHost()
    {
        return self::$cookie_host;
    }


    /**
     * Set an encryption token to improve the security
     *
     * @param string $token
     */
    public static function setToken($token)
    {
        self::$token = $token;
    }


    /**
     * Store an encrypted cookie
     *
     * @param string $cookieName
     * @param mixed  $cookieValue
     * @param int    $expiry default stores just for the browser session
     */
    public static function set($cookieName, $cookieValue, $expiry = 0)
    {
        if (isset($_COOKIE['synsec'])) {
            $synsec = $_COOKIE['synsec'];
        } else {
            $synsec = Tools::randomString('12');
        }

        if ((!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on') &&
            (!isset($_SERVER['HTTP_X_FORWARDED_PROTO']) || $_SERVER['HTTP_X_FORWARDED_PROTO'] != 'https')
        ) {
            $ssl = false;
        } else {
            $ssl = true;
        }

        setcookie('synsec', $synsec, $expiry, '/', SecureCookie::getCookieHost(), $ssl, true);

        $synsec .= 'synErgy' . self::$token;

        /* Open the cipher */
        $td = mcrypt_module_open('rijndael-256', '', 'ofb', '');

        /* Create the IV and determine the keysize length, use MCRYPT_RAND
         * on Windows instead */
        // MCRYPT_DEV_URANDOM is less secure but faster on our restricted virtual servers
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_DEV_URANDOM);
        $ks = mcrypt_enc_get_key_size($td);

        /* Create key */
        $key = substr(md5($synsec), 0, $ks);

        /* Intialize encryption */
        mcrypt_generic_init($td, $key, $iv);

        /* Encrypt data */
        $encrypted = mcrypt_generic($td, serialize($cookieValue));

        # Store our secure cookie
        setcookie(
            $cookieName,
            base64_encode($iv . '|' . $encrypted),
            $expiry,
            '/',
            SecureCookie::getCookieHost(),
            $ssl,
            true
        );

        /* Terminate encryption handler */
        mcrypt_generic_deinit($td);
    }


    /**
     * Get the value of the encrypted cookie
     *
     * @param $cookieName
     *
     * @return mixed|null
     * @throws SynergyException
     */
    public static function get($cookieName)
    {
        if (isset($_COOKIE['synsec'])) {
            $synsec = $_COOKIE['synsec'] . 'synErgy' . self::$token;
        } else {
            return null;
        }

        if (isset($_COOKIE[$cookieName])) {
            $val = base64_decode($_COOKIE[$cookieName]);
            if (preg_match('/\|/', $val)) {
                # Decode our string
                list ($iv, $cipherText) = explode('|', $val, 2);
            } else {
                return null;
            }
        } else {
            return null;
        }

        if (strlen($iv) != 32) {
            self::delete($cookieName);
            return null;
        }

        /* Open the cipher */
        $td = mcrypt_module_open('rijndael-256', '', 'ofb', '');
        $ks = mcrypt_enc_get_key_size($td);

        /* Create key */
        $key = substr(md5($synsec), 0, $ks);
        /* Initialize encryption module for decryption */
        mcrypt_generic_init($td, $key, $iv);

        /* Decrypt encrypted string */
        $decrypted = mdecrypt_generic($td, $cipherText);

        /* Terminate decryption handle and close module */
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);

        # Return the clearText
        return unserialize(trim($decrypted));
    }


    /**
     * Delete a secure cookie
     *
     * @param string $cookieName
     */
    public static function delete($cookieName)
    {
        if ((!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on') &&
            (!isset($_SERVER['HTTP_X_FORWARDED_PROTO']) || $_SERVER['HTTP_X_FORWARDED_PROTO'] != 'https')
        ) {
            $ssl = false;
        } else {
            $ssl = true;
        }

        setcookie($cookieName, null, time() + 60 * 60 * 24 * 30, '/', SecureCookie::getCookieHost(), $ssl, true);
    }
}
