<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either expressed or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   mse
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2015 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Tools\SessionHandler;

use Predis\Client as PredisClient;

/**
 * Class RedisSessionHandler
 *
 * @category ChrisNoden\Synergy\Tools\SessionHandler
 * @package  mse
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class RedisSessionHandler extends SessionHandlerAbstract implements \SessionHandlerInterface
{
    public $ttl = 43200; // 12 hours
    protected $db;
    protected $prefix;

    public function __construct(PredisClient $db, $prefix = 'PHPSESSID:') {
        $this->db = $db;
        $this->prefix = $prefix;
    }

    public function open($savePath, $sessionName) {
        // No action necessary because connection is injected
        // in constructor and arguments are not applicable.
        return true;
    }

    public function close() {
        $this->db = null;
        unset($this->db);
        return true;
    }

    public function read($id) {
        $id = $this->prefix . $id;
        $sessData = $this->db->get($id);
        $this->db->expire($id, $this->ttl);
        return $sessData;
    }

    public function write($id, $data) {
        $id = $this->prefix . $id;
        $this->db->set($id, $data);
        $this->db->expire($id, $this->ttl);
        return true;
    }

    public function destroy($id) {
        $this->db->del($this->prefix . $id);
        return true;
    }

    public function gc($maxLifetime) {
        // no action necessary because using EXPIRE
        return true;
    }
}
