<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Console;

use ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException;

/**
 * Class Arguments
 * Parse console arguments for use in our classes
 *
 * @category ChrisNoden\Synergy\Console
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class Arguments
{

    /**
     * @var array
     */
    protected $aArgs = array();
    /**
     * @var array
     */
    protected $aSwitches = array();
    /**
     * @var array
     */
    protected $aNodes = array();


    /**
     * Parses out all the command line to find the requested controller plus
     * any arguments which are available to both Synergy framework
     * classes and to the Controller class
     *
     * @param array $args line of args to be parsed (if null then taken from command line)
     *
     * @return $this
     * @throws SynergyInvalidArgumentException
     */
    public function parse($args = array())
    {
        if (empty($args) && isset($_SERVER['argv']) && is_array($_SERVER['argv'])) {
            $aArgs = $_SERVER['argv'];
        } elseif (!empty($args)) {
            $aArgs = $args;
        } else {
            throw new SynergyInvalidArgumentException('Invalid input for Arguments');
        }

        if (isset($_SERVER['SCRIPT_FILENAME'])) {
            $script_filename = strtolower($_SERVER['SCRIPT_FILENAME']);
        } else {
            $script_filename = '';
        }

        $lastSwitch = null;
        foreach ($aArgs as $val)
        {
            if ($val == $script_filename) {
                continue;
            }
            if (preg_match('/^[\-]{2,}([a-zA-Z0-9]+)[\=]{0,1}(.*)$/', $val, $matches)) {
                $this->aArgs[$matches[1]] = $matches[2];
                $lastSwitch = null;
            } elseif (substr($val, 0, 1) == '-') {
                if (preg_match('/^\-([a-zA-Z0-9]+)\=(.*)$/', $val, $matches)) {
                    $this->aSwitches[$matches[1]] = $matches[2];
                    $lastSwitch = null;
                } else {
                    $this->aSwitches[substr($val, 1)] = true;
                    $lastSwitch = substr($val, 1);
                }
            } else {
                if (!is_null($lastSwitch)) {
                    $this->aSwitches[$lastSwitch] = $val;
                    $lastSwitch = null;
                } else {
                    $this->aNodes[] = $val;
                }
            }
        }

        return $this;
    }


    /**
     * Look in the command line args for the argument and return the value if found
     *
     * @param string $name
     *
     * @return mixed|bool
     */
    public function arg($name)
    {
        foreach ($this->aArgs as $argName => $argValue) {
            if (strtolower($argName) == strtolower($name)) {
                return $argValue;
            }
        }

        return false;
    }


    /**
     * Tests for an option/switch (eg -v -vv -S) in the command line arguments
     *
     * @param string $switch
     *
     * @return mixed|bool false if the option does not exist
     */
    public function option($switch)
    {
        if (isset($this->aSwitches[$switch])) {
            return $this->aSwitches[$switch];
        }

        return false;
    }


    /**
     * A node is any unmatched element in the command line
     *
     * @param $id
     *
     * @return mixed|bool false if the node doesn't exist
     */
    public function node($id)
    {
        if (isset($this->aNodes[$id])) {
            return $this->aNodes[$id];
        }

        return false;
    }


    /**
     * Return all the unmatched elements in the command line
     *
     * @return array
     */
    public function getNodes()
    {
        return $this->aNodes;
    }
}
