<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License; Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing; software
 * distributed under the License is distributed on an "AS IS" BASIS;
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND; either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information; please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Type;

use ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException;
use Eloquent\Enumeration\AbstractEnumeration;
use Eloquent\Enumeration\Exception\UndefinedMemberException;

/**
 * Class HttpStatusCode
 *
 * @category ChrisNoden\Synergy\Type
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information; please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class HttpStatusCode extends AbstractEnumeration
{

    const HTTP_100 = 'Continue';
    const HTTP_101 = 'Switching Protocols';
    const HTTP_200 = 'OK';
    const HTTP_201 = 'Created';
    const HTTP_202 = 'Accepted';
    const HTTP_203 = 'Non-Authoritative Information';
    const HTTP_204 = 'No Content';
    const HTTP_205 = 'Reset Content';
    const HTTP_206 = 'Partial Content';
    const HTTP_300 = 'Multiple Choices';
    const HTTP_301 = 'Moved Permanently';
    const HTTP_302 = 'Found';
    const HTTP_303 = 'See Other';
    const HTTP_304 = 'Not Modified';
    const HTTP_305 = 'Use Proxy';
    const HTTP_307 = 'Temporary Redirect';
    const HTTP_400 = 'Bad Request';
    const HTTP_401 = 'Unauthorized';
    const HTTP_402 = 'Payment Required';
    const HTTP_403 = 'Forbidden';
    const HTTP_404 = 'Not Found';
    const HTTP_405 = 'Method Not Allowed';
    const HTTP_406 = 'Not Acceptable';
    const HTTP_407 = 'Proxy Authentication Required';
    const HTTP_408 = 'Request Timeout';
    const HTTP_409 = 'Conflict';
    const HTTP_410 = 'Gone';
    const HTTP_411 = 'Length Required';
    const HTTP_412 = 'Precondition Failed';
    const HTTP_413 = 'Request Entity Too Large';
    const HTTP_414 = 'Request-URI Too Long';
    const HTTP_415 = 'Unsupported Media Type';
    const HTTP_416 = 'Requested Range Not Satisfiable';
    const HTTP_417 = 'Expectation Failed';
    const HTTP_500 = 'Internal Server Error';
    const HTTP_501 = 'Not Implemented';
    const HTTP_502 = 'Bad Gateway';
    const HTTP_503 = 'Service Unavailable';
    const HTTP_504 = 'Gateway Timeout';
    const HTTP_505 = 'HTTP Version Not Supported';

    public static $codes = array(
        100 => self::HTTP_100,
        101 => self::HTTP_101,
        200 => self::HTTP_200,
        201 => self::HTTP_201,
        202 => self::HTTP_202,
        203 => self::HTTP_203,
        204 => self::HTTP_204,
        205 => self::HTTP_205,
        206 => self::HTTP_206,
        300 => self::HTTP_300,
        301 => self::HTTP_301,
        302 => self::HTTP_302,
        303 => self::HTTP_303,
        304 => self::HTTP_304,
        305 => self::HTTP_305,
        307 => self::HTTP_307,
        400 => self::HTTP_400,
        401 => self::HTTP_401,
        402 => self::HTTP_402,
        403 => self::HTTP_403,
        404 => self::HTTP_404,
        405 => self::HTTP_405,
        406 => self::HTTP_406,
        407 => self::HTTP_407,
        408 => self::HTTP_408,
        409 => self::HTTP_409,
        410 => self::HTTP_410,
        411 => self::HTTP_411,
        412 => self::HTTP_412,
        413 => self::HTTP_413,
        414 => self::HTTP_414,
        415 => self::HTTP_415,
        416 => self::HTTP_416,
        417 => self::HTTP_417,
        500 => self::HTTP_500,
        501 => self::HTTP_501,
        502 => self::HTTP_502,
        503 => self::HTTP_503,
        504 => self::HTTP_504,
        505 => self::HTTP_505
    );


    /**
     * @param int $code 3 digit status code
     *
     * @return \Eloquent\Enumeration\MultitonInterface
     * @throws SynergyInvalidArgumentException
     */
    public static function code($code)
    {
        $key = 'HTTP_'.$code;
        try {
            return self::memberByKey($key);
        } catch (UndefinedMemberException $ex) {
            throw new SynergyInvalidArgumentException('Invalid HTTP Status code');
        }
    }
}
