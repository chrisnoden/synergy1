<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Api;

use ChrisNoden\Synergy\Controller\ControllerAbstract;
use ChrisNoden\Synergy\Controller\ControllerInterface;
use ChrisNoden\Synergy\Exception\SynergyApiCreateRequestException;
use ChrisNoden\Synergy\Exception\SynergyApiDuplicateRefusedException;
use ChrisNoden\Synergy\Exception\SynergyApiException;
use ChrisNoden\Synergy\Exception\SynergyApiInvalidContentTypeException;
use ChrisNoden\Synergy\Exception\SynergyApiInvalidEntityException;
use ChrisNoden\Synergy\Exception\SynergyApiInvalidPermissionsException;
use ChrisNoden\Synergy\Exception\SynergyApiObjectDeletedException;
use ChrisNoden\Synergy\Exception\SynergyApiObjectUpdateFailException;
use ChrisNoden\Synergy\Exception\SynergyApiUnknownIdentifierException;
use ChrisNoden\Synergy\Exception\SynergyException;
use ChrisNoden\Synergy\Request\WebRequest;
use ChrisNoden\Synergy\View\ApiResponse;
use ChrisNoden\Synergy\View\ResponseInterface;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\Exception\UnknownColumnException;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\ArrayCollection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Exception\PropelException;

/**
 * Class ApiPropelController
 * Expose Propel2 based Objects as a RESTful API
 *
 * @category ChrisNoden\Synergy\Api
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class ApiPropelController extends ControllerAbstract implements ControllerInterface
{
    /** @var WebRequest */
    protected $request;
    /** @var array */
    protected $url_parts = array();
    /** @var array */
    protected $options = array();
    /** @var string */
    protected $url_root;
    /** @var int maximum number of results that can be requested in one list response */
    protected $max_per_page = 2500;
    /** @var array the resource response */
    protected $data = array();
    /** @var string the namespace of the Model classes */
    protected $namespace;
    /** @var array */
    protected $resource_elements;
    /** @var ApiTarget */
    protected $target;
    /** @var string */
    protected $format;
    /** @var string */
    protected $common_query;
    /** @var bool force permissive mode */
    protected $permissive = false;
    /** @var string create, read, update, delete */
    protected $request_type;
    /** @var array */
    protected $metadata;
    /** @var bool */
    protected $isSSL = false;
    /** @var ApiAccessTokenInterface */
    protected $access_token;
    /** @var int Override the response status code */
    protected $response_status;
    /** @var array all error messages collected together */
    protected $error;
    /** @var string limit the session cookie to this host */
    protected $session_host;


    public function __construct()
    {
        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ||
            (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
        ) {
            $this->isSSL = true;
        }

        // Set the options (starting with the defaults)
        $this->options = $this->getDefaultOptions();

        if (isset($_SERVER['HTTP_HOST'])) {
            if (is_null($this->session_host)) {
                $this->session_host = $_SERVER["HTTP_HOST"];
            }

            try {
                // Start the PHP Sessions
                $this->startSessions();

                // Look for an access_token
                $this->loadAccessToken();
            } catch (\Exception $ex) {
                $this->logger->critical($ex->getMessage(), $ex->getTrace());
            }
        }
    }


    /**
     * Returns an appropriate error response to suit the Exception
     *
     * @param \Exception $ex
     *
     * @return ApiResponse
     */
    public function getExceptionResponse(\Exception $ex)
    {
        if ($ex instanceof SynergyApiInvalidPermissionsException) {
            return $this->getErrorResponse(403, 'Unauthorised request');
        } elseif ($ex instanceof SynergyApiInvalidContentTypeException) {
            return $this->getErrorResponse(406, 'Unsupported format');
        } elseif ($ex instanceof SynergyApiObjectUpdateFailException) {
            return $this->getErrorResponse(404, $ex->getMessage());
        }
        return $this->getErrorResponse(404, $ex->getMessage());
    }


    /**
     * @param string $rest
     *
     * @return \ChrisNoden\Synergy\View\Response|string
     * @throws \ChrisNoden\Synergy\Exception\SynergyException
     */
    public function defaultAction($rest)
    {
        if (
            $this->permissive ||
            $this->access_token instanceof ApiAccessTokenInterface
        ) {
            if ($this->access_token instanceof ApiAccessTokenInterface && !$this->access_token->isValid()) {
                return $this->getErrorResponse(403, 'access_token expired');
            }
            try {
                $this->processRequest($rest);

                // return a detailed error
                if (!empty($this->error)) {
                    $response = new ApiResponse();

                    $arr = array(
                        'code' => 400,
                        'message' => 'Failed request',
                        'errors' => $this->error
                    );

                    $response->setMetadata($arr);
                    $response->setContentType($this->getResponseFormat());
                    $response->setStatus(400);

                    return $response;
                }

                return $this->getResponse();
            } catch (\Exception $ex) {
                return $this->getExceptionResponse($ex);
            }
        }

        return $this->getErrorResponse(403, 'Unauthorised request');
    }


    /**
     * Return a simple response for OPTIONS method requests
     *
     * @return ApiResponse
     * @throws SynergyApiInvalidContentTypeException
     */
    public function optionsAction()
    {
        $response = new ApiResponse();
        $response->setOptionsResponse(true);
        $response->setContentType($this->getResponseFormat());
        return $response;
    }


    /**
     * Start PHP sessions
     */
    protected function startSessions()
    {
        // Start sessions
        if (!session_id()) {
            // lock down our session cookie
            session_set_cookie_params(0, '/', $this->session_host, $this->isSSL, true);
            session_start();
        }
    }


    /**
     * Load the access_token
     */
    protected function loadAccessToken()
    {
        $this->lookForAccessTokenInSession();
    }


    /**
     * Looks for a valid access_token in $_SESSION
     */
    protected function lookForAccessTokenInSession()
    {
        if (session_id()) {
            if (isset($_SESSION['access_token']) && $_SESSION['access_token'] instanceof ApiAccessTokenInterface) {
                $this->access_token = $_SESSION['access_token'];
            }
        }
    }


    /**
     * Default response options
     *
     * @return array
     */
    protected function getDefaultOptions()
    {
        return array(
            'parent_object' => false,
            'recurse'       => 0,
            'date_format'   => DATE_ISO8601,
            'page'          => 1,
            'per_page'      => 20,
            'format'        => 'json',
            'sort'          => 'natural:asc'
        );
    }


    /**
     * Array of param names that will be carried forward in the _url item
     *
     * @return array
     */
    protected function getCommonQueryParams()
    {
        return array(
            'format',
            'recurse',
            'date_format',
            'per_page',
            'sort'
        );
    }


    /**
     * Associative array of the accept types (eg text/csv) and their format (eg csv)
     * which are supported by this API
     *
     * @return array
     */
    protected function getSupportedFormatTypes()
    {
        return array( // associate types with file extensions
            '*/*'              => $this->options['format'],
            'text/*'           => 'json',
            'text/csv'         => 'csv',
            'text/xml'         => 'xml',
            'text/javascript'  => 'json',
            'application/*'    => 'json',
            'application/json' => 'json',
            'application/xml'  => 'xml'
        );
    }


    /**
     * What is the API's default response if the Entity class doesn't specify ApiOptions
     *
     * @param string $type create, read, update, delete
     *
     * @return bool
     */
    protected function getDefaultPermission($type)
    {
        switch (strtolower($type)) {
            case 'read':
                return false;

            case 'create':
                return false;

            case 'update':
                return false;

            case 'delete':
            default:
                return false;
        }
    }


    /**
     * Build any query that is passed through in links
     * Sets the common_query object parameter
     *
     * @return void
     */
    private function buildCommonParams()
    {
        $arr = array();
        foreach ($this->request->getParameters() as $key => $val) {
            if (in_array($key, $this->getCommonQueryParams())) {
                $arr[] = sprintf('%s=%s', $key, htmlentities($val));
            }
        }
        if (!empty($arr)) {
            $this->common_query = join('&', $arr);
        }
    }


    /**
     * Process the WebRequest and build the requested Response
     *
     * @param string $resource optional URL path to use as the request
     *
     * @return $this
     * @throws SynergyApiException
     * @throws SynergyApiUnknownIdentifierException
     * @throws SynergyApiInvalidPermissionsException
     * @throws SynergyApiInvalidEntityException
     */
    public function processRequest($resource = null)
    {
        $this->parseRequestComponents($resource);

        // Work out what type of CRUD request this is
        if (
            $this->request->getMethod() == 'DELETE' ||
            ($this->request->getMethod() == 'POST' && $this->target->getId() && !(is_null($this->request->get('delete'))))
        ) {
            $this->request_type = 'delete';
        } elseif ($this->request->getMethod() != 'GET') {
            if ($this->target->getId() !== false) {
                $this->request_type = 'update';
            } else {
                $this->request_type = 'create';
            }
        } else {
            $this->request_type = 'read';
        }

        // Does this user have permission to the resource?
        if (!$this->checkPermissions($this->target->getClassName(), $this->request_type)) {
            throw new SynergyApiInvalidPermissionsException('Insufficient permissions');
        }

        // Are we launching a controller method within the target class?
        if ($this->target->getController()) {
            $classname = $this->target->getClassName();
            $object = new $classname();
            $methodName = $this->target->getController();
            $r = new \ReflectionMethod($classname, $methodName);
            $classParams = $r->getParameters();
            $parameters = array();
            // Populate the parameters for the controller
            foreach ($classParams as $argKey => $oName) {
                $argName = (string)$oName->getName();
                if ($this->request->get($argName)) {
                    $parameters[$argKey] = $this->request->get($argName);
                } elseif ($argName == 'request') {
                    $parameters[$argKey] = $this->request;
                }
            }

            // This is quicker than call_user_func_array
            switch(count($parameters)) {
                case 0:
                    $response = $object->{$methodName}();
                    break;
                case 1:
                    $response = $object->{$methodName}($parameters[0]);
                    break;
                case 2:
                    $response = $object->{$methodName}($parameters[0], $parameters[1]);
                    break;
                case 3:
                    $response = $object->{$methodName}($parameters[0], $parameters[1], $parameters[2]);
                    break;
                case 4:
                    $response = $object->{$methodName}($parameters[0], $parameters[1], $parameters[2], $parameters[3]);
                    break;
                case 5:
                    $response = $object->{$methodName}($parameters[0], $parameters[1], $parameters[2], $parameters[3], $parameters[4]);
                    break;
                default:
                    $response = call_user_func_array(array($object, $methodName), $parameters);
            }

            if ($response instanceof ObjectCollection || $response instanceof ArrayCollection || is_array($response)) {
                $this->data = $response;
            } else {
                $this->data = array('response' => (string)$response);
            }
        } else {
            // Normal API response

            switch ($this->request_type) {
                case 'create':
                    if ($this->target->getId() !== false) {
                        throw new SynergyApiUnknownIdentifierException(
                            'Unable to create an object with a specific Identifier'
                        );
                    }
                    $this->data = $this->createObject($this->target, $this->request);

                    break;

                case 'read':
                    if ($this->target->getId() === false) {
                        $this->listRecords();
                        if ($this->data === false) {
                            throw new SynergyApiException('Invalid request');
                        }
                    } else {
                        $this->data = $this->loadObjectData($this->target);
                    }

                    break;

                case 'update':
                    if ($this->target->getId() === false) {
                        throw new SynergyApiException('Unable to update an object without a specific Identifier');
                    } else {
                        $this->data = $this->loadObjectData($this->target);
                        if (!($this->data instanceof ActiveRecordInterface)) {
                            throw new SynergyApiUnknownIdentifierException('Not found');
                        }
                        $this->data = $this->updateObject($this->data, $this->request);
                    }

                    break;

                case 'delete':
                    if ($this->target->getId() === false) {
                        throw new SynergyApiException('Unable to delete an object without a specific Identifier');
                    }

                    if (false === $this->deleteObject($this->target)) {
                        throw new SynergyApiException('Object not found');
                    } else {
                        throw new SynergyApiObjectDeletedException('Object deleted');
                    }

                    break;
            }
        }

//        if (!is_array($this->data) || empty($this->data)) {
//            if ($this->target->getId() !== false) {
//                throw new SynergyApiUnknownIdentifierException('Invalid identifier');
//            } else {
//                throw new SynergyApiException('Invalid request');
//            }
//
//        }

        return $this;
    }


    /**
     * Parse the request and set our class properties so we know what the request is for
     *
     * @param null|string $resource
     *
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiInvalidEntityException
     */
    protected function parseRequestComponents($resource = null)
    {
        // Parse the request URI
        if (!is_null($resource)) {
            if (substr($resource, 0, 1) != '/') {
                $resource = '/'.$resource;
            }
            $aUrl = array_merge(parse_url($this->request->getUri()), parse_url($resource));
        } else {
            $aUrl = parse_url($this->request->getUri());
        }

        $this->setUrlParts($aUrl);

        // replace any options with values from the request query
        if (isset($this->url_parts['query'])) {
            $this->setOptionsFromQuerystring($this->url_parts['query']);
        }

        // Set any common query params (which will be passed through to any URL links in the response data
        $this->buildCommonParams($this->request);

        // parse the path into the resources and identifiers
        $resources = $this->parseRequestPath($aUrl['path']);

        if (count($resources) < 1) {
            throw new SynergyApiInvalidEntityException('No entity name specified');
        }

        // load the resource target
        $this->buildResourceData($resources);
    }


    /**
     * Parse the resource request URI (the path component)
     * into the hierarchy of object entities and parameters
     *
     * @param string $request the path component of the uri
     *
     * @return array (0 => array('resource' => 'object name', 'id' => 'object ID' or null if a list request))
     */
    private function parseRequestPath($request)
    {
        if (strrpos($request, '.')) {
            $request = substr($request, 0, strrpos($request, '.'));
        }
        $request = preg_replace('#[/]{2,}#', '/', $request);
        if (substr($request, 0, 1) == '/') {
            $request = substr($request, 1);
        }

        $arr = array();
        $resource = null;
        $params = array();
        $new_namespace = null;
        foreach (explode('/', $request) as $key => $val) {
            $classname = $this->namespace . '\\' . ucfirst($resource);
            // Should we change the namespace
            if ($key > 0 &&
                !is_null($resource) &&
                class_exists($classname) &&
                method_exists($classname, 'get'.ucfirst($resource).'Namespace') &&
                preg_match('/^[0-9]+$/', $val) &&
                $key+1 < count(explode('/', $request))
            ) {
                $tgt = new ApiTarget();
                $tgt->setClassName($classname)
                    ->setParams(array(intval($val)));
                $obj = $this->loadRecord($tgt);
                $this->namespace = call_user_func(array($obj, 'get'.ucfirst($resource).'Namespace'));

                $arr[] = array(
                    'name'   => ucfirst($resource),
                    'params' => array(intval($val))
                );
                $resource = null;
                $params = array();
                continue;
            }

            if (preg_match('/^[^0-9]+$/', $val)) {
                if (!is_null($resource)) {
                    $arr[] = array(
                        'name'   => ucfirst($resource),
                        'params' => $params
                    );
                }
                $resource = $val;
                $params = array();
            } elseif (!is_null($resource)) {
                $params[] = intval($val);
            }
        }
        if (!is_null($resource)) {
            $arr[] = array(
                'name'   => ucfirst($resource),
                'params' => $params
            );
        }

        $this->resource_elements = $arr;

        // our $arr array should contain key value pairs of the resource (object) requested and the params
        // or the ID will be null if this is a list request
        return $arr;
    }


    /**
     * Iterate through the resources that were parsed out of the URI and
     * fetch the objects (from the Model classes). Load the target item if an identifier
     * has also been specified in the URI
     *
     * @param array $resources associative array with all the components of each resource in the URI
     *
     * @return bool true if request was identified properly
     * @throws SynergyApiInvalidEntityException if the requested entity (classname) does not exist
     */
    private function buildResourceData(Array $resources)
    {
        $objects = array();

        foreach ($resources as $key => $data) {
            $entityName = $data['name'];

            // what is the full classname that our resource should be under
            $testClassname = sprintf(
                '%s\\%s',
                $this->namespace,
                $entityName
            );

            $filter = array(); // default to no filter for the query

            // permit a plural entity name for a list request
            if (
                $key == count($resources)-1 &&
                empty($data['params']) &&
                substr($testClassname, -1) == 's' &&
                class_exists(substr($testClassname, 0, -1))
            ) {
                $testClassname = substr($testClassname, 0, -1);
                $entityName = substr($entityName, 0, -1);
                $this->url_root = substr($this->url_root, 0, -1);
            } elseif (
                // test for plural of 'ies' instead of 'y' (eg categories)
                $key == count($resources)-1 &&
                empty($data['params']) &&
                substr($testClassname, -3) == 'ies' &&
                class_exists(substr($testClassname, 0, -3).'y')
            ) {
                $testClassname = substr($testClassname, 0, -3).'y';
                $entityName = substr($testClassname, 0, -3).'y';
                $this->url_root = substr($this->url_root, 0, -3).'y';
            }

            if (class_exists($testClassname)) {
                // class name is valid
                if (
                    !empty($objects) &&
                    $key > 0 &&
                    method_exists($testClassname, 'set'.$objects[$key-1]['name'].'Id')
                ) {
                    // we need to filter the results - constrained to the parent resource (eg getGameId)
                    $foreignKeyName = $objects[$key-1]['name'] . 'Id';
                    // filter on the parent resource primary key
                    $filter = array($foreignKeyName => $objects[$key-1]['params']['Id']);
                }
            } elseif ($key > 0 && class_exists($objects[$key-1]['classname'] . $entityName)) {
                // look for a child resource
                // eg if request is /player/1/email then look for PlayerEmail class
                $testClassname  = $objects[$key-1]['classname'] . $data['name'];
                $foreignKeyName = $objects[$key-1]['name'] . 'Id';
                $foreignKeyVal  = $objects[$key-1]['params']['Id'];
                $filter = array($foreignKeyName => $foreignKeyVal);
            } else {
                throw new SynergyApiInvalidEntityException('Invalid request');
            }

            $objects[$key] = array(
                'name'      => $entityName,
                'classname' => $testClassname,
                'params'    => $this->getPkeyFilterArray($testClassname, $data['params'])
            );
        }

        if (!isset($testClassname)) {
            throw new SynergyApiInvalidEntityException('Invalid request');
        }

        $this->target = new ApiTarget();
        $this->target
            ->setClassName($testClassname)
            ->setParams($data['params'])
            ->setFilter($filter)
            ->setName($entityName)
            ->setId($this->getRequestIdentifier($testClassname, $data['params']));

        // Are we looking for a specific controller method in the target class
        if (isset($this->url_parts['extension'])) {
            $cont = 'controller'. ucfirst($this->url_parts['extension']);
            if (method_exists($testClassname, $cont)) {
                $this->target->setController($cont);
            }
        }

        return true;
    }


    /**
     * Return the PHP names of all the primary keys in the Model object
     *
     * @param $object
     *
     * @return array
     */
    private function getObjectPrimaryKeys($object)
    {
        $arr = array();

        $map_classname = constant(get_class($object).'::TABLE_MAP');
        if (class_exists($map_classname)) {
            $map = new $map_classname();
            $keys = $map->getPrimaryKeys();
            /** @var \Propel\Runtime\Map\ColumnMap $key */
            foreach ($keys as $key) {
                $arr[] = $key->getPhpName();
            }
        }

        return $arr;
    }


    /**
     * Return an array suitable for a FilterBy or FindByArray query
     *
     * @param string $classname
     * @param array  $params
     *
     * @return array
     */
    private function getPkeyFilterArray($classname, $params)
    {
        $obj = new $classname();
        $pkeys = $this->getObjectPrimaryKeys($obj);
        $filter = array();
        foreach ($params as $key => $val) {
            $filter[$pkeys[$key]] = $val;
        }

        return $filter;
    }


    /**
     * Load a specific object from the database and return its data
     *
     * @param ApiTarget $target
     *
     * @return \Object|false
     */
    private function loadObjectData(ApiTarget $target)
    {
        $classname = $target->getClassName();
        $object = new $classname();

        $queryClassname = get_class($object) . 'Query';
        if (class_exists($queryClassname)) {
            $qry = call_user_func($queryClassname . '::create');
            $qry = $qry->filterByArray($target->getFilter());

            return $qry->findPk($target->getId());
        }

        return false;
    }


    /**
     * Get a Propel compatible pkey parameter from the request
     *
     * @param string $classname
     * @param array  $params
     *
     * @return bool|int|array false means there is no identifer in the request
     */
    private function getRequestIdentifier($classname, $params)
    {
        $object = new $classname();
        $pkeys = $this->getObjectPrimaryKeys($object);

        $queryClassname = get_class($object) . 'Query';
        if (class_exists($queryClassname)) {
            $qry = call_user_func($queryClassname . '::create');
            $filter = $this->target->getFilter();
            if (!empty($filter)) {
                $qry = $qry->filterByArray($filter);
            }

            if (count($params) == count($pkeys)) {
                // return a specific record object
                if (count($params) == 1) {
                    $params = $params[0];
                }

                return $params;
            }
        }

        return false;
    }


    /**
     * Get a list of all the objects in the classname using the given params and filters
     */
    private function listRecords()
    {
        $classname = $this->target->getClassName();
        $object = new $classname();
        $this->metadata = array(
            '_metadata' => array(),
            'paging'    => array()
        );

        $pkeys = $this->getObjectPrimaryKeys($object);
        $queryClassname = get_class($object) . 'Query';
        if (class_exists($queryClassname)) {
            /** @var ModelCriteria $qry */
            $qry = call_user_func($queryClassname . '::create');

            // sort options
            if (isset($this->options['sort'])) {
                $sort = explode(':', $this->options['sort']);
                if (preg_match_all('/([a-z\_]+)\.[a-zA-Z0-9\_\`]{1,}/', $sort[0], $matches) > 0) {
                    foreach ($matches[1] as $test_entity) {
                        $joinClassName = $classname.'Query';
                        if (class_exists($joinClassName)) {
                            $joinMethodName = 'join' . ucfirst($test_entity);
                            if (method_exists($joinClassName, $joinMethodName)) {
                                $qry = $qry->$joinMethodName();
                            }
                        }
                    }
                }
                if (isset($_GET['order']) && ($_GET['order'] == 'asc' || $_GET['order'] == 'desc')) {
                    $sort[1] = $_GET['order'];
                }
                if ($sort[0] != 'natural') {
                    try {
                        if (isset($sort[1]) && strtolower($sort[1]) == 'desc') {
                            $qry = call_user_func(array($qry, 'orderBy' . ucfirst($sort[0])), Criteria::DESC);
                        } else {
                            $qry = call_user_func(array($qry, 'orderBy' . ucfirst($sort[0])));
                        }
                    } catch (UnknownColumnException $ex) {
                        // ignore
                    }
                }
            }

            // amend our filter
            $fba = array();
            foreach ($this->target->getParams() as $key => $val) {
                $fba[ucfirst($pkeys[$key])] = $val;
            }

            $filter = array_merge($this->target->getFilter(), $fba);
            $specificFilter = false;

            // refine the filter based on any querystring parameters
            foreach ($_GET as $fkey => $fval) {
                if (method_exists($object, 'get'.ucfirst($fkey))) {
                    $specificFilter = true;
                    $filter[ucfirst($fkey)] = $fval;
                } elseif ($fkey == 'search' && method_exists($queryClassname, 'filterByQuickSearch')) {
                    $filter['QuickSearch'] = $fval;
                    break;
                } elseif ($fkey == 'filter') {
                    $custom_filter = $this->fixCustomFilter($fval);
                }
            }

            if (isset($custom_filter) && preg_match_all('/([a-z\_]+)\.[a-zA-Z0-9\_\`]{1,}/', $custom_filter, $matches) > 0) {
                foreach ($matches[1] as $test_entity) {
                    $joinClassName = $classname.'Query';
                    if (class_exists($joinClassName)) {
                        $joinMethodName = 'join' . ucfirst($test_entity);
                        if (method_exists($joinClassName, $joinMethodName)) {
                            $qry = $qry->$joinMethodName();
                        }
                    }
                }
            }

            $qry = $qry->filterByArray($filter);
            if (isset($custom_filter)) {
                if ($specificFilter) {
                    $qry = $qry->_and();
                    $custom_filter = '( '.$custom_filter. ' )';
                }
                $qry = $qry->where($custom_filter);
            }
            $result = $qry
                ->paginate($this->options['page'], $this->options['per_page']);

            // create the paging links
            if ($result->haveToPaginate()) {
                if ($result->getPage() < $result->getLastPage()) {
                    $this->metadata['paging']['next'] = sprintf(
                        '%s://%s%s?%s',
                        $this->request->getScheme(),
                        $this->request->getHost(),
                        $this->request->getPath(),
                        $this->buildQueryString(array('page' => $result->getNextPage()))
                    );
                }
                if ($result->getPage() > 1) {
                    $this->metadata['paging']['previous'] = sprintf(
                        '%s://%s%s?%s',
                        $this->request->getScheme(),
                        $this->request->getHost(),
                        $this->request->getPath(),
                        $this->buildQueryString(array('page' => $result->getPreviousPage()))
                    );
                }
                $this->metadata['paging']['first'] = sprintf(
                    '%s://%s%s?%s',
                    $this->request->getScheme(),
                    $this->request->getHost(),
                    $this->request->getPath(),
                    $this->buildQueryString(array('page' => $result->getFirstPage()))
                );
                $this->metadata['paging']['last'] = sprintf(
                    '%s://%s%s?%s',
                    $this->request->getScheme(),
                    $this->request->getHost(),
                    $this->request->getPath(),
                    $this->buildQueryString(array('page' => $result->getLastPage()))
                );
            }

            // create the metadata
            $this->metadata['_metadata'] = array(
                'total_count' => $result->getNbResults(),
                'page'        => $result->getPage(),
                'page_count'  => $result->getLastPage(),
                'per_page'    => $this->options['per_page'],
            );

            $this->data = $result;
        }
    }


    /**
     * Works with the filter (from the DevExtreme query) to try to make something suitable for Propel to work with
     *
     * @param string $filter
     *
     * @return mixed|string
     */
    protected function fixCustomFilter($filter)
    {
        $items = preg_split('/[\ ]+/', $filter);

        $new_filter = '';
        foreach ($items as $input) {
            if (strpos($input, '.') > 0) {
                $new_filter .= substr($input, 0, strpos($input, '.') + 1);
                $input = substr($input, strpos($input, '.')+1);
            }
            if (preg_match('/[a-z]+[A-Z]{1}[a-z]{1,}/', $input) === 1 && preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches) > 0) {
                $ret = $matches[0];
                foreach ($ret as &$match) {
                    $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
                }
                $new_filter .= '`'.implode('_', $ret) .'`' . ' ';
            } else {
                $new_filter .= $input . ' ';
            }
        }
        $new_filter = trim($new_filter);

        $new_filter = preg_replace('/([`a-z0-9\_]+) contains \"([^\"]+)\"/', '$1 LIKE "%$2%"', $new_filter);
        $new_filter = preg_replace('/([`a-z0-9\_]+) notcontains \"([^\"]+)\"/', '$1 NOT LIKE "%$2%"', $new_filter);
        $new_filter = preg_replace('/([`a-z0-9\_]+) startswith \"([^\"]+)\"/', '$1 LIKE "$2%"', $new_filter);
        $new_filter = preg_replace('/([`a-z0-9\_]+) endswith \"([^\"]+)\"/', '$1 LIKE "%$2"', $new_filter);
        $new_filter = preg_replace('/([`a-z0-9\_]+) between ([^\ ]+) ([^\ ]+)/', '$1 BETWEEN $2 AND $3', $new_filter);
        $new_filter = preg_replace('/\"false\"/', '0', $new_filter);
        $new_filter = preg_replace('/\"true\"/', '1', $new_filter);

        // Fix any DX date time strings (eg
        $new_filter = preg_replace_callback(
            '/"([A-Z][a-z]{2} [A-Z][a-z]{2} [0-9]{1,2} [0-9]{2,4} [0-9\:]{8} [^\"]*)"/',
            function ($matches) {
                $filter = $matches[1];
                $filter = preg_replace('/(\([a-zA-Z0-9 ]+\))/', '', $filter);
                $filter = trim($filter);
                $dt = new \DateTime(date('Y/m/d H:i:s', strtotime($filter)));
                $userTimezone = new \DateTimeZone('Europe/London');
                $offset = $userTimezone->getOffset($dt);
                return '"'.date('Y-m-d', $dt->format('U') + $offset).'"';
            },
            $new_filter
        );

        return $new_filter;
    }


    /**
     * Get the metadata that is added to the ApiResponse
     *
     * @return array
     */
    public function getResponseMetadata()
    {
        $metadata = array('_metadata' => array());

        if (!(is_null($this->data)) && !(is_null($this->target))) {
            $metadata = array(
                '_metadata' => array(
                    'entity' => array(
                        'namespace' => $this->namespace,
                        'object'    => join('', array_slice(explode('\\', $this->target->getClassName()), -1))
                    )
                )
            );
        }

        if (is_array($this->metadata) && !empty($this->metadata)) {
            $metadata = array_merge_recursive($metadata, $this->metadata);
        }

        return $metadata;
    }


    /**
     * Override this method to use your own child Response
     *
     * @return ApiResponse
     */
    protected function getApiResponse()
    {
        return new ApiResponse();
    }


    /**
     * Get an ApiResponse (child of WebResponse)
     *
     * @return ApiResponse
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiException
     */
    public function getResponse()
    {
        if (is_null($this->data)) {
            throw new SynergyApiException('no data results for response');
        }

        // create the ApiResponse
        $response = $this->getApiResponse();
        $response->setContent($this->data);
        if (isset($this->options['field_list'])) {
            $response->setFieldInclusions($this->options['field_list']);
        }
        $response->setMetadata($this->getResponseMetadata());
        $response->setContentType($this->getResponseFormat());
        $response->setRecurse($this->options['recurse']);
        if (!is_null($this->response_status)) {
            $response->setStatus($this->response_status);
        }

        return $response;
    }


    /**
     * @param int         $code
     * @param string      $message
     *
     * @return ApiResponse
     */
    public function getErrorResponse($code, $message)
    {
        $response = $this->getApiResponse();

        $arr = array(
            'code' => $code,
            'message' => $message
        );

        $response->setMetadata($arr);
        $response->setContentType($this->getResponseFormat());
        $response->setStatus($code);

        return $response;
    }


    /**
     * Replace our class options with settings found in the querystring
     *
     * @param string $query
     *
     * @return void
     */
    private function setOptionsFromQuerystring($query = null)
    {
        if (is_null($query)) {
            if (isset($_SERVER['QUERY_STRING'])) {
                $query = $_SERVER['QUERY_STRING'];
            } else {
                return;
            }
        }

        parse_str($query, $arr);

        foreach ($arr as $key => $val) {
            if ($key == 'fields') {
                $this->setFieldList($val);
                continue;
            }

            if (isset($this->options[$key])) {
                // format/cast the querystring parameter to match the default format/type
                $cur = $this->options[$key];
                switch (gettype($cur)) {
                    case 'boolean':
                        if ($val == 1 || strtolower($val) == 'yes' || strtolower($val) == 'true') {
                            $val = true;
                        }
                        break;

                    case 'integer':
                        if ($key == 'per_page' && intval($val) > $this->max_per_page) {
                            $val = $this->max_per_page;
                        } elseif (is_string($val) && strtolower($val) == 'true') {
                            $val = 1;
                        } else {
                            $val = intval($val);
                        }
                        break;

                    case 'array':
                        $val = explode(',', $val);
                        break;
                }
            }
            // set the option
            $this->options[$key] = $val;

            // Look for bootstrap-table options and use them
            if (isset($this->options['limit']) && isset($this->options['offset'])) {
                $this->options['page'] = intval(1 + $this->options['offset'] / $this->options['limit']);
                $this->options['per_page'] = $this->options['limit'];
            }

        }
    }


    /**
     * Load a single item from the entity
     *
     * @param ApiTarget $target
     *
     * @return object
     * @throws SynergyApiUnknownIdentifierException if the record is not found
     * @throws SynergyApiInvalidEntityException        if incorrectly configured or some other error occurs
     */
    private function loadRecord(ApiTarget $target)
    {
        $classname = $target->getClassName();
        if (!class_exists($classname)) {
            throw new SynergyApiInvalidEntityException('Invalid request');
        }

        $params = $target->getParams();

        $object = new $classname();
        $pkeys = $this->getObjectPrimaryKeys($object);
        $queryClassname = $classname . 'Query';

        if (class_exists($queryClassname)) {
            $qry = call_user_func($queryClassname . '::create');

            if (count($params) == count($pkeys)) {
                // return a specific record object
                if (count($params) == 1) {
                    $params = $params[key($params)];
                }
                $result = $qry->findPk($params);
                if (is_a($result, $classname)) {
                    return $result;
                }
            }
        }

        throw new SynergyApiUnknownIdentifierException('Invalid identifier');
    }


    /**
     * Throws an exception if the user does not have permission to access this class
     *
     * @param string $classname
     * @param string $type create, read, update or delete
     *
     * @return bool
     *
     * @throws SynergyApiInvalidPermissionsException
     * @throws SynergyApiException
     */
    private function checkPermissions($classname, $type)
    {
        if ($this->permissive === true) {
            return true;
        }

        $object = new $classname();

        if (method_exists($object, 'apiOptions')) {
            $apiOptions = call_user_func(array($object, 'apiOptions'));

            // forbidden fields are never passed to the ApiResponse
            if (isset($apiOptions['forbidden'])) {
                $this->options['forbidden'] = $apiOptions['forbidden'];
            }

            if (isset($apiOptions['permissions'])) {

                // new style
                if (is_array($apiOptions['permissions'])) {
                    $perms = $apiOptions['permissions']['server'];
                    if ($type == 'read' && $perms['read'] === true) {
                        return true;
                    } elseif ($type == 'update' && ($perms['write'] === true || $perms['update'] === true)) {
                        return true;
                    } elseif ($type == 'delete' && $perms['delete'] === true) {
                        return true;
                    } elseif ($type == 'create' && $perms['create'] === true) {
                        return true;
                    }
                } elseif ($apiOptions['permissions'] === false) {
                    return false;
                } elseif ($apiOptions['permissions'] instanceof \Closure) {
                    $closure = $apiOptions['permissions'];
                    $reflection = new \ReflectionFunction($closure);
                    $arguments  = $reflection->getParameters();
                    $passed = array();
                    /** @var \ReflectionParameter $arg */
                    foreach ($arguments as $arg) {
                        if (property_exists($this, $arg->name)) {
                            $name = $arg->name;
                            $passed[] = $this->$name;
                        }
                    }
                    if (count($arguments) == count($passed)) {
                        if (count($arguments) == 0) {
                            return call_user_func($apiOptions['permissions']);
                        } elseif (count($arguments) == 1) {
                            return call_user_func($apiOptions['permissions'], $passed[0]);
                        } elseif (count($arguments) == 2) {
                            return call_user_func($apiOptions['permissions'], $passed[0], $passed[1]);
                        } elseif (count($arguments) == 3) {
                            return call_user_func($apiOptions['permissions'], $passed[0], $passed[1], $passed[2]);
                        }
                    } else {
                        throw new SynergyApiException('Invalid arguments for closure in apiOptions');
                    }
                }
            }
        }

        return $this->getDefaultPermission($type);
    }


    /**
     * Create a new object
     *
     * @param ApiTarget $target
     * @param WebRequest $request
     *
     * @return array
     * @throws SynergyApiCreateRequestException
     * @throws SynergyApiDuplicateRefusedException
     * @throws SynergyApiInvalidPermissionsException
     * @throws \Exception any other exception
     */
    private function createObject(ApiTarget $target, WebRequest $request)
    {
        $classname = $target->getClassName();
        $object = new $classname();

        $object = $this->updateObject($object, $request);

        $this->response_status = 201;
        return $object;
    }


    /**
     * Update an existing object
     *
     * @param ActiveRecordInterface $object
     * @param WebRequest            $request
     *
     * @return ActiveRecordInterface
     * @throws SynergyApiCreateRequestException
     * @throws SynergyApiInvalidPermissionsException
     * @throws \Exception any other exception
     */
    protected function updateObject(ActiveRecordInterface $object, WebRequest $request)
    {
        $params = $request->getParameters();

        // set parameters from the request
        foreach ($params as $key => $val) {
            if ($val === '') {
                $cur_val = null;
                $getter = sprintf('get%s', ucfirst($key));
                if (method_exists($object, $getter)) {
                    $cur_val = call_user_func(array($object, $getter));
                } elseif (method_exists($object, '__get')) {
                    $cur_val = $object->$key;
                }

                if (is_null($cur_val) || $cur_val === '') {
                    continue;
                }
            }

            $setter = sprintf('set%s', ucfirst($key));
            try {
                if (method_exists($object, $setter)) {
                    call_user_func(array($object, $setter), $val);
                } elseif (method_exists($object, '__set')) {
                    $object->$key = $val;
                }
            } catch (SynergyException $ex) {
                $this->error[$key] = $ex->getMessage();
            }
        }

        // set any params that can be derived from the resource elements
        foreach ($this->resource_elements as $resource) {
            if (isset($resource['params']) && count($resource['params']) == 1) {
                $setter = sprintf('set%sId', ucfirst($resource['name']));
                if (method_exists($object, $setter)) {
                    call_user_func(array($object, $setter), $resource['params'][0]);
                }
            }
        }

        if (!empty($this->error)) {
            return $object;
        }

        try {
            if (!isset($_GET['validate']) || !$_GET['validate'] == '1') {
                $object->save();
            }
        } catch (PropelException $ex) {
            if (preg_match('/^Unable to execute INSERT statement/', $ex->getMessage()) > 0) {
                throw new SynergyApiDuplicateRefusedException('Invalid/missing data');
            }
            throw $ex;
//            throw new SynergyApiCreateRequestException('Request refused by model constraint');
        } catch (\Exception $ex) {
            throw $ex;
        }

        return $object;
    }


    /**
     * Parse the accept string (taken from the request headers)
     * to match a response format type
     *
     * @param string $accept
     *
     * @return string (eg json)
     * @throws SynergyApiException if no supported accept type can be matched
     */
    protected function detectRequestAcceptType($accept)
    {
        if (!is_string($accept)) {
            $accept = '*/*';
        }

        $mimetypes = array (
            '*/*'              => 'json',
            'text/*'           => 'json',
            'text/csv'         => 'csv',
            'text/xml'         => 'xml',
            'text/javascript'  => 'json',
            'application/*'    => 'json',
            'application/json' => 'json',
            'application/xml'  => 'xml'
        );

        $types = array();
        foreach (explode(',', $accept) as $mediaRange) {
            @list ($type, $qparam) = preg_split(
                '/\s*;\s*/',
                $mediaRange
            ); // the q parameter must be the first one according to the RFC
            $q = substr($qparam, 0, 2) == 'q=' ? floatval(substr($qparam, 2)) : 1;
            if ($q <= 0) {
                continue;
            }
            if (substr($type, -1) == '*') {
                $q -= 0.0001;
            }
            if (@$type[0] == '*') {
                $q -= 0.0001;
            }
            $types[$type] = $q;
        }
        arsort($types); // sort from highest to lowest q value
        foreach ($types as $type => $q) {
            if (isset ($mimetypes[$type])) {
                return $mimetypes[$type];
            }
        }

        throw new SynergyApiException('Unsupported Accept Type');
    }


    /**
     * Delete an object
     *
     * @param ApiTarget $target
     *
     * @return array|bool false if failed to delete the identified object
     */
    private function deleteObject(ApiTarget $target)
    {
        $classname = $target->getClassName();
        $object = new $classname();

        $queryClassname = get_class($object) . 'Query';
        if (class_exists($queryClassname)) {
            $qry = call_user_func($queryClassname . '::create');
            $qry = $qry->filterByArray($target->getFilter());

            $result = $qry->findPk($target->getId());
            if (is_a($result, get_class($object)) && method_exists($result, 'delete')) {
                $data = $this->getApiResponse();
                $data->setContent($result);

                call_user_func(array($result, 'delete'));

                return $data;
            }
        }

        return false;
    }


    /**
     * Set the value of url_parts member
     *
     * @param array $url_parts
     *
     * @return void
     */
    private function setUrlParts(Array $url_parts)
    {
        $this->url_parts = $url_parts;

        if (isset($this->url_parts['path']) && substr($this->url_parts['path'], -1) == '/') {
            $this->url_parts['path'] = substr($this->url_parts['path'], 0, strlen($this->url_parts['path']) -1);
        }

        // set the root url for this request (without any querystring)
        $this->url_root = sprintf(
            '%s://%s%s',
            $this->url_parts['scheme'],
            $this->url_parts['host'],
            preg_replace('/[0-9\/]+$/', '', $this->url_parts['path'])
        );

        if (isset($this->url_parts['path']) && strstr($this->url_parts['path'], '.')) {
            $arr = explode('.', $this->url_parts['path']);
            $this->url_parts['path'] = $arr[0];
            $this->url_parts['extension'] = $arr[1];
        }
    }


    /**
     * parse the fields= query to build the field inclusion list
     *
     * @todo make it work with exclusions and brackets
     *
     * @param $query
     */
    private function setFieldList($query)
    {
        $arr = explode(',', $query);
        foreach ($arr as $key => $field) {
            if (trim($field) == '' || strlen($field) == 0) {
                unset($arr[$key]);
            }
        }
        $this->options['field_list'] = $arr;
    }


    /**
     * Return a HTTP valid query-string for links
     *
     * @param array $params associative array of keys and values to add/replace in the querystring
     *
     * @return string
     */
    private function buildQueryString(Array $params)
    {
        $arr = array();
        if (isset($this->url_parts['query'])) {
            parse_str($this->url_parts['query'], $arr);
        }

        $arr = array_merge($arr, $params);

        return http_build_query($arr);
    }


    /**
     * Figure out what response format is required
     *
     * @return string
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiException
     */
    protected function getResponseFormat()
    {
        if (isset($this->options['content_type'])) {
            return $this->options['content_type'];
        }
        if ($this->request->get('format')) {
            return $this->request->get('format');
        } elseif (strrpos($this->request->getPath(), '.')) {
            $ext = substr($this->request->getPath(), strrpos($this->request->getPath(), '.')+1);
            if (in_array($ext, $this->getSupportedFormatTypes())) {
                return $ext;
            }
        }
        // See what Accept header is set in the request
        if ($this->request instanceof WebRequest && !is_null($this->request->getRequestHeader('Accept'))) {
            $response_format = $this->detectRequestAcceptType($this->request->getRequestHeader('Accept'));
        } elseif (isset($_SERVER['Accept'])) {
            $response_format = $this->detectRequestAcceptType(
                $this->request->getRequestHeader($_SERVER['Accept'])
            );
        } else {
            $def = $this->getDefaultOptions();
            if (isset($def['format'])) {
                $response_format = $def['format'];
            } else {
                $response_format = 'json';
            }
        }

        return $response_format;
    }


    /**
     * Set the value of one of the options
     *
     * @param $option_name
     * @param $value
     *
     * @throws SynergyApiException
     */
    public function setOption($option_name, $value)
    {
        if (isset($this->options[$option_name])) {
            $this->options[$option_name] = $value;
        } else {
            throw new SynergyApiException('Unknown option '.$option_name);
        }
    }


    /**
     * Get the request data object or collection
     *
     * @return \Propel\Runtime\Collection\Collection|Object
     */
    public function getData()
    {
        return $this->data;
    }


    /**
     * Get the request data response as an associative array
     *
     * @return array value of member
     */
    public function getDataAsArray()
    {
        if (empty($this->data) || !isset($this->data['data'])) {
            return array();
        }
        // create the ApiResponse
        $response = ApiResponse::create($this->data['data']);
        if (isset($this->data['metadata'])) {
            $response->setMetadata($this->data['metadata']);
        }

        return $response;
    }


    /**
     * The classes for our API entities are in this namespace
     *
     * @param string $core_namespace
     *
     * @return $this
     */
    public function setNamespace($core_namespace)
    {
        $this->namespace = $core_namespace;

        return $this;
    }


    /**
     * Force permissive mode (all requests permitted)
     *
     * @param boolean $permissive
     *
     * @return $this
     */
    public function setPermissive($permissive)
    {
        $this->permissive = $permissive;

        return $this;
    }
}
