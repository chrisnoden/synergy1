<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Api;

/**
 * Class ApiTarget
 * The target entity
 *
 * @category ChrisNoden\Synergy\Api
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class ApiTarget
{

    /** @var string */
    protected $className;
    /** @var int */
    protected $id;
    /** @var array */
    protected $params;

    protected $filter;
    /** @var string */
    protected $name;
    /** @var string */
    protected $controller;


    /**
     * Set the value of className member
     *
     * @param string $className
     *
     * @return $this
     */
    public function setClassName($className)
    {
        $this->className = $className;

        return $this;
    }


    /**
     * Value of member className
     *
     * @return string value of member
     */
    public function getClassName()
    {
        return $this->className;
    }


    /**
     * Set the value of filter member
     *
     * @param mixed $filter
     *
     * @return $this
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;

        return $this;
    }


    /**
     * Value of member filter
     *
     * @return mixed value of member
     */
    public function getFilter()
    {
        return $this->filter;
    }


    /**
     * Set the value of id member
     *
     * @param mixed $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }


    /**
     * Value of member id
     *
     * @return mixed value of member
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set the value of params member
     *
     * @param array $params
     *
     * @return $this
     */
    public function setParams(Array $params)
    {
        $this->params = $params;

        return $this;
    }


    /**
     * Value of member params
     *
     * @return array value of member
     */
    public function getParams()
    {
        return $this->params;
    }


    /**
     * Set the value of name member
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }


    /**
     * Value of member name
     *
     * @return string value of member
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Value of member controller
     *
     * @return string value of member
     */
    public function getController()
    {
        return $this->controller;
    }


    /**
     * Set the value of controller member
     *
     * @param string $val
     *
     * @return $this
     */
    public function setController($val)
    {
        $this->controller = $val;

        return $this;
    }
}
 