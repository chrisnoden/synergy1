<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Api;

use ChrisNoden\Synergy\Exception\SynergyApiException;

/**
 * Class ApiInterface
 *
 * @category ChrisNoden\Synergy\Api
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
interface ApiInterface 
{


    /**
     * Set the value of one of the options
     *
     * @param $option_name
     * @param $value
     *
     * @throws SynergyApiException
     */
    public function setOption($option_name, $value);


    /**
     * Get the request data response as an associative array
     *
     * @return array value of member
     */
    public function getDataAsArray();


    /**
     * Get the request data response as JSON
     *
     * @return string
     */
    public function getDataAsJson();


    /**
     * Get the request data response as a SimpleXML object
     *
     * @return \SimpleXMLElement
     */
    public function getDataAsXml();
}
 