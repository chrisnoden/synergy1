# How to build the model classes from the test_schema.xml

    vendor/bin/propel model:build --config-dir="src/Synergy/Tests" --output-dir="src/Synergy/Tests/Api/Model" --schema-dir="src/Synergy/Tests" --disable-namespace-auto-package --verbose

    vendor/bin/propel sql:build --config-dir="src/Synergy/Tests" --schema-dir="src/Synergy/Tests" --output-dir="src/Synergy/Tests/Api/sql" --verbose

    vendor/bin/propel sql:insert --config-dir="src/Synergy/Tests" --sql-dir="src/Synergy/Tests/Api/sql/" --connection="synergy=mysql:host=127.0.0.1;dbname=synergy;user=root;password=" --verbose