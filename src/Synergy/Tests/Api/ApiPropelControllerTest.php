<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Tests\Api;

use ChrisNoden\Synergy\Api\ApiPropelController;
use ChrisNoden\Synergy\Request\WebRequest;

class ApiPropelControllerTest extends \PHPUnit_Framework_TestCase
{

    public function testGetListResponse()
    {
        $request = new WebRequest();
        $request->setUri('https://api.test.com/client');

        $api = new ApiPropelController();
        $api->setNamespace('ChrisNoden\Synergy\Tests\Api\Model')
            ->setPermissive(true)
            ->setRequest($request)
            ->processRequest();
        $this->assertInstanceOf('ChrisNoden\Synergy\Api\ApiPropelController', $api);
        $this->assertNotEmpty($api->getData());
        $response = $api->getResponse();
        $this->assertInstanceOf('ChrisNoden\Synergy\View\ApiResponse', $response);
        if (!in_array('Content-Type: application/json', $response->getHeaders())) {
            $this->fail('application/json header missing');
        }

        $json = json_decode($response->getContent(), true);

        $expArr = array(
            'data' => array(
                array(
                    'id'      => 1,
                    'name'    => 'Chris Noden',
                    'active'  => true,
                    'dtStart' => '2014-03-22T22:05:00+0000',
                )
            ),
            '_metadata' => array(
                'entity' => array(
                    'namespace' => 'ChrisNoden\Synergy\Tests\Api\Model',
                    'object'    => 'Client'
                ),
                'total_count' => 1,
                'page' => 1,
                'page_count' => 1,
                'per_page' => 20
            ),
            'paging' => array()
        );

        $this->assertEquals($expArr, $json);
        $data = $json['data'];
        if (!is_array($data)) {
            $this->fail('Expected data element to be an array');
        } elseif (empty($data)) {
            $this->fail('data array is empty');
        }
    }


    /**
     * Test that a request for a single element in an entity returns a valid response
     *
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiException
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiInvalidEntityException
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiInvalidPermissionsException
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiObjectDeletedException
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiUnknownIdentifierException
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException
     */
    public function testGetObjectResponse()
    {
        $request = new WebRequest();
        $request->setUri('https://api.test.com/client/1');

        $api = new ApiPropelController();
        $api->setNamespace('ChrisNoden\Synergy\Tests\Api\Model')
            ->setPermissive(true)
            ->setRequest($request)
            ->processRequest();

        $response = $api->getResponse();
        $this->assertInstanceOf('ChrisNoden\Synergy\View\ApiResponse', $response);
        if (!in_array('Content-Type: application/json', $response->getHeaders())) {
            $this->fail('application/json header missing');
        }

        $json = json_decode($response->getContent(), true);


        $expArr = array(
            'data' => array(
                'id'      => 1,
                'name'    => 'Chris Noden',
                'active'  => '1',
                'dtStart' => '2014-03-22T22:05:00+0000',
            ),
            '_metadata' => array(
                'entity' => array(
                    'namespace' => 'ChrisNoden\Synergy\Tests\Api\Model',
                    'object'    => 'Client'
                )
            )
        );

        $this->assertEquals($expArr, $json);
    }


    /**
     * Test the response format can be changed by our request headers
     *
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiException
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiInvalidEntityException
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiInvalidPermissionsException
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiObjectDeletedException
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiUnknownIdentifierException
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException
     */
    public function testCsvResponseFormat()
    {
        $request = new WebRequest();
        $request->setUri('https://api.test.com/client/1');
        $request->setHeader('Accept', 'text/csv');

        $api = new ApiPropelController();
        $api->setNamespace('ChrisNoden\Synergy\Tests\Api\Model')
            ->setPermissive(true)
            ->setRequest($request)
            ->processRequest();

        $response = $api->getResponse();
        $this->assertInstanceOf('ChrisNoden\Synergy\View\ApiResponse', $response);
        if (!in_array('Content-Type: text/csv', $response->getHeaders())) {
            $this->fail('text/csv header missing');
        }

        // @todo not sure if the ISO8601 date format should be returned in quotes
        $this->assertEquals("1,\"Chris Noden\",1,2014-03-22T22:05:00+0000\n", $response->getContent());
    }


    /**
     * Test the response format can be changed by our request headers
     *
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiException
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiInvalidEntityException
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiInvalidPermissionsException
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiObjectDeletedException
     * @throws \ChrisNoden\Synergy\Exception\SynergyApiUnknownIdentifierException
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException
     */
    public function testXmlResponseFormat()
    {
        $request = new WebRequest();
        $request->setUri('https://api.test.com/client/1');
        $request->setHeader('Accept', 'text/xml');

        $api = new ApiPropelController();
        $api->setNamespace('ChrisNoden\Synergy\Tests\Api\Model')
            ->setPermissive(true)
            ->setRequest($request)
            ->processRequest();

        $response = $api->getResponse();
        $this->assertInstanceOf('ChrisNoden\Synergy\View\ApiResponse', $response);
        if (!in_array('Content-Type: text/xml', $response->getHeaders())) {
            $this->fail('text/xml header missing');
        }

        $xml = "<?xml version='1.0' standalone='yes'?>
<data>
<data>
	<id>1</id>
	<name>Chris Noden</name>
	<active>1</active>
	<dtStart>2014-03-22T22:05:00+0000</dtStart>
</data>
<_metadata>
	<entity>
		<namespace>ChrisNoden\\Synergy\\Tests\\Api\\Model</namespace>
		<object>Client</object>
	</entity>
</_metadata>
</data>";
        $this->assertEquals($xml, $response->getContent());
    }
}
