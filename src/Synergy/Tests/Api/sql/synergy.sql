
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- client
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `client`;

CREATE TABLE `client`
(
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(60) NOT NULL,
    `active` TINYINT(1) DEFAULT 1 NOT NULL,
    `dt_start` DATETIME NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT='Business partners';

-- ---------------------------------------------------------------------
-- client_data
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `client_data`;

CREATE TABLE `client_data`
(
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `client_id` int(10) unsigned NOT NULL,
    `value` TEXT NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `client_data_fi_90166c` (`client_id`),
    CONSTRAINT `client_data_fk_90166c`
        FOREIGN KEY (`client_id`)
        REFERENCES `client` (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
