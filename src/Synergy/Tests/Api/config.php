<?php
$serviceContainer = \Propel\Runtime\Propel::getServiceContainer();
$serviceContainer->checkVersion('2.0.0-dev');
$serviceContainer->setAdapterClass('synergy', 'mysql');
$manager = new \Propel\Runtime\Connection\ConnectionManagerSingle();
$manager->setConfiguration(array (
        'dsn' => 'mysql:host=127.0.0.1;dbname=synergy;charset=UTF8',
        'user' => 'root',
        'password' => '',
        'settings' =>
            array (
                'charset' => 'utf8',
            ),
    ));
$manager->setName('synergy');
$serviceContainer->setConnectionManager('synergy', $manager);
$serviceContainer->setDefaultDatasource('synergy');