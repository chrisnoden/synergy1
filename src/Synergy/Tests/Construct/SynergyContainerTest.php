<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Synergy\Tests\Construct;

use ChrisNoden\Synergy\Construct\SynergyContainer;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class SynergyContainerTest extends \PHPUnit_Framework_TestCase
{

    public function testNullLogger()
    {
        $c = new SynergyContainer();

        $this->assertInstanceOf('Psr\Log\LoggerInterface', $c['logger']);
        $this->assertInstanceOf('Psr\Log\NullLogger', $c['logger']);
    }


    public function testReplaceLogger()
    {
        $c = new SynergyContainer();
        $logger = new Logger('Test');
        $logger->pushHandler(new StreamHandler('/tmp/synergy.log', Logger::ERROR));
        $c->setLogger($logger);

        $this->assertInstanceOf('Psr\Log\LoggerInterface', $c['logger']);
        $this->assertInstanceOf('Monolog\Logger', $c['logger']);
    }
}
