<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Tests\Construct;

use ChrisNoden\Synergy\Request\WebRequest;
use ChrisNoden\Synergy\Synergy\ProjectFactory;
use ChrisNoden\Synergy\Type\ProjectType;
use Psr\Log\NullLogger;

class ProjectFactoryTest extends \PHPUnit_Framework_TestCase
{

    public function testWebProject()
    {
        $project = ProjectFactory::create(
            ProjectType::WEB_PROJECT(),
            __DIR__ . DIRECTORY_SEPARATOR . 'webConfig.yml',
            sys_get_temp_dir(),
            new NullLogger()
        );

        $this->assertInstanceOf('ChrisNoden\Synergy\Synergy\WebProject', $project);
        $this->assertInstanceOf('ChrisNoden\Synergy\Synergy\ProjectAbstract', $project);
        $this->assertInstanceOf('ChrisNoden\Synergy\Synergy\ProjectInterface', $project);

        $this->assertEquals(ProjectType::WEB_PROJECT(), $project->getType());

        $request = new WebRequest();
        $request->setUri('https://www.test.com/my/path?link=here#part2');
        $request->setUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 6_0 like Mac OS X; en-us) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/10A5355d Safari/7534.48.3');

        $project->setRequest($request);
        $this->expectOutputString("TEST SMARTY TEMPLATE v2\nChris Noden");
        $project->run();
    }


}
