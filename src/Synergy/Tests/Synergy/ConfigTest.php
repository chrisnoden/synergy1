<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Tests\Synergy;

use ChrisNoden\Synergy\Synergy\Config;

class ConfigTest extends \PHPUnit_Framework_TestCase
{


    public function testCreateFromYamlFile()
    {
        $config = Config::createFromFile(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Construct' . DIRECTORY_SEPARATOR . 'webConfig.yml');
        $this->assertInstanceOf('ChrisNoden\Synergy\Synergy\Config', $config);
        $basedir = dirname(dirname(dirname(dirname((__DIR__)))));
        $this->assertEquals($basedir . DIRECTORY_SEPARATOR . 'examples/routes.yml', $config->get('synergy:routes'));
        $this->assertEquals(array('routes' => $basedir . DIRECTORY_SEPARATOR . 'examples/routes.yml'), $config->get('synergy'));

        $this->assertEquals(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Construct', $config->get('test:var1'), 'config.dir value does not match');
        $this->assertEquals('webConfig.yml', $config->get('test:var2'), 'config.basename value does not match');
        $this->assertEquals(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Construct' . DIRECTORY_SEPARATOR . 'webConfig.yml', $config->get('test:var3'), 'config.filename value does not match');
        $this->assertEquals($basedir . DIRECTORY_SEPARATOR . 'src/Synergy', $config->get('test:var4'), 'synergy.dir value does not match');
        $this->assertEquals($basedir . DIRECTORY_SEPARATOR . 'examples', $config->get('test:var5'), 'synergy.examples.dir value does not match');
    }


    public function testFlattenToArray()
    {
        $config = Config::createFromFile(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Construct' . DIRECTORY_SEPARATOR . 'webConfig.yml');

        $expected = array(
            "synergy.dir" => "/opt/src/Synergy",
            "synergy.examples.dir" => "/opt/examples",
            "config.dir" => "/opt/src/Synergy/Tests/Construct",
            "config.basename" => "webConfig.yml",
            "config.filename" => "/opt/src/Synergy/Tests/Construct/webConfig.yml",
            "synergy:routes" => "/opt/examples/routes.yml",
            "test:var1" => "/opt/src/Synergy/Tests/Construct",
            "test:var2" => "webConfig.yml",
            "test:var3" => "/opt/src/Synergy/Tests/Construct/webConfig.yml",
            "test:var4" => "/opt/src/Synergy",
            "test:var5" => "/opt/examples"
        );
        $this->assertEquals($expected, $config->flatten());
    }
}
