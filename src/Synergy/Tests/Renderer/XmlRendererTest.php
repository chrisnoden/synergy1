<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Tests\Renderer;

use ChrisNoden\Synergy\Renderer\XmlRenderer;

class XmlRendererTest extends \PHPUnit_Framework_TestCase
{


    public function testRender()
    {
        $data = array(
            'arr1' => array(
                'item' => 123.06,
                'qty'  => 42,
                'desc' => 'item description'
            ),
            'item2' => 'this is another item',
            'arr2' => array(
                'item' => 99.99,
                'qty'  => 14,
                'desc' => 'yet another description'
            )
        );

        $expected = "<?xml version='1.0' standalone='yes'?>
<data>
<arr1>
	<item>123.06</item>
	<qty>42</qty>
	<desc>item description</desc>
</arr1>
<item2>this is another item</item2>
<arr2>
	<item>99.99</item>
	<qty>14</qty>
	<desc>yet another description</desc>
</arr2>
</data>";

        $render = XmlRenderer::render($data);

        $this->assertEquals($expected, $render);
    }
}
