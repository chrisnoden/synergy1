<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Synergy\Tests\View;

use ChrisNoden\Synergy\View\FileResponse;

class FileResponseTest extends \PHPUnit_Framework_TestCase
{

    public function testFileAsset()
    {
        $testFile = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Request' . DIRECTORY_SEPARATOR . 'user_agents.json';
        $asset = FileResponse::create($testFile);
        $this->assertInstanceOf('ChrisNoden\Synergy\View\FileResponse', $asset);
        $this->assertInstanceOf('ChrisNoden\Synergy\View\Response', $asset);
        $this->assertInstanceOf('ChrisNoden\Synergy\View\ResponseAbstract', $asset);
        $this->assertEquals('application/json', $asset->getContentType());
        $exp_headers = array(
            'HTTP/1.1 200 OK',
            'Status: 200 OK',
            'Expires: '.date('r', strtotime('+' . $asset->getCacheDuration() . ' seconds')),
            'Last-Modified: '.date('r', filemtime($testFile)),
            'Cache-Control: private, max-age='.$asset->getCacheDuration().', must-revalidate',
            'Content-Length: '.filesize($testFile),
            'Pragma: private',
            'Content-Type: application/json',
            'X-Filename: '.basename($testFile),
            'ETag: '.md5(filectime($testFile)),
        );
        $diff = array_diff($asset->getHeaders(), $exp_headers);
        if (!empty($diff)) {
            $this->fail('Headers not as expected');
        }
    }


    /**
     * Should produce a text/plain content-type
     */
    public function testFileAssetNoExtension()
    {
        $testFile = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Request' . DIRECTORY_SEPARATOR . 'user_agents';
        $asset = FileResponse::create($testFile);
        $this->assertInstanceOf('ChrisNoden\Synergy\View\FileResponse', $asset);
        $this->assertInstanceOf('ChrisNoden\Synergy\View\Response', $asset);
        $this->assertInstanceOf('ChrisNoden\Synergy\View\ResponseAbstract', $asset);
        $exp_headers = array(
            'HTTP/1.1 200 OK',
            'Status: 200 OK',
            'Expires: '.date('r', strtotime('+' . $asset->getCacheDuration() . ' seconds')),
            'Last-Modified: '.date('r', filemtime($testFile)),
            'Cache-Control: private, max-age='.$asset->getCacheDuration().', must-revalidate',
            'Content-Length: '.filesize($testFile),
            'Pragma: private',
            'Content-Type: text/plain',
            'X-Filename: '.basename($testFile),
            'ETag: '.md5(filectime($testFile)),
        );
        $this->assertEmpty(array_diff($asset->getHeaders(), $exp_headers), 'Headers differ from expected');
    }


    public function testInvalidFileAsset()
    {
        $testFile = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'user_agents.json';
        $this->setExpectedException('ChrisNoden\Synergy\Exception\SynergyFileNotReadableException');
        $asset = FileResponse::create($testFile);
    }


    public function testInvalidContent()
    {
        $obj = new \stdClass();
        $this->setExpectedException('ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException');
        $asset = FileResponse::create($obj);
    }


    public function testInvalidExtension()
    {
        $testFile = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Request' . DIRECTORY_SEPARATOR . 'user_agents.json';
        $asset = FileResponse::create($testFile);
        $this->setExpectedException('ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException');
        $asset->setExtension('bad');
    }


    public function testInvalidStatus()
    {
        $content = '{
                "Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en; rv:1.8.1.11) Gecko/20071128 Camino/1.5.4": {
                "platform": "Macintosh",
                "browser": "Camino",
                "version": "1.5.4",
                "device": "desktop"
            }';

        $this->setExpectedException('ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException');
        $asset = FileResponse::create($content, 199);
    }


    public function testSetCacheDurationInvalid()
    {
        $asset = new FileResponse();
        $this->setExpectedException('ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException');
        $asset->setCacheDuration('3600');
    }
}
