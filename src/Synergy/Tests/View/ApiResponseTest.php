<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Tests\View;

use ChrisNoden\Synergy\Tests\Api\Model\Client;
use ChrisNoden\Synergy\Type\HttpStatusCode;
use ChrisNoden\Synergy\View\ApiResponse;
use Propel\Runtime\Collection\Collection;

class ApiResponseTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Test we get our object back in JSON format with the metadata intact
     *
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException
     */
    public function testBasicResponse()
    {
        // our test object
        $client = new Client();
        $client->setId(1)->setName('Test Client')->setActive(false)->setDtStart(new \DateTime());
        // create an ApiResponse object built from our test object
        $obj = ApiResponse::create($client);
        // Add our metadata (additional elements to return in the response body)
        $obj->setMetadata(
            array(
                '_metadata' => array(
                    "total_count" => 1,
                    "page" => 1,
                    "page_count" => 1,
                    "per_page" => 20
                ),
                'paging' => array()
            )
        );
        $this->assertInstanceOf('ChrisNoden\Synergy\View\ApiResponse', $obj);
        $this->assertInstanceOf('ChrisNoden\Synergy\View\Response', $obj);
        $this->assertInstanceOf('ChrisNoden\Synergy\View\ResponseInterface', $obj);

        if (!in_array('Content-Type: application/json', $obj->getHeaders())) {
            $this->fail('application/json header missing');
        }

        $json = json_decode($obj->getContent(), true);
        if (is_null($json)) {
            $this->fail('Unable to decode the response body as valid JSON');
        }

        $this->assertArrayHasKey('_metadata', $json);
        $this->assertArrayHasKey('data', $json);
        $this->assertArrayHasKey('paging', $json);
        $data = $json['data'];

        $expected = array(
            'id'      => 1,
            'name'    => "Test Client",
            'active'  => false,
            'dtStart' => date(DATE_ISO8601)
        );

        $this->assertEmpty(array_diff($expected, $data));
        $this->assertEquals(
            array(
                "total_count" => 1,
                "page" => 1,
                "page_count" => 1,
                "per_page" => 20
            ),
            $json['_metadata']
        );
        $this->assertEmpty($json['paging']);
    }


    public function testObjectAsCsvResponse()
    {
        // our test object
        $client = new Client();
        $client->setId(1)->setName('Test Client')->setActive(false)->setDtStart(new \DateTime());
        // create an ApiResponse object built from our test object
        $obj = ApiResponse::create($client);
        $obj->setContentType('csv');

        if (!in_array('Content-Type: text/csv', $obj->getHeaders())) {
            $this->fail('text/csv header missing');
        }

        // @todo not sure if the ISO8601 date format should be returned in quotes
        $this->assertEquals(sprintf("1,\"Test Client\",,%s\n", date(DATE_ISO8601)), $obj->getContent());
    }


    /**
     * Tests the ApiResponse class correctly handles an ObjectCollection
     *
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException
     */
    public function testCollectionResponse()
    {
        $collection = new Collection();
        $client1 = new Client();
        $client1->setId(1)->setName('Test Client 1')->setActive(false)->setDtStart(new \DateTime());
        $client2 = new Client();
        $client2->setId(2)->setName('Test Client Number two')->setActive(true)->setDtStart(new \DateTime());
        $collection->push($client1);
        $collection->push($client2);
        $obj = ApiResponse::create($collection);
        $obj->setMetadata(
            array(
                '_metadata' => array(
                    "total_count" => 2,
                    "page" => 1,
                    "page_count" => 1,
                    "per_page" => 20
                ),
                'paging' => array()
            )
        );
        $obj->setDefaultDateFormat('Y-m-d');
        $this->assertInstanceOf('ChrisNoden\Synergy\View\ApiResponse', $obj);
        $this->assertInstanceOf('ChrisNoden\Synergy\View\Response', $obj);
        $this->assertInstanceOf('ChrisNoden\Synergy\View\ResponseInterface', $obj);

        if (!in_array('Content-Type: application/json', $obj->getHeaders())) {
            $this->fail('application/json header missing');
        }

        $json = json_decode($obj->getContent(), true);

        $this->assertArrayHasKey('_metadata', $json);
        $this->assertArrayHasKey('data', $json);
        $this->assertArrayHasKey('paging', $json);
        $data = $json['data'];
        if (!is_array($data)) {
            $this->fail('Expected data element to be an array');
        }
        $this->assertEquals(1, $data[0]['id']);
        $this->assertEquals(2, $data[1]['id']);
        $this->assertEquals('Test Client 1', $data[0]['name']);
        $this->assertEquals('Test Client Number two', $data[1]['name']);
        $this->assertEquals(false, $data[0]['active']);
        $this->assertEquals(true, $data[1]['active']);
        $this->assertEquals(date('Y-m-d'), $data[0]['dtStart']);
        $this->assertEquals(date('Y-m-d'), $data[1]['dtStart']);
        $this->assertEquals(
            array(
                "total_count" => 2,
                "page" => 1,
                "page_count" => 1,
                "per_page" => 20
            ),
            $json['_metadata']
        );

    }


    public function testInvalidContent()
    {
        $this->setExpectedException('ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException');
        $obj = ApiResponse::create("Hello World");
    }


    /**
     * Test the default headers are as expected
     */
    public function testApiHeaders()
    {
        $client = new Client();
        $client->setId(1)->setName('Test Client')->setActive(false)->setDtStart(new \DateTime());
        $response = ApiResponse::create($client);

        $exp_headers = array(
            'HTTP/1.1 200 OK',
            'Status: 200 OK',
            'Expires: '.date('r', strtotime('Yesterday')),
            'Last-Modified: '.date('r'),
            "Cache-Control: no-store, no-cache, max-age=0, must-revalidate",
            "Content-Length: 120",
            "Pragma: no-cache",
            "Content-Type: application/json",
            'Access-Control-Allow-Origin: *',
            'Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS',
            'Access-Control-Allow-Headers: accept, origin, Content-Type, Accept-Encoding',
            'Access-Control-Max-Age: 300'
        );

        $this->assertInstanceOf('ChrisNoden\Synergy\View\Response', $response);
        /** @noinspection PhpUndefinedMethodInspection */
        $this->assertEquals(HttpStatusCode::HTTP_200(), $response->getStatus());
        $diff = array_diff($response->getHeaders(), $exp_headers);
        if (!empty($diff)) {
            var_dump($diff);
            $this->fail('Headers are not as expected');
        }
    }


    /**
     * Test we can change the Access-Control-Allow-Origin header
     */
    public function testCustomAllowOriginHeader()
    {
        $client = new Client();
        $client->setId(1)->setName('Test Client')->setActive(false)->setDtStart(new \DateTime());
        $response = ApiResponse::create($client);
        $response->setAllowedOrigin('http://www.testdomain.com');

        if (!in_array('Access-Control-Allow-Origin: http://www.testdomain.com', $response->getHeaders())) {
            $this->fail('Unable to set Access-Control-Allow-Origin value');
        }
    }


    /**
     * Test we can change the Access-Control-Max-Age header
     */
    public function testCustomMaxAge()
    {
        $client = new Client();
        $client->setId(1)->setName('Test Client')->setActive(false)->setDtStart(new \DateTime());
        $response = ApiResponse::create($client);
        $response->setMaxAge(3600);

        if (!in_array('Access-Control-Max-Age: 3600', $response->getHeaders())) {
            $this->fail('Unable to set Max-Age header value');
        }
    }


    /**
     * Test we can change the Access-Control-Allow-Headers header
     */
    public function testCustomAllowHeaders()
    {
        $client = new Client();
        $client->setId(1)->setName('Test Client')->setActive(false)->setDtStart(new \DateTime());
        $response = ApiResponse::create($client);
        $response->setAllowedHeaders(array('Content-Type'));

        if (!in_array('Access-Control-Allow-Headers: Content-Type', $response->getHeaders())) {
            $this->fail('Unable to set Access-Control-Allow-Headers header value');
        }
    }


    /**
     * Test we can change the Access-Control-Allow-Methods header
     */
    public function testCustomAllowMethodHeaders()
    {
        $client = new Client();
        $client->setId(1)->setName('Test Client')->setActive(false)->setDtStart(new \DateTime());
        $response = ApiResponse::create($client);
        $response->setAllowedMethods(array('PUT', 'OPTIONS'));

        if (!in_array('Access-Control-Allow-Methods: PUT, OPTIONS', $response->getHeaders())) {
            $this->fail('Unable to set Access-Control-Allow-Methods header value');
        }
    }
}
