<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Tests\View;

use ChrisNoden\Synergy\Tests\Providers;
use ChrisNoden\Synergy\Type\HttpStatusCode;
use ChrisNoden\Synergy\View\Response;

class ResponseTest extends \PHPUnit_Framework_TestCase
{
    use Providers;

    public function testCreateBasicResponse()
    {
        $response = Response::create(
            'Good job',
            200
        );

        $exp_headers = array(
            'HTTP/1.1 200 OK',
            'Status: 200 OK',
            'Expires: '.date('r', strtotime('Yesterday')),
            'Last-Modified: '.date('r'),
            "Cache-Control: no-store, no-cache, max-age=0, must-revalidate",
            "Content-Length: 8",
            "Pragma: no-cache",
            "Content-Type: text/html",
        );

        $this->assertInstanceOf('ChrisNoden\Synergy\View\Response', $response);
        $this->assertEquals('Good job', $response->getContent());
        /** @noinspection PhpUndefinedMethodInspection */
        $this->assertEquals(HttpStatusCode::HTTP_200(), $response->getStatus());
        $this->assertEmpty(array_diff($response->getHeaders(), $exp_headers), 'Headers differ from expected');
    }


    /**
     * @dataProvider contentTypes
     */
    public function testCreateCustomContentType($extension, $mimetype)
    {
        $response = Response::create(
            'Good job',
            200
        )
            ->setContentType($mimetype);

        $exp_headers = array(
            'HTTP/1.1 200 OK',
            'Status: 200 OK',
            'Expires: '.date('r', strtotime('Yesterday')),
            'Last-Modified: '.date('r'),
            "Cache-Control: no-store, no-cache, max-age=0, must-revalidate",
            "Content-Length: 8",
            "Pragma: no-cache",
            "Content-Type: ".$mimetype,
        );

        $this->assertInstanceOf('ChrisNoden\Synergy\View\Response', $response);
        $this->assertEquals('Good job', $response->getContent());
        /** @noinspection PhpUndefinedMethodInspection */
        $this->assertEquals(HttpStatusCode::HTTP_200(), $response->getStatus());
        $this->assertNotEmpty($response->getHeaders());
        $diff = array_diff($response->getHeaders(), $exp_headers);
        if (!empty($diff)) {
            var_dump(array_diff($response->getHeaders(), $exp_headers));
            $this->fail("Unexpected difference in headers, try tests again");
        }
    }


    public function testCreateInvalidCustomContentType()
    {
        $this->setExpectedException('ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException');
        $response = Response::create(
            'Good job',
            200
        )
            ->setContentType('chrisnoden');
    }


    /**
     * @dataProvider sampleJson
     */
    public function testJsonResponse($content)
    {
        $asset = Response::create($content);
        $asset->setContentType('json');

        $this->assertEquals('application/json', $asset->getContentType());
        $this->assertEquals($content, $asset->getContent());
        $this->expectOutputString($content);
        $asset->sendContent();
    }


    public function testAddCustomHeader()
    {
        $response = Response::create(
            'Good job',
            200
        );
        $response->addHeader('X-Chris-Noden', 'Developer');
        $response->setCacheDuration(600);
        $exp_headers = array(
            'HTTP/1.1 200 OK',
            'Status: 200 OK',
            'Expires: '.date('r', strtotime('+600 seconds')),
            'Last-Modified: '.date('r'),
            'Cache-Control: private, max-age=600, must-revalidate',
            "Content-Length: 8",
            'Pragma: private',
            'Content-Type: text/html',
            'X-Chris-Noden: Developer',
        );
        $this->assertEmpty(array_diff($response->getHeaders(), $exp_headers), 'Headers differ from expected');
    }


    public function testInvalidContent()
    {
        $obj = new \stdClass();
        $this->setExpectedException('ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException');
        $response = Response::create($obj);
    }


    /**
     * Pass an object that has a __toString() method
     */
    public function testObjectToStringContent()
    {
        $response = Response::create($this);
    }


    /**
     * Pass an object that has a __toString() method
     */
    public function testInvalidHttpStatus()
    {
        $this->setExpectedException('ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException');
        $response = Response::create($this, 199);
    }


    /**
     * Used for the tests
     * @return string
     */
    public function __toString()
    {
        return 'HELLO WORLD';
    }
}
