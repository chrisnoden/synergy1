<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Tests\Template;

use ChrisNoden\Synergy\Template\SmartyTemplate;

class SmartyTemplateTest extends \PHPUnit_Framework_TestCase
{

    public function testBasicTemplate()
    {
        $tpl = new SmartyTemplate();
        $this->assertInstanceOf('ChrisNoden\Synergy\Template\TemplateAbstract', $tpl);
        $this->assertInstanceOf('ChrisNoden\Synergy\Template\TemplateInterface', $tpl);

        $tpl->setTemplateDir(__DIR__);
        $tpl->setTemplateFile('test_smarty.html.tpl');
        $tpl->setParameters(array('testvar' => 'chris'));
        $tpl->addPluginDir(__DIR__);

        $this->assertEquals('TEST SMARTY TEMPLATE v2'."\n".'chris', (string) $tpl);
    }


    /**
     * The template expects one var called testvar
     */
//    public function testTemplateError()
//    {
//        $tpl = new SmartyTemplate();
//        $this->assertInstanceOf('ChrisNoden\Synergy\Template\TemplateAbstract', $tpl);
//        $this->assertInstanceOf('ChrisNoden\Synergy\Template\TemplateInterface', $tpl);
//
//        $tpl->setTemplateDir(__DIR__);
//        $tpl->setTemplateFile('test_smarty.html.tpl');
//
//        $this->setExpectedException('ChrisNoden\Synergy\Exception\NoticeException');
//        $tpl->getRender();
//    }

}
