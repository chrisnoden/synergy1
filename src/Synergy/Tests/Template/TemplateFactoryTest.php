<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Tests\Template;

use ChrisNoden\Synergy\Synergy\Config;
use ChrisNoden\Synergy\Template\TemplateFactory;
use ChrisNoden\Synergy\Type\TemplateType;

class TemplateFactoryTest extends \PHPUnit_Framework_TestCase
{

    public function testSmartyFactory()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $tpl = TemplateFactory::create(TemplateType::SMARTY_TEMPLATE(), __DIR__ . DIRECTORY_SEPARATOR . 'test_smarty.html.tpl');

        $this->assertInstanceOf('ChrisNoden\Synergy\Template\SmartyTemplate', $tpl);
        $this->assertEquals(__DIR__, $tpl->getTemplateDir());
        $this->assertEquals('test_smarty.html.tpl', $tpl->getTemplateFile());
    }


    public function testInvalidTemplateDirectory()
    {
        $this->setExpectedException('ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException');
        /** @noinspection PhpUndefinedMethodInspection */
        $tpl = TemplateFactory::create(TemplateType::SMARTY_TEMPLATE(), __DIR__ . 'fake' . DIRECTORY_SEPARATOR . 'test_smarty.html.tpl');
    }


    public function testRelativeTemplateFile()
    {
        $tpl = TemplateFactory::create(TemplateType::SMARTY_TEMPLATE(), 'test_smarty.html.tpl');
        $this->assertInstanceOf('ChrisNoden\Synergy\Template\SmartyTemplate', $tpl);
    }


    public function testRelativeTemplateWithConfig()
    {
        Config::setConfig(
            array(
                'synergy' => array(
                    'template_dir' => __DIR__
                )
            )
        );
        $tpl = TemplateFactory::create(TemplateType::SMARTY_TEMPLATE(), 'test_smarty.html.tpl');
        $this->assertInstanceOf('ChrisNoden\Synergy\Template\SmartyTemplate', $tpl);
        $this->assertEquals(__DIR__, $tpl->getTemplateDir());
        $this->assertEquals('test_smarty.html.tpl', $tpl->getTemplateFile());
    }
}
