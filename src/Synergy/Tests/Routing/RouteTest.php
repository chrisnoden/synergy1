<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Synergy\Tests\Web;

use ChrisNoden\Synergy\Request\WebRequest;
use ChrisNoden\Synergy\Routing\MappingType;
use ChrisNoden\Synergy\Routing\Request;
use ChrisNoden\Synergy\Type\RequestType;
use ChrisNoden\Synergy\Routing\Route;
use ChrisNoden\Synergy\Tests\Controller\TestController;
use ChrisNoden\Synergy\Tests\Providers;

class RouteTest extends \PHPUnit_Framework_TestCase
{

    public function testDefaultNaming()
    {
        $route1 = new Route();
        $route2 = new Route();
        $r1 = intval(preg_replace('/[^0-9]/','', $route1->getName()));

        $this->assertEquals(sprintf('route%s',$r1+1), $route2->getName());
    }


    public function testBasicRoute()
    {
        $route = new Route();
        $this->assertInstanceOf('ChrisNoden\Synergy\Routing\Route', $route);
        $route->setName('Test Route');
        /** @noinspection PhpUndefinedMethodInspection */
        $route->addMapping(MappingType::PATH(), '/foo/bar');

        $exp = array(MappingType::PATH =>'/foo/bar');
        $this->assertEquals($exp, $route->getMappings());
    }


    public function testLegacyRouteStyle()
    {
        $testRouteArr = array(
            'route1' => array(
                'path' => '/foo',
                'defaults' => array(
                    '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController:foo',
                ),
                'methods' => array('GET')
            )
        );

        $route = Route::build($testRouteArr);
        $this->assertInstanceOf('ChrisNoden\Synergy\Routing\Route', $route);
        $this->assertEquals(
            array(
                'className'  => '\ChrisNoden\Synergy\Tests\Controller\TestController',
                'methodName' => 'fooAction'
            ),
            $route->getController()
        );
        $this->assertEquals(
            array(
                MappingType::PATH => '/foo',
                MappingType::METHODS => array('GET')
            ),
            $route->getMappings()
        );
        $this->assertEmpty($route->getMetaData());
    }


    public function testNewRouteStyle()
    {
        $testRouteArr = array(
            'route1' => array(
                'path' => '/foo',
                'controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController:foo',
                'methods' => array('GET'),
                'chris'  => array('noden' => 'yup', 'smith' => 'no')
            )
        );

        $route = Route::build($testRouteArr);
        $this->assertInstanceOf('ChrisNoden\Synergy\Routing\Route', $route);
        $this->assertEquals(
            array(
                'className'  => '\ChrisNoden\Synergy\Tests\Controller\TestController',
                'methodName' => 'fooAction'
            ),
            $route->getController()
        );
        $this->assertEquals(
            array(
                MappingType::PATH => '/foo',
                MappingType::METHODS => array('GET')
            ),
            $route->getMappings()
        );
        $this->assertEquals(array('chris'  => array('noden' => 'yup', 'smith' => 'no')), $route->getMetaData());
    }


    public function testDefaultControllerMethod()
    {
        $route = new Route();
        $route->setController('ChrisNoden\Synergy\Tests\Controller\TestController');
        $this->assertEquals('defaultAction', $route->getController()['methodName'], 'If route does not specify method then it must default to defaultAction');
    }


    public function testIncompleteRoute()
    {
        $testRouteArr = array(
            'route1' => array(
                'path' => '/foo',
                'methods' => array('GET')
            )
        );

        // should fail because we have no controller set
        $this->setExpectedException('ChrisNoden\Synergy\Exception\SynergyInvalidRouteException');
        $route = Route::build($testRouteArr);
    }


    public function testControllerNotExists()
    {
        $route = new Route();
        $this->setExpectedException('ChrisNoden\Synergy\Exception\SynergyControllerNotFoundException');
        $route->setController('ChrisNoden\Synergy\Tests\Controller\DoesNotExist');
    }


    public function testControllerNotImplementsInterface()
    {
        $route = new Route();
        $route->setController('ChrisNoden\Synergy\Tests\Controller\TestInvalidController');
        $this->setExpectedException('ChrisNoden\Synergy\Exception\SynergyInvalidControllerException');
        $object = $route->getControllerObject();
    }


    public function testInvalidMethod()
    {
        $route = new Route();
        $this->setExpectedException('ChrisNoden\Synergy\Exception\SynergyInvalidControllerMethodException');
        $route->setController('ChrisNoden\Synergy\Tests\Controller\TestController:notValid');
    }


    public function testMetaData()
    {
        $route = new Route();
        $metadata = array('Test' => 'data', 'object' => new TestController());
        $route->addMetaData('item1', $metadata);
        $this->assertEquals(array('item1' => $metadata), $route->getMetaData());

        // add another item to the metadata
        $route->addMetaData('item2', array('nothing' => 'here'));
        $this->assertEquals(
            array(
                'item1' => $metadata,
                'item2' => array('nothing' => 'here')
            ),
            $route->getMetaData()
        );

        // replace part of item1
        $route->addMetaData('item1', array('merge' => 'this', 'Test' => 'changed'));
        $this->assertEquals(
            array(
                'item1' => array('Test' => 'changed', 'object' => new TestController(), 'merge' => 'this'),
                'item2' => array('nothing' => 'here')
            ),
            $route->getMetaData()
        );

        // add a scalar
        $route->addMetaData('item3', 'Hello There');
        $this->assertEquals(
            array(
                'item1' => array('Test' => 'changed', 'object' => new TestController(), 'merge' => 'this'),
                'item2' => array('nothing' => 'here'),
                'item3' => 'Hello There'
            ),
            $route->getMetaData()
        );

        // replace the scalar with an array
        $route->addMetaData('item3', array('part0' => 'Hello There'));
        $this->assertEquals(
            array(
                'item1' => array('Test' => 'changed', 'object' => new TestController(), 'merge' => 'this'),
                'item2' => array('nothing' => 'here'),
                'item3' => array('part0' => 'Hello There')
            ),
            $route->getMetaData()
        );

        // append a scalar to item3
        $route->addMetaData('item3', 'Hello Again');
        $this->assertEquals(
            array(
                'item1' => array('Test' => 'changed', 'object' => new TestController(), 'merge' => 'this'),
                'item2' => array('nothing' => 'here'),
                'item3' => array('part0' => 'Hello There', 0 => 'Hello Again')
            ),
            $route->getMetaData()
        );
    }


    public function testSimpleRegexMatch()
    {
        $route = Route::build(
            array(
                'routeRegex' => array(
                    'path'     => '/\/[a-zA-Z]+/',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                )
            )
        );

        /** @noinspection PhpUndefinedMethodInspection */
        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/anystring');

        $this->assertEquals(true, $route->requestMatch($request));
    }


    public function testParameterMatch()
    {
        $route = Route::build(
            array(
                'routeWithReq' => array(
                    'path'     => '/get/{itemId}/{itemName}/static',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    )
                )
            )
        );

        /** @noinspection PhpUndefinedMethodInspection */
        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/get/bob/smith/static');

        $route->requestMatch($request);
        $this->assertEquals(true, $route->requestMatch($request));
        $this->assertEquals(
            array(
                'itemId' => 'bob',
                'itemName' => 'smith'
            ),
            $route->getParams()
        );
    }


    public function testParameterReqsMatch()
    {
        $route = Route::build(
            array(
                'routeWithReq' => array(
                    'path'     => '/get/{itemId}/{itemName}/static',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController:bar',
                    ),
                    'requirements' => array(
                        'itemId' => '\d+',
                        'itemName' => '.*'
                    )
                )
            )
        );

        /** @noinspection PhpUndefinedMethodInspection */
        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/get/246/chri2s/static');

        $route->requestMatch($request);
        $this->assertEquals(true, $route->requestMatch($request));
        $this->assertEquals(
            array(
                'itemId' => '246',
                'itemName' => 'chri2s'
            ),
            $route->getParams()
        );

        $response = $route->launchAction();
        $this->assertEquals('Hello chri2s you are item:246', $response);
    }


    public function testParameterReqsFail()
    {
        $route = Route::build(
            array(
                'routeWithReq' => array(
                    'path'     => '/get/{itemId}/{itemName}/static',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                    'requirements' => array(
                        'itemId' => '\d+',
                        'itemName' => '.*'
                    )
                )
            )
        );

        /** @noinspection PhpUndefinedMethodInspection */
        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/get/246d/chri2s/static');

        $route->requestMatch($request);
        $this->assertEquals(false, $route->requestMatch($request));
    }


    public function testParameterReqsMisMatch()
    {
        $route = Route::build(
            array(
                'routeWithReq' => array(
                    'path'     => '/get/{itemId}/chris/static',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController:bar',
                    ),
                    'requirements' => array(
                        'itemId' => '\d+',
                        'itemName' => '.*'
                    )
                )
            )
        );

        /** @noinspection PhpUndefinedMethodInspection */
        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/get/246/chris/static');

        $route->requestMatch($request);
        $this->assertEquals(true, $route->requestMatch($request));

        $this->setExpectedException(
            'ChrisNoden\Synergy\Exception\SynergyInvalidRouteParameterCountException',
            'ChrisNoden\Synergy\Tests\Controller\TestController->barAction() expects 2 parameters, itemName missing in Route definition'
        );
        $response = $route->launchAction();
    }
}
