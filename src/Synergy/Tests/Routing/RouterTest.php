<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://bitbucket.org/chrisnoden
 */

namespace ChrisNoden\Synergy\Tests\Web;

use ChrisNoden\Synergy\Request\WebRequest;
use ChrisNoden\Synergy\Routing\Route;
use ChrisNoden\Synergy\Routing\Router;
use ChrisNoden\Synergy\Tests\TempFiles;
use ChrisNoden\Synergy\Tests\Providers;
use Psr\Log\NullLogger;

class RouterTest extends \PHPUnit_Framework_TestCase
{
    use TempFiles;
    use Providers;


    public function tearDown()
    {
        $this->cleanUpFiles();
    }


    public function testObject()
    {
        $router = Router::create();
        $this->assertInstanceOf('\ChrisNoden\Synergy\Routing\Router', $router);
    }


    /**
     * @dataProvider YamlDataProvider
     */
    public function testRoutesFromYamlFile($testYaml, $expectedArr)
    {
        $ymlFileName = $this->createTempFile($testYaml, 'yml');

        $router = Router::create()->addRoutesFromFile($ymlFileName);
        $this->assertInstanceOf('\ChrisNoden\Synergy\Routing\Router', $router);
        $this->assertNotEmpty($router->getRoutes());
        $this->assertEquals(count($expectedArr), count($router->getRoutes()), 'Number of discovered routes does not match actual routes');
    }


    /**
     * @dataProvider YamlDataProvider
     */
    public function testAddingRoute($testYaml, $expectedArr)
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        if (count($expectedArr) > 0) {
            foreach ($expectedArr as $name => $routeArr) {
                $route = Route::build(array($name => $routeArr));
                $router->addRoute($route);
            }
        } else {
            $route = Route::build($expectedArr);
            $router->addRoute($route);
        }

        $this->assertEquals(count($expectedArr), count($router->getRoutes()));
    }


    /**
     * @dataProvider YamlDataProvider
     */
    public function testRequestMatchFail($testYaml, $testRouteArr)
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        $router->addRoutesFromArray($testRouteArr);

        $request = new WebRequest();
        $request->setUri('http://127.0.0.1/foobar');

        $route = $router->match($request);

        if ($route instanceof Route) {
            $this->fail('There should be no routes in the YamlDataProvider that match '.$request->getUri());
        }
    }


    public function testSimpleRegexRouteMatch()
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        $router->addRoutesFromArray(
            array(
                'routeRegex' => array(
                    'path'     => '/\/[a-zA-Z]+/',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                )
            )
        );

        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/anystring');

        $route = $router->match($request);

        $this->assertInstanceOf('ChrisNoden\Synergy\Routing\Route', $route);
        $this->assertEquals('routeRegex', $route->getName());
    }


    public function testSimpleTextRouteMatch()
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        $router->addRoutesFromArray(
            array(
                'routeSimple' => array(
                    'path'     => '/anystring',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController:foo',
                    ),
                )
            )
        );

        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/anystring');

        $route = $router->match($request);

        $this->assertInstanceOf('ChrisNoden\Synergy\Routing\Route', $route);
        $this->assertEquals('routeSimple', $route->getName());
        $this->assertInstanceOf('ChrisNoden\Synergy\Tests\Controller\TestController', $route->getControllerObject());
        $this->assertEquals('fooAction', $route->getController()['methodName']);
    }


    public function testRouteWithDeviceMatch()
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        $router->addRoutesFromArray(
            array(
                'routeWithDevice' => array(
                    'path'     => '/anystring',
                    'devices'  => 'mobile',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                )
            )
        );

        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/anystring');
        $request->setUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 6_0 like Mac OS X; en-us) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/10A5355d Safari/7534.48.3');
        $this->assertEquals(true, $request->getRequestClient()->isMobile());

        $route = $router->match($request);

        $this->assertInstanceOf('ChrisNoden\Synergy\Routing\Route', $route);
        $this->assertEquals('routeWithDevice', $route->getName());
        $this->assertInstanceOf('ChrisNoden\Synergy\Tests\Controller\TestController', $route->getControllerObject());
    }


    public function testRouteWithDeviceArrayMatch()
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        $router->addRoutesFromArray(
            array(
                'routeWithDevice' => array(
                    'path'     => '/anystring',
                    'devices'  => array('mobile', 'tablet'),
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                )
            )
        );

        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/anystring');
        $request->setUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 6_0 like Mac OS X; en-us) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/10A5355d Safari/7534.48.3');
        $this->assertEquals(true, $request->getRequestClient()->isMobile());

        $route = $router->match($request);

        $this->assertInstanceOf('ChrisNoden\Synergy\Routing\Route', $route);
        $this->assertEquals('routeWithDevice', $route->getName());
        $this->assertInstanceOf('ChrisNoden\Synergy\Tests\Controller\TestController', $route->getControllerObject());
    }


    public function testRouteWithDeviceFail()
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        $router->addRoutesFromArray(
            array(
                'routeWithDevice' => array(
                    'path'     => '/anystring',
                    'devices'  => array('desktop', 'tablet'),
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                )
            )
        );

        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/anystring');
        $request->setUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 6_0 like Mac OS X; en-us) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/10A5355d Safari/7534.48.3');
        $this->assertEquals(true, $request->getRequestClient()->isMobile());

        $route = $router->match($request);

        $this->assertNull($route);
    }


    public function testRouteWithMethodMatch()
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        $router->addRoutesFromArray(
            array(
                'routeWithMethod' => array(
                    'path'     => '/anystring',
                    'methods'  => 'GET',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                )
            )
        );

        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/anystring');
        $this->assertEquals(false, $request->getRequestClient()->isMobile(), 'Basic request should not return true for isMobile');
        $this->assertEquals(false, $request->getRequestClient()->isDesktop(), 'Basic request should not return true for isDesktop');
        $this->assertEquals(false, $request->getRequestClient()->isTablet(), 'Basic request should not return true for isTablet');

        $route = $router->match($request);

        $this->assertInstanceOf('ChrisNoden\Synergy\Routing\Route', $route);
        $this->assertEquals('routeWithMethod', $route->getName());
        $this->assertInstanceOf('ChrisNoden\Synergy\Tests\Controller\TestController', $route->getControllerObject());
    }


    public function testRouteWithMethodArrayMatch()
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        $router->addRoutesFromArray(
            array(
                'routeWithMethod' => array(
                    'path'     => '/anystring',
                    'methods'  => array('GET', 'POST'),
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                )
            )
        );

        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/anystring');
        $request->setMethod('POST');

        $route = $router->match($request);

        $this->assertInstanceOf('ChrisNoden\Synergy\Routing\Route', $route);
        $this->assertEquals('routeWithMethod', $route->getName());
        $this->assertInstanceOf('ChrisNoden\Synergy\Tests\Controller\TestController', $route->getControllerObject());
    }


    public function testRouteWithMethodArrayFail()
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        $router->addRoutesFromArray(
            array(
                'routeWithMethod' => array(
                    'path'     => '/anystring',
                    'methods'  => array('GET', 'HEAD'),
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                )
            )
        );

        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/anystring');
        $request->setMethod('POST');

        $route = $router->match($request);

        $this->assertNull($route);
    }


    public function testRouteWithHostMatch()
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        $router->addRoutesFromArray(
            array(
                'route1' => array(
                    'path'     => '/anystring',
                    'host'     => 'dev.not.dev',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                ),
                'route2' => array(
                    'path'     => '/anystring',
                    'host'     => 'dev.dev.dev',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                )
            )
        );

        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/anystring');

        $route = $router->match($request);

        $this->assertInstanceOf('ChrisNoden\Synergy\Routing\Route', $route);
        $this->assertEquals('route2', $route->getName(), 'Route matched should be route2 - host on route1 is not a match');
        $this->assertInstanceOf('ChrisNoden\Synergy\Tests\Controller\TestController', $route->getControllerObject());
    }


    public function testRouteWithHostFail()
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        $router->addRoutesFromArray(
            array(
                'route1' => array(
                    'path'     => '/any1string',
                    'host'     => 'dev.not.dev',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                ),
                'route2' => array(
                    'path'     => '/anystring',
                    'host'     => 'dev.dev.dev',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                )
            )
        );

        $request = new WebRequest();
        $request->setUri('http://dev.not.dev/anystring');

        $route = $router->match($request);

        $this->assertNull($route);
    }


    public function testRouteWithHostRegexMatch()
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        $router->addRoutesFromArray(
            array(
                'route1' => array(
                    'path'     => '/anystring',
                    'host'     => 'dev.not.dev',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                ),
                'route2' => array(
                    'path'     => '/anystring',
                    'host'     => '/^[\s\S]+$/',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                )
            )
        );

        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/anystring');

        $route = $router->match($request);

        $this->assertInstanceOf('ChrisNoden\Synergy\Routing\Route', $route);
        $this->assertEquals('route2', $route->getName(), 'Route matched should be route2 - host on route1 is not a match');
        $this->assertInstanceOf('ChrisNoden\Synergy\Tests\Controller\TestController', $route->getControllerObject());
    }


    public function testRouteWithSchemeMatch()
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        $router->addRoutesFromArray(
            array(
                'routeSimple' => array(
                    'path'     => '/anystring',
                    'scheme'   => 'https',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                )
            )
        );

        $request = new WebRequest();
        $request->setUri('https://dev.dev.dev/anystring');

        $route = $router->match($request);

        $this->assertInstanceOf('ChrisNoden\Synergy\Routing\Route', $route);
        $this->assertEquals('routeSimple', $route->getName());
        $this->assertInstanceOf('ChrisNoden\Synergy\Tests\Controller\TestController', $route->getControllerObject());
    }


    public function testRouteWithSchemeFail()
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        $router->addRoutesFromArray(
            array(
                'routeSimple' => array(
                    'path'     => '/anystring',
                    'scheme'   => 'https',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                )
            )
        );

        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/anystring');

        $route = $router->match($request);

        $this->assertNull($route);
    }


    public function testRouteWithUrlMatch()
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        $router->addRoutesFromArray(
            array(
                'routeSimple' => array(
                    'url'     => 'http://dev.dev.dev/anystring',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                )
            )
        );

        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/anystring');

        $route = $router->match($request);

        $this->assertInstanceOf('ChrisNoden\Synergy\Routing\Route', $route);
        $this->assertEquals('routeSimple', $route->getName());
        $this->assertInstanceOf('ChrisNoden\Synergy\Tests\Controller\TestController', $route->getControllerObject());
    }


    public function testRouteWithUrlFail()
    {
        $router = new Router();
        $router->setLogger(new NullLogger());
        $router->addRoutesFromArray(
            array(
                'routeSimple' => array(
                    'url'     => 'http://dev.not.dev/anystring',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController',
                    ),
                )
            )
        );

        $request = new WebRequest();
        $request->setUri('http://dev.dev.dev/anystring');

        $route = $router->match($request);

        $this->assertNull($route);
    }
}
