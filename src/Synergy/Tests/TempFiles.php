<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Unit Test Trait
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://bitbucket.org/chrisnoden
 */

namespace ChrisNoden\Synergy\Tests;

use ChrisNoden\Synergy\Tools\Tools;

trait TempFiles
{
    /** @var array files we've created so we can clean up */
    protected $aFiles = array();


    /**
     * Create a temp file and fill it with the contents
     *
     * @param string $contents
     * @param string $extension
     *
     * @return string
     */
    protected function createTempFile($contents, $extension = '')
    {
        $filename = $this->createTempFilename($extension);
        $fp = fopen($filename, 'w');
        fwrite($fp, $contents);
        @fclose($fp);
        $this->aFiles[] = $filename;

        return $filename;
    }


    /**
     * get a temporary filename (full path and name)
     *
     * @param string $extension optional file extension
     *
     * @return string
     */
    protected function createTempFilename($extension = '')
    {
        if (defined('SYNERGY_TEMP_DIR')) {
            $tmp = SYNERGY_TEMP_DIR;
        } else {
            $tmp = sys_get_temp_dir();
        }

        do
        {
            $tempFileName = sprintf(
                '%s%s%s_%s',
                $tmp,
                DIRECTORY_SEPARATOR,
                date('Ymd'),
                Tools::randomString(8)
            );
            if ($extension != '') {
                $tempFileName .= '.'.$extension;
            }
        } while (file_exists($tempFileName));

        return $tempFileName;
    }


    /**
     * Remove any temp files we've created
     *
     * @return void
     */
    protected function cleanUpFiles()
    {
        foreach ($this->aFiles as $key => $filename) {
            if (strlen($filename) > 4) {
                @unlink($filename);
            }
            unset($this->aFiles[$key]);
        }
    }
}
