<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Synergy\Tests\Parser;

use ChrisNoden\Synergy\Parser\YamlParser;
use ChrisNoden\Synergy\Tests\Providers;

class YamlParserTest extends \PHPUnit_Framework_TestCase
{

    use Providers;


    /**
     * @dataProvider YamlDataProvider
     */
    public function testYamlToArray($testYaml, $expectedArr)
    {
        $arr = YamlParser::YAMLLoad($testYaml);
        $this->assertEquals($expectedArr, $arr);
    }


    /**
     * @dataProvider YamlDataProvider
     */
    public function testArrayToYaml($expectedYaml, $testArr)
    {
        $out = YamlParser::YAMLDump($testArr);
        $this->assertEquals($expectedYaml, $out);
    }


    /**
     * Test loading YAML files and ensure the result it as expected
     */
    public function testFileParsing()
    {
        $filename = __DIR__ . DIRECTORY_SEPARATOR . 'routeA.yml';
        $arr      = YamlParser::YAMLLoad($filename);
        $this->assertEquals(
            array(
                'route1' => array(
                    'path'     => '/admin/dl/retention',
                    'defaults' => array(
                        '_controller' => 'GmbAdmin\Controller\AdminController:download'
                    )
                ),
                'route2' => array(
                    'path'     => '/foo/bar',
                    'defaults' => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController:foo'
                    )
                ),
                'route3' => array(
                    'path'         => '/get/{itemId}/{itemName}/static',
                    'defaults'     => array(
                        '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController:bar'
                    ),
                    'requirements' => array(
                        'itemId'   => '\\d+',
                        'itemName' => '.*'
                    )
                ),
                'route4' => array(
                    'path'         => '/og/{packname}',
                    'defaults'     => array(
                        '_controller' => 'GmbAdmin\Controller\FacebookPaymentsController'
                    ),
                    'requirements' => array(
                        'packname' => '.*'
                    )
                ),
                'route5' => array(
                    'path'         => '/api/{app_id}/product/{currency_iso_code}',
                    'defaults'     => array(
                        '_controller' => 'GmbAdmin\Controller\FacebookPaymentsController:productPriceList'
                    ),
                    'requirements' => array(
                        'app_id'            => '\d+',
                        'currency_iso_code' => '.*'
                    )
                )
            ),
            $arr
        );
    }
}
 