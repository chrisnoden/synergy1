<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Tests\Controller;

use ChrisNoden\Synergy\Controller\ControllerRouter;
use ChrisNoden\Synergy\Synergy\Config;

class ControllerRouterTest extends \PHPUnit_Framework_TestCase
{


    public function testLegacyStyle()
    {
        $obj = new ControllerRouter();
        $test_string = 'ChrisNoden\Synergy\Tests\Controller\TestController:foo';
        $expected = array(
            'className'  => '\ChrisNoden\Synergy\Tests\Controller\TestController',
            'methodName' => 'fooAction'
        );
        $this->assertEquals($expected, $obj->parseControllerString($test_string));
    }


    public function testLegacyDefaultStyle()
    {
        $obj = new ControllerRouter();
        $test_string = 'ChrisNoden\Synergy\Tests\Controller\TestController';
        $expected = array(
            'className'  => '\ChrisNoden\Synergy\Tests\Controller\TestController',
            'methodName' => 'defaultAction'
        );
        $this->assertEquals($expected, $obj->parseControllerString($test_string));
    }


    public function testColonDelimited()
    {
        $obj = new ControllerRouter();
        $test_string = 'ChrisNoden:Synergy:Tests:Controller:TestController:foo';
        $expected = array(
            'className'  => '\ChrisNoden\Synergy\Tests\Controller\TestController',
            'methodName' => 'fooAction'
        );
        $this->assertEquals($expected, $obj->parseControllerString($test_string));
    }


    public function testColonDefaultDelimited()
    {
        $obj = new ControllerRouter();
        $test_string = 'ChrisNoden:Synergy:Tests:Controller:TestController';
        $expected = array(
            'className'  => '\ChrisNoden\Synergy\Tests\Controller\TestController',
            'methodName' => 'defaultAction'
        );
        $this->assertEquals($expected, $obj->parseControllerString($test_string));
    }



    public function testShorthand()
    {
        $config = Config::setConfig(array('synergy' => array('class_root' => '\\ChrisNoden\\Synergy\\Tests\\Controller')));
        $obj = new ControllerRouter();
        $test_string = 'TestController:foo';
        $expected = array(
            'className'  => '\ChrisNoden\Synergy\Tests\Controller\TestController',
            'methodName' => 'fooAction'
        );
        $this->assertEquals($expected, $obj->parseControllerString($test_string));
    }


    public static function tearDownAfterClass()
    {
        Config::setConfig(array());
    }
}
