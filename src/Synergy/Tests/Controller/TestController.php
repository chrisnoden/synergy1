<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Synergy\Tests\Controller;

use ChrisNoden\Synergy\Controller\ControllerAbstract;
use ChrisNoden\Synergy\Controller\ControllerInterface;
use ChrisNoden\Synergy\Template\TemplateFactory;
use ChrisNoden\Synergy\Type\TemplateType;
use ChrisNoden\Synergy\View\Response;
use Psr\Log\LoggerInterface;

/**
 * Class TestController
 * Referenced by the Tests - does not contain any tests
 *
 * @category ChrisNoden\Synergy\Tests\Controller
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link     https://github.com/chrisnoden/synergy
 */
class TestController extends ControllerAbstract implements ControllerInterface
{

    public function fooAction()
    {
        return 'test was successful';
    }


    public function barAction($itemId, $itemName)
    {
        return 'Hello '.$itemName.' you are item:'.$itemId;
    }

    public function webResponseAction()
    {
        if ($this->logger instanceof LoggerInterface) {
            $this->logger->alert("We are here");
        }
        $tpl = TemplateFactory::create(
            TemplateType::SMARTY_TEMPLATE(),
            dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Template' . DIRECTORY_SEPARATOR . 'test_smarty.html.tpl'
        );
        $tpl->setParameters(array('testvar' => 'Chris Noden'));

        $response = Response::create($tpl);

        return $response;
    }
}
 