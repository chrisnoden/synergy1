<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Tests\Console;

use ChrisNoden\Synergy\Console\Arguments;

class ArgumentsTest extends \PHPUnit_Framework_TestCase
{

    public function testObject()
    {
        $obj = new Arguments();
        $this->assertInstanceOf('ChrisNoden\Synergy\Console\Arguments', $obj);
    }


    public function testArguments()
    {
        $obj = new Arguments();
        $aArgs = array(
            'vendor/bin/console',
            '-t',
            '-vv',
            '-p',
            '2',
            '--test=yes',
            '--string=A very long string, or not'
        );
        $obj->parse($aArgs);
        $this->assertTrue($obj->option('t'), 't option not detected');
        $this->assertTrue($obj->option('vv'), 'vv option not detected');
        $this->assertEquals('2', $obj->option('p'), 'p option wrong value');
        $this->assertEquals('yes', $obj->arg('test'), 'test arg value not detected');
        $this->assertEquals('A very long string, or not', $obj->arg('string'), 'string arg value not detected');
    }
}
