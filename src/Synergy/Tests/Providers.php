<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Unit Test Trait
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://bitbucket.org/chrisnoden
 */

namespace ChrisNoden\Synergy\Tests;

use ChrisNoden\Synergy\Tools\Tools;
use ChrisNoden\Synergy\Type\ContentType;

trait Providers
{

    public function YamlDataProvider()
    {
        return array(

            array(
                "---\nroute1:\n  path: /foo/bar\n  defaults:\n    _controller: ChrisNoden\\Synergy\\Tests\\Controller\\TestController:foo\n  methods:\n    - GET\n",
                array(
                    'route1' => array(
                        'path'     => '/foo/bar',
                        'defaults' => array(
                            '_controller' => 'ChrisNoden\\Synergy\\Tests\\Controller\\TestController:foo',
                        ),
                        'methods'  => array('GET')
                    )
                )
            ),
            array(
                "---\nroute2:\n  path: /foo\n  defaults:\n    _controller: ChrisNoden\\Synergy\\Tests\\Controller\\TestController:foo\n  methods:\n    - GET\nroute3:\n  path: /\n  devices: mobile\n  defaults:\n    _controller: ChrisNoden\\Synergy\\Tests\\Controller\\TestController\nroute4:\n  path: /\n  defaults:\n    _controller: ChrisNoden\\Synergy\\Tests\\Controller\\TestController\n",
                array(
                    'route2' => array(
                        'path'     => '/foo',
                        'defaults' => array(
                            '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController:foo',
                        ),
                        'methods'  => array('GET')
                    ),
                    'route3' => Array(
                        'path'     => '/',
                        'devices'  => 'mobile',
                        'defaults' => Array(
                            '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController'
                        )
                    ),
                    'route4' => Array(
                        'path'     => '/',
                        'defaults' => Array(
                            '_controller' => 'ChrisNoden\Synergy\Tests\Controller\TestController'
                        )
                    )
                )
            )

        );
    }


    /**
     * @return array different browser request headers
     */
    public function browserRequests()
    {
        return array(
            'desktop' => array(
                'chrome' =>
                    array(
                        'UNIQUE_ID'            => Tools::randomString(20),
                        'HTTP_HOST'            => '127.0.0.1',
                        'HTTP_CONNECTION'      => 'keep-alive',
                        'HTTP_CACHE_CONTROL'   => 'max-age=0',
                        'HTTP_ACCEPT'          => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                        'HTTP_USER_AGENT'      => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.77 Safari/537.36',
                        'HTTP_REFERER'         => 'http://127.0.0.1/',
                        'HTTP_ACCEPT_ENCODING' => 'gzip,deflate,sdch',
                        'HTTP_ACCEPT_LANGUAGE' => 'en-US,en;q=0.8',
                        'PATH'                 => '/opt/local/bin:/opt/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin',
                        'SERVER_SIGNATURE'     => null,
                        'SERVER_SOFTWARE'      => 'Apache/2.2.26 (Unix) mod_ssl/2.2.26 OpenSSL/1.0.1f DAV/2 PHP/5.5.8',
                        'SERVER_NAME'          => '127.0.0.1',
                        'SERVER_ADDR'          => '127.0.0.1',
                        'SERVER_PORT'          => '80',
                        'REMOTE_ADDR'          => '127.0.0.1',
                        'DOCUMENT_ROOT'        => '/Users/chris/projects',
                        'SERVER_ADMIN'         => 'chris.noden@gmail.com',
                        'SCRIPT_FILENAME'      => '/Users/chris/projects/phpinfo.php',
                        'REMOTE_PORT'          => mt_rand(1025, 65536),
                        'GATEWAY_INTERFACE'    => 'CGI/1.1',
                        'SERVER_PROTOCOL'      => 'HTTP/1.1',
                        'REQUEST_METHOD'       => 'GET',
                        'QUERY_STRING'         => 'test=yes',
                        'REQUEST_URI'          => '/foo?test=yes',
                        'SCRIPT_NAME'          => '/foo',
                        'PHP_SELF'             => '/foo',
                        'REQUEST_TIME_FLOAT'   => microtime(true),
                        'REQUEST_TIME'         => time(),
                    )
            ),
            'mobile'  => array(
                'iphone-safari' =>
                    array(
                        'UNIQUE_ID'            => Tools::randomString(20),
                        'HTTP_HOST'            => '127.0.0.1',
                        'HTTP_CONNECTION'      => 'keep-alive',
                        'HTTP_CACHE_CONTROL'   => 'max-age=0',
                        'HTTP_ACCEPT'          => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                        'HTTP_USER_AGENT'      => 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 6_0 like Mac OS X; en-us) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/10A5355d Safari/7534.48.3',
                        'HTTP_REFERER'         => 'http://127.0.0.1/',
                        'HTTP_ACCEPT_ENCODING' => 'gzip,deflate,sdch',
                        'HTTP_ACCEPT_LANGUAGE' => 'en-US,en;q=0.8',
                        'PATH'                 => '/opt/local/bin:/opt/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin',
                        'SERVER_SIGNATURE'     => null,
                        'SERVER_SOFTWARE'      => 'Apache/2.2.26 (Unix) mod_ssl/2.2.26 OpenSSL/1.0.1f DAV/2 PHP/5.5.8',
                        'SERVER_NAME'          => '127.0.0.1',
                        'SERVER_ADDR'          => '127.0.0.1',
                        'SERVER_PORT'          => '80',
                        'REMOTE_ADDR'          => '127.0.0.1',
                        'DOCUMENT_ROOT'        => '/Users/chris/projects',
                        'SERVER_ADMIN'         => 'chris.noden@gmail.com',
                        'SCRIPT_FILENAME'      => '/Users/chris/projects/phpinfo.php',
                        'REMOTE_PORT'          => mt_rand(1025, 65536),
                        'GATEWAY_INTERFACE'    => 'CGI/1.1',
                        'SERVER_PROTOCOL'      => 'HTTP/1.1',
                        'REQUEST_METHOD'       => 'GET',
                        'QUERY_STRING'         => 'test=yes&chris=noden',
                        'REQUEST_URI'          => '/?test=yes&chris=noden',
                        'SCRIPT_NAME'          => '/',
                        'PHP_SELF'             => '/',
                        'REQUEST_TIME_FLOAT'   => microtime(true),
                        'REQUEST_TIME'         => time(),
                    )
            ),
            'tablet'  => array(
                'ipad-safari' =>
                    array(
                        'UNIQUE_ID'            => Tools::randomString(20),
                        'HTTP_HOST'            => '127.0.0.1',
                        'HTTP_CONNECTION'      => 'keep-alive',
                        'HTTP_CACHE_CONTROL'   => 'max-age=0',
                        'HTTP_ACCEPT'          => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                        'HTTP_USER_AGENT'      => 'Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25',
                        'HTTP_REFERER'         => 'http://127.0.0.1/',
                        'HTTP_ACCEPT_ENCODING' => 'gzip,deflate,sdch',
                        'HTTP_ACCEPT_LANGUAGE' => 'en-US,en;q=0.8',
                        'PATH'                 => '/opt/local/bin:/opt/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin',
                        'SERVER_SIGNATURE'     => null,
                        'SERVER_SOFTWARE'      => 'Apache/2.2.26 (Unix) mod_ssl/2.2.26 OpenSSL/1.0.1f DAV/2 PHP/5.5.8',
                        'SERVER_NAME'          => '127.0.0.1',
                        'SERVER_ADDR'          => '127.0.0.1',
                        'SERVER_PORT'          => '80',
                        'REMOTE_ADDR'          => '127.0.0.1',
                        'DOCUMENT_ROOT'        => '/Users/chris/projects',
                        'SERVER_ADMIN'         => 'chris.noden@gmail.com',
                        'SCRIPT_FILENAME'      => '/Users/chris/projects/phpinfo.php',
                        'REMOTE_PORT'          => mt_rand(1025, 65536),
                        'GATEWAY_INTERFACE'    => 'CGI/1.1',
                        'SERVER_PROTOCOL'      => 'HTTP/1.1',
                        'REQUEST_METHOD'       => 'GET',
                        'QUERY_STRING'         => 'test=yes',
                        'REQUEST_URI'          => '/?test=yes',
                        'SCRIPT_NAME'          => '/',
                        'PHP_SELF'             => '/',
                        'REQUEST_TIME_FLOAT'   => microtime(true),
                        'REQUEST_TIME'         => time(),
                    )
            )
        );
    }


    /**
     * @return array test browser request headers
     */
    public function requestHeadersChrome()
    {
        return array(
            'Accept'          => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding' => 'gzip,deflate,sdch',
            'Accept-Language' => 'en-US,en;q=0.8',
            'Cache-Control'   => 'max-age=0',
            'Connection'      => 'keep-alive',
            'Host'            => '127.0.0.1',
            'Referer'         => 'http://127.0.0.1/',
            'User-Agent'      => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.77 Safari/537.36'
        );
    }


    /**
     * @return array file extensions mapped to their Content-Type (mimetype) value
     */
    public function contentTypes()
    {
        $arr = array();
        foreach (ContentType::getAll() as $key => $val) {
            $arr[] = array('extension' => $key, 'mimetype' => $val);
        }

        return $arr;
    }


    /**
     * @return array sample json strings
     */
    public function sampleJson()
    {
        return array(
            array(
                '{"access_token":"f1bb2e8b65c40c401e865f980b780223","expires":"2014-02-11T11:12:41+0000","issued":"2014-02-10T11:23:00+0000"}',
                '{"data":[{"id":4,"legacyId":null,"alpha2":"AF","alpha3":"AFG","langEN":"Afghanistan","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/4"},{"id":8,"legacyId":71,"alpha2":"AL","alpha3":"ALB","langEN":"Albania","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/8"},{"id":10,"legacyId":76,"alpha2":"AQ","alpha3":"ATA","langEN":"Antarctica","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/10"},{"id":12,"legacyId":null,"alpha2":"DZ","alpha3":"DZA","langEN":"Algeria","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/12"},{"id":16,"legacyId":null,"alpha2":"AS","alpha3":"ASM","langEN":"American Samoa","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/16"},{"id":20,"legacyId":73,"alpha2":"AD","alpha3":"AND","langEN":"Andorra","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/20"},{"id":24,"legacyId":74,"alpha2":"AO","alpha3":"AGO","langEN":"Angola","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/24"},{"id":28,"legacyId":77,"alpha2":"AG","alpha3":"ATG","langEN":"Antigua and Barbuda","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/28"},{"id":31,"legacyId":82,"alpha2":"AZ","alpha3":"AZE","langEN":"Azerbaijan","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/31"},{"id":32,"legacyId":1,"alpha2":"AR","alpha3":"ARG","langEN":"Argentina","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/32"},{"id":36,"legacyId":2,"alpha2":"AU","alpha3":"AUS","langEN":"Australia","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/36"},{"id":40,"legacyId":null,"alpha2":"AT","alpha3":"AUT","langEN":"Austria","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/40"},{"id":44,"legacyId":83,"alpha2":"BS","alpha3":"BHS","langEN":"Bahamas","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/44"},{"id":48,"legacyId":84,"alpha2":"BH","alpha3":"BHR","langEN":"Bahrain","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/48"},{"id":50,"legacyId":4,"alpha2":"BD","alpha3":"BGD","langEN":"Bangladesh","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/50"},{"id":51,"legacyId":79,"alpha2":"AM","alpha3":"ARM","langEN":"Armenia","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/51"},{"id":52,"legacyId":85,"alpha2":"BB","alpha3":"BRB","langEN":"Barbados","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/52"},{"id":56,"legacyId":5,"alpha2":"BE","alpha3":"BEL","langEN":"Belgium","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/56"},{"id":60,"legacyId":87,"alpha2":"BM","alpha3":"BMU","langEN":"Bermuda","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/60"},{"id":64,"legacyId":88,"alpha2":"BT","alpha3":"BTN","langEN":"Bhutan","currency":null,"_url":"https:\/\/api.gamesko.com\/country\/64"}],"_metadata":{"total_count":241,"page":1,"page_count":13,"per_page":20},"paging":{"next":"https:\/\/api.gamesko.com\/country?recurse=true&page=2","first":"https:\/\/api.gamesko.com\/country?recurse=true&page=1","last":"https:\/\/api.gamesko.com\/country?recurse=true&page=13"}}',
                '{ "glossary": { "title": "example glossary","GlossDiv": { "title": "S","GlossList": { "GlossEntry": { "ID": "SGML","SortAs": "SGML","GlossTerm": "Standard Generalized Markup Language","Acronym": "SGML","Abbrev": "ISO 8879:1986","GlossDef": { "para": "A meta-markup language, used to create markup languages such as DocBook.","GlossSeeAlso": ["GML", "XML"] },"GlossSee": "markup" } } } }}'            )
        );
    }
}
 