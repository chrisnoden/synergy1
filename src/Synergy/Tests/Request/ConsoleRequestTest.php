<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Tests\Web;

use ChrisNoden\Synergy\Request\ConsoleRequest;

class ConsoleRequestTest extends \PHPUnit_Framework_TestCase
{

    public function testObject()
    {
        $obj = new ConsoleRequest();
        $this->assertInstanceOf('ChrisNoden\Synergy\Request\ConsoleRequest', $obj);
        $this->assertInstanceOf('ChrisNoden\Synergy\Request\RequestAbstract', $obj);
        $this->assertInstanceOf('ChrisNoden\Synergy\Request\RequestInterface', $obj);
    }


    public function testArgumentParsing()
    {
        $obj = new ConsoleRequest();
        $aArgs = array(
            'vendor/bin/console',
            '-t',
            '-vv',
            '--test=yes',
            '--string=A very long string, or not'
        );
        $obj->setArgs($aArgs);
        $obj->build();

        $args = $obj->getArguments();
        $this->assertInstanceOf('ChrisNoden\Synergy\Console\Arguments', $args);

        $this->assertEquals('yes', $args->arg('test'));
    }
}
