<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Tests\Request;

use ChrisNoden\Synergy\Request\WebRequest;
use ChrisNoden\Synergy\Tests\Providers;

class WebRequestTest extends \PHPUnit_Framework_TestCase
{

    use Providers;


    public function testObject()
    {
        $obj = new WebRequest();
        $this->assertInstanceOf('ChrisNoden\Synergy\Request\WebRequest', $obj);
        $this->assertInstanceOf('ChrisNoden\Synergy\Request\RequestAbstract', $obj);
        $this->assertInstanceOf('ChrisNoden\Synergy\Request\RequestInterface', $obj);
    }


    /**
     * @dataProvider browserRequests
     */
    public function testBuildWebRequestFromGlobals($testHeaders)
    {
        $aMethods = array('GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'OPTIONS');
        foreach ($aMethods as $method) {
            /** @noinspection PhpUndefinedMethodInspection */
            $request = new WebRequest();
            $testHeaders['REQUEST_METHOD'] = $method;
            parse_str($testHeaders['QUERY_STRING'], $expParams);
            $request->setServerParams($testHeaders);
            $request->build();
            $this->assertInstanceOf('ChrisNoden\Synergy\Request\WebRequest', $request);
            $this->assertEquals($method, $request->getMethod());
            $this->assertEquals($testHeaders['QUERY_STRING'], $request->getQueryString());
            $this->assertEquals($testHeaders['SCRIPT_NAME'], $request->getPath());
            $this->assertEquals($testHeaders['HTTP_USER_AGENT'], $request->getUserAgent());
            $this->assertEquals($testHeaders['HTTP_REFERER'], $request->getReferer());
            $this->assertEquals($expParams, $request->getParameters());
        }
    }


    public function testInvalidMethod()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $request = new WebRequest();
        $this->setExpectedException('ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException');
        $request->setMethod('UNSUPPORTED');
    }


    public function testRequestFromUri()
    {
        $request = new WebRequest();
        $request->setUri('https://www.test.com/my/path?link=here#part2');
        $this->assertEquals(true, $request->getSsl());
        $this->assertEquals('www.test.com', $request->getHost());
        $this->assertEquals('/my/path', $request->getPath());
        $this->assertEquals(array('link' => 'here'), $request->getParameters());
        $this->assertEquals('part2', $request->getFragment());
    }


    public function testUriPlusUserAgent()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $request = new WebRequest();
        $request->setUri('https://www.test.com/my/path?link=here#part2');
        $request->setUserAgent('Mozilla/5.0 (iPhone; U; CPU iPhone OS 6_0 like Mac OS X; en-us) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/10A5355d Safari/7534.48.3');
        $this->assertEquals(true, $request->getSsl());
        $this->assertEquals('www.test.com', $request->getHost());
        $this->assertEquals('/my/path', $request->getPath());
        $this->assertEquals(array('link' => 'here'), $request->getParameters());
        $this->assertEquals('part2', $request->getFragment());
        $this->assertEquals(true, $request->getRequestClient()->isMobile());
        $this->assertEquals(false, $request->getRequestClient()->isDesktop());
        $this->assertEquals(false, $request->getRequestClient()->isTablet());
    }


    public function testDeleteMethodWithBody()
    {
        $request_source = "DELETE /game/2?access_token=4282c6a4499ab61444b6973088997c47 HTTP/1.1\r\nHost: dev-api.gamesko.com\r\nCache-Control: no-cache\r\nPostman-Token: 808cccdd-bfd5-8202-c1f4-347db8c53449\r\nContent-Type: multipart/form-data; boundary=----WebKitFormBoundaryE19zNvXGzXaLvS5C\r\n\r\n----WebKitFormBoundaryE19zNvXGzXaLvS5C\r\nContent-Disposition: form-data; name=\"access_token\"\r\n\r\n4282c6a4099ab61444b6973088997c47\r\n----WebKitFormBoundaryE19zNvXGzXaLvS5C\r\nContent-Disposition: form-data; name=\"field2\"\r\n\r\nmyvalue\r\n----WebKitFormBoundaryE19zNvXGzXaLvS5C";

        $request = new WebRequest();
        $request->parseRawWebRequest($request_source);

        $this->assertEquals('DELETE', $request->getMethod());
        $this->assertEquals('/game/2', $request->getPath());
        $this->assertEquals(
            array(
                'access_token' => '4282c6a4099ab61444b6973088997c47',
                'field2' => 'myvalue'
            ),
            $request->getParameters()
        );
        $this->assertEquals('dev-api.gamesko.com', $request->getHost());
    }


    public function testUrlEncodedBody()
    {
        $request_source = "PUT /game/2?access_token=4282c6a4099ab61444b6973088997c47 HTTP/1.1\r\nHost: dev-api.gamesko.com\r\nCache-Control: no-cache\r\nPostman-Token: c4f574a5-8161-819b-991e-6bc09360ce0c\r\nContent-Type: application/x-www-form-urlencoded\r\n\r\nfield1=value1&field2=value2";

        $request = new WebRequest();
        $request->parseRawWebRequest($request_source);

        $this->assertEquals('PUT', $request->getMethod());
        $this->assertEquals('/game/2', $request->getPath());
        $this->assertEquals(
            array(
                'access_token' => '4282c6a4099ab61444b6973088997c47',
                'field1'       => 'value1',
                'field2'       => 'value2'
            ),
            $request->getParameters()
        );
        $this->assertEquals('dev-api.gamesko.com', $request->getHost());
    }


    public function testPostBody()
    {
        $request_source = "POST /game/2?access_token=4282c6a4099ab61444b6973088997c47 HTTP/1.1\r\nHost: dev-api.gamesko.com\r\nCache-Control: no-cache\r\nPostman-Token: c4f574a5-8161-819b-991e-6bc09360ce0c\r\nContent-Type: application/x-www-form-urlencoded\r\n\r\nfield1=value1&field2=value2&custom=absolutely";

        $request = new WebRequest();
        $request->parseRawWebRequest($request_source);

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals('/game/2', $request->getPath());
        $this->assertEquals(
            array(
                'access_token' => '4282c6a4099ab61444b6973088997c47',
                'field1'       => 'value1',
                'field2'       => 'value2',
                'custom'       => 'absolutely'
            ),
            $request->getParameters()
        );
        $this->assertEquals('dev-api.gamesko.com', $request->getHost());
    }
}
