<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Unit Test
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 * @license   http://www.apache.org/licenses/LICENSE-2.0 Apache License, Version 2.0
 * @link      https://github.com/chrisnoden
 */

namespace ChrisNoden\Synergy\Tests\Web;

use ChrisNoden\Synergy\Request\WebClient;
use ChrisNoden\Synergy\Tests\Providers;

class WebClientTest extends \PHPUnit_Framework_TestCase
{

    use Providers;

    /**
     * @dataProvider browserRequests
     */
    public function testBrowsers($testHeaders)
    {
        $user_agent = $testHeaders['HTTP_USER_AGENT'];
        $client = WebClient::buildClientData($user_agent, $testHeaders);

        list(,$dataset,) = explode('"', $this->getDataSetAsString(false));
        switch ($dataset) {
            case 'desktop':
                $this->assertEquals(true, $client->isDesktop());
                $this->assertEquals(false, $client->isMobile());
                $this->assertEquals(false, $client->isTablet());
                $this->assertEquals('Macintosh', $client->getPlatform());
                $this->assertEquals('Chrome', $client->getBrowser());
                $this->assertEquals('32.0.1700.77', $client->getBrowserVersion());
                break;

            case 'tablet':
                $this->assertEquals(false, $client->isDesktop());
                $this->assertEquals(false, $client->isMobile());
                $this->assertEquals(true, $client->isTablet());
                $this->assertEquals('iPad', $client->getPlatform());
                $this->assertEquals('Safari', $client->getBrowser());
                $this->assertEquals('6.0', $client->getBrowserVersion());
                break;

            case 'mobile':
                $this->assertEquals(false, $client->isDesktop());
                $this->assertEquals(true, $client->isMobile());
                $this->assertEquals(false, $client->isTablet());
                $this->assertEquals('iPhone', $client->getPlatform());
                $this->assertEquals('Safari', $client->getBrowser());
                $this->assertEquals('7534.48.3', $client->getBrowserVersion());
                break;

            default:
                $this->fail("Unknown dataset type");
        }
    }


    public function testParseUserAgent() {
        $uas = json_decode(file_get_contents(__DIR__ . '/user_agents.json'), true);
        $server_params = array(
            'HTTP_HOST'            => '127.0.0.1',
            'HTTP_USER_AGENT'      => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.77 Safari/537.36',
            'SERVER_NAME'          => '127.0.0.1',
            'SERVER_ADDR'          => '127.0.0.1',
            'SERVER_PORT'          => '80',
            'REMOTE_ADDR'          => '127.0.0.1',
            'SCRIPT_FILENAME'      => '/Users/chris/projects/phpinfo.php',
            'REQUEST_METHOD'       => 'GET',
            'QUERY_STRING'         => 'test=yes',
            'REQUEST_URI'          => '/foo?test=yes',
            'SCRIPT_NAME'          => '/foo',
            'PHP_SELF'             => '/foo',
        );

        foreach($uas as $ua => $expected) {
            $result = WebClient::buildClientData($ua, $server_params);
            $this->assertEquals($result->getPlatform(), $expected['platform']);
            $this->assertEquals($result->getBrowser(), $expected['browser']);
            $this->assertEquals($result->getBrowserVersion(), $expected['version']);
            $this->assertEquals($expected['device'], $result->getDevice(), sprintf("%s v%s on %s is not parsed as a %s browser", $result->getBrowser(), $result->getBrowserVersion(), $result->getPlatform(), $expected['device']));
        }
    }


    function testParseUserAgentEmpty() {
        $expected = array(
            'platform' => null,
            'browser'  => null,
            'version'  => null,
        );

        $server_params = array(
            'HTTP_HOST'            => '127.0.0.1',
            'HTTP_USER_AGENT'      => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.77 Safari/537.36',
            'SERVER_NAME'          => '127.0.0.1',
            'SERVER_ADDR'          => '127.0.0.1',
            'SERVER_PORT'          => '80',
            'REMOTE_ADDR'          => '127.0.0.1',
            'SCRIPT_FILENAME'      => '/Users/chris/projects/phpinfo.php',
            'REQUEST_METHOD'       => 'GET',
            'QUERY_STRING'         => 'test=yes',
            'REQUEST_URI'          => '/foo?test=yes',
            'SCRIPT_NAME'          => '/foo',
            'PHP_SELF'             => '/foo',
        );

        $result = WebClient::buildClientData('', $server_params);
        $this->assertEquals($result->getPlatform(), $expected['platform']);
        $this->assertEquals($result->getBrowser(), $expected['browser']);
        $this->assertEquals($result->getBrowserVersion(), $expected['version']);

        $result = WebClient::buildClientData('asdjkakljasdkljasdlkj', $server_params);
        $this->assertEquals($result->getPlatform(), $expected['platform']);
        $this->assertEquals($result->getBrowser(), $expected['browser']);
        $this->assertEquals($result->getBrowserVersion(), $expected['version']);

        $result = WebClient::buildClientData('Mozilla (asdjkakljasdkljasdlkj) BlahBlah', $server_params);
        $this->assertEquals($result->getPlatform(), $expected['platform']);
        $this->assertEquals($result->getBrowser(), $expected['browser']);
        $this->assertEquals($result->getBrowserVersion(), $expected['version']);
    }
}
