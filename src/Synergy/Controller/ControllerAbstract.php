<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Controller;

use ChrisNoden\Synergy\Request\RequestInterface;
use ChrisNoden\Synergy\Request\WebRequest;
use ChrisNoden\Synergy\View\Response;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use ChrisNoden\Synergy\Synergy\Config;

/**
 * Class ControllerAbstract
 *
 * @category ChrisNoden\Synergy\Controller
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class ControllerAbstract
{

    use LoggerAwareTrait;

    /** @var Config */
    protected $config;
    /** @var RequestInterface */
    protected $request;
    /** @var int cache max age of resource for caching */
    protected $max_age = 3600;


    /**
     * Set the logger
     *
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }


    /**
     * Set the Config object
     *
     * @param Config $config
     */
    public function setConfig(Config $config)
    {
        $this->config = $config;
    }


    /**
     * Set the Request object
     *
     * @param RequestInterface $request
     *
     * @return static
     */
    public function setRequest(RequestInterface $request)
    {
        $this->request = $request;

        if ($this->request instanceof WebRequest && $this->request->getMethod() == 'OPTIONS') {
            if (method_exists($this, 'optionsAction')) {
                return call_user_func(array($this, 'optionsAction'));
            } else {
                $response = new Response();
                $response->setOptionsResponse(true);

                return $response;
            }
        }

        return $this;
    }
}
