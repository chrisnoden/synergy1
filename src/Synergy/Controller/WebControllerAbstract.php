<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Controller;

use ChrisNoden\Synergy\Exception\SynergyException;
use ChrisNoden\Synergy\Exception\SynergyRequestException;
use ChrisNoden\Synergy\Request\RequestInterface;
use ChrisNoden\Synergy\Request\WebRequest;
use ChrisNoden\Synergy\Synergy\Config;
use ChrisNoden\Synergy\Template\TemplateFactory;
use ChrisNoden\Synergy\Template\TemplateInterface;
use ChrisNoden\Synergy\Type\TemplateType;
use ChrisNoden\Synergy\View\Response;

/**
 * Class WebControllerAbstract
 * Adds useful extra methods and properties when creating a child Web Controller class
 *
 * @category ChrisNoden\Synergy\Controller
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class WebControllerAbstract extends ControllerAbstract
{
    /** @var WebRequest */
    protected $request;
    /** @var bool */
    protected $isSSL = false;
    /** @var bool by default we ignore request for binary files */
    protected $ignoreBinaryRequests = true;
    /** @var string */
    protected $templateDir;
    /** @var string */
    protected $templatePath;


    public function __construct()
    {
        $this->detectSslConnection();
        $this->startSecureSessions();

        $this->setTemplateDirFromConfig();

        if (method_exists($this, 'init')) {
            call_user_func(array($this, 'init'));
        }
    }


    /**
     * Sets the object property, $isSSL, to true if the browser connection
     * is using SSL (tested using Apache server)
     *
     * @return void
     */
    protected function detectSslConnection()
    {
        // detect if the connection is using SSL/HTTPS
        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ||
            (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) {
            $this->isSSL = true;
        }
    }


    /**
     * Start session handling. Defaults to making them more secure than normal
     *
     * @link http://php.net/manual/en/function.session-set-cookie-params.php
     *
     * @param int $lifetime <p>
     * Lifetime of the
     * session cookie, defined in seconds.
     * </p>
     * @param string $path [optional] <p>
     * Path on the domain where
     * the cookie will work. Use a single slash ('/') for all paths on the
     * domain.
     * </p>
     * @param string $domain [optional] <p>
     * Cookie domain, for
     * example 'www.php.net'. To make cookies visible on all subdomains then
     * the domain must be prefixed with a dot like '.php.net'.
     * </p>
     * @param bool $secure [optional] <p>
     * If true cookie will only be sent over
     * secure connections.
     * </p>
     * @param bool $httponly [optional] <p>
     * If set to true then PHP will attempt to send the
     * httponly
     * flag when setting the session cookie.
     * </p>
     *
     * @return void
     */
    protected function startSecureSessions($lifetime = 0, $path = '/', $domain = null, $secure = null, $httponly = null)
    {
        if (is_null($domain) && isset($_SERVER['HTTP_HOST'])) {
            $domain = $_SERVER['HTTP_HOST'];
        }
        if (is_null($secure)) {
            $secure = $this->isSSL;
        }
        if (is_null($httponly)) {
            $httponly = true;
        }

        // Start sessions - locking them to SSL if the connection is using it
        if (!session_id() && !headers_sent()) {
            // lock down our session cookie
            session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
            session_start();
        }
    }


    /**
     * @param string $path [optional] directory to use as the root path for the template search
     * will use a config key 'synergy:template_dir' if this is not set
     *
     * @return TemplateInterface|bool false if no template file found
     * @throws \ChrisNoden\Synergy\Exception\SynergyException
     * @throws \ChrisNoden\Synergy\Exception\SynergyRequestException
     */
    protected function findRequestTemplate($path = null)
    {
        if (! ($this->request instanceof RequestInterface)) {
            throw new SynergyRequestException('Request not set, unable to match template file');
        }

        if (is_null($path)) {
            if (Config::get('synergy:template_dir')) {
                $this->templateDir = Config::get('synergy:template_dir');
            }
            if (isset($this->templateDir)) {
                $path = $this->templateDir;
            }
        }

        if (is_null($path)) {
            throw new SynergyException('Template path not set nor found in Config');
        }

        $templateName = $this->request->getPath();
        if (substr($templateName, 0, 1) == '/') {
            $templateName = substr($templateName, 1);
        }
        if (strlen($templateName) == 0 || $templateName == 'index.php') {
            $templateName = 'index';
        }

        // Look for a Smarty template to match the request
        if (file_exists($path . DIRECTORY_SEPARATOR . $templateName . '.html.tpl')) {
            $tpl    = TemplateFactory::create(
                TemplateType::SMARTY_TEMPLATE(),
                $templateName . '.html.tpl',
                $this->templateDir
            );
            // add template parameters
            $tpl->addParameter('pagename', $templateName);

            return $tpl;
        } elseif (file_exists($path . DIRECTORY_SEPARATOR . $templateName . DIRECTORY_SEPARATOR . 'index.html.tpl')) {
            $tpl    = TemplateFactory::create(
                TemplateType::SMARTY_TEMPLATE(),
                $templateName . DIRECTORY_SEPARATOR .  'index.html.tpl',
                $this->templateDir
            );
            // add template parameters
            $tpl->addParameter('pagename', $templateName . DIRECTORY_SEPARATOR .  'index');

            return $tpl;
        }

        return false;
    }


    /**
     * Looks for the synergy:template_dir key in the Config
     */
    protected function setTemplateDirFromConfig()
    {
        if (Config::get('synergy:template_dir')) {
            $this->templateDir = Config::get('synergy:template_dir');
            $this->templatePath = $this->templateDir;
        }
    }


    /**
     * Looks for the synergy:template_dir key in the Config
     *
     * @deprecated
     */
    protected function setTemplatePathFromConfig()
    {
        if (Config::get('synergy:template_dir')) {
            $this->templateDir = Config::get('synergy:template_dir');
            $this->templatePath = $this->templateDir;
        }
    }


    /**
     * Set the Request object
     * returns a 404 response if the request is for a common binary file (jpg, gif, etc)
     *
     * @param RequestInterface $request
     *
     * @return void|Response
     */
    public function setRequest(RequestInterface $request)
    {
        $this->request = $request;

        // is this a request for a binary file
        $ext = substr($this->request->getPath(), strrpos($this->request->getPath(), '.')+1);

        switch ($ext) {
            case 'jpg':
            case 'jpeg':
            case 'gif':
            case 'css':
            case 'png':
            case 'ico':
            case 'xml':
            case 'doc':
            case 'csv':
            case 'xls':
            case 'docx':
            case 'xlsx':
            case 'pdf':
            case 'js':
                return $this->errorResponse(404);
        }

        return false;
    }


    /**
     * Just return a basic HTTP response with the given status code
     *
     * @param int $code
     *
     * @return static
     */
    protected function errorResponse($code = 404)
    {
        $response = Response::create('Not found', $code);

        return $response;
    }


    /**
     * @return bool
     */
    public function isSsl()
    {
        return $this->isSSL;
    }
}
