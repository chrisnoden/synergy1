<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Controller;

use ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException;
use ChrisNoden\Synergy\Exception\SynergyException;
use ChrisNoden\Synergy\Exception\SynergyControllerNotFoundException;
use ChrisNoden\Synergy\Exception\SynergyInvalidControllerMethodException;
use ChrisNoden\Synergy\Synergy\Config;

/**
 * Class ControllerRouter
 *
 * @category ChrisNoden\Synergy\Controller
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class ControllerRouter
{
    /** @var string */
    protected $class_name;
    /** @var string */
    protected $method_name;


    /**
     * Find the controller and method to use from the controller string
     *
     * @param string $controller_string
     *
     * @return array className, methodName keys
     * @throws SynergyException
     * @throws SynergyInvalidArgumentException if the argument is not valid
     * @throws SynergyControllerNotFoundException
     * @throws SynergyInvalidControllerMethodException
     */
    public function parseControllerString($controller_string)
    {
        if (!is_string($controller_string)) {
            throw new SynergyInvalidArgumentException('controller_string expects a string value');
        }

        $class_root = Config::get('synergy:class_root');

        $arr = preg_split('/[^0-9a-zA-Z\_]/', $class_root . ':' . $controller_string);

        $test_class = implode('\\', $arr);
        if (class_exists($test_class)) {
            $this->class_name = $test_class;
            $this->method_name = 'defaultAction';

            return array(
                'className'  => $this->class_name,
                'methodName' => $this->method_name
            );
        }

        $test_method = $arr[count($arr)-1];
        unset($arr[count($arr)-1]);
        $test_class = implode('\\', $arr);
        if (class_exists($test_class)) {
            if (method_exists($test_class, $test_method . 'Action')) {
                $this->class_name = $test_class;
                $this->method_name = $test_method.'Action';

                return array(
                    'className'  => $this->class_name,
                    'methodName' => $this->method_name
                );
            } else {
                throw new SynergyInvalidControllerMethodException(
                    'Controller method '.$test_method.'() not found in class '.$test_class
                );
            }
        }

        throw new SynergyControllerNotFoundException('Controller not located in project');
    }


    /**
     * Value of member class_name
     *
     * @return string value of member
     */
    public function getClassName()
    {
        return $this->class_name;
    }


    /**
     * Value of member method_name
     *
     * @return string value of member
     */
    public function getMethodName()
    {
        return $this->method_name;
    }
}
