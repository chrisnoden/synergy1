<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Exception;

use ChrisNoden\Synergy\Synergy\Config;

/**
 * Class ErrorHandler
 *
 * @category ChrisNoden\Synergy\Construct
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class ErrorHandler
{

    public static function register()
    {
        register_shutdown_function(__CLASS__ . '::shutdown');

        /**
         * throw exceptions based on E_* error types
         */
        /** @noinspection PhpUnusedParameterInspection */
        set_error_handler(
            function ($err_severity, $err_msg, $err_file, $err_line, array $err_context) {

                // error was suppressed with the @-operator
                if (0 === error_reporting()) {
                    return false;
                }
                switch ($err_severity) {
                    case E_ERROR:
                        throw new SynergyErrorException($err_msg, 0, $err_severity, $err_file, $err_line);

                    case E_WARNING:
                        throw new WarningException($err_msg, 0, $err_severity, $err_file, $err_line);

                    case E_PARSE:
                        throw new ParseException($err_msg, 0, $err_severity, $err_file, $err_line);

                    case E_NOTICE:
                        Config::logger()->notice($err_msg, array('file' => $err_file, 'line' => $err_line));
                        break;

                    case E_CORE_ERROR:
                        throw new CoreErrorException($err_msg, 0, $err_severity, $err_file, $err_line);

                    case E_CORE_WARNING:
                        throw new CoreWarningException($err_msg, 0, $err_severity, $err_file, $err_line);

                    case E_COMPILE_ERROR:
                        throw new CompileErrorException($err_msg, 0, $err_severity, $err_file, $err_line);

                    case E_COMPILE_WARNING:
                        throw new CoreWarningException($err_msg, 0, $err_severity, $err_file, $err_line);

                    case E_USER_ERROR:
                        throw new UserErrorException($err_msg, 0, $err_severity, $err_file, $err_line);

                    case E_USER_WARNING:
                        throw new UserWarningException($err_msg, 0, $err_severity, $err_file, $err_line);

                    case E_USER_NOTICE:
                        throw new UserNoticeException($err_msg, 0, $err_severity, $err_file, $err_line);

                    case E_STRICT:
                        throw new StrictException($err_msg, 0, $err_severity, $err_file, $err_line);

                    case E_RECOVERABLE_ERROR:
                        throw new RecoverableErrorException($err_msg, 0, $err_severity, $err_file, $err_line);

                    case E_DEPRECATED:
                        Config::logger()->warning($err_msg, array('file' => $err_file, 'line' => $err_line));
                        break;

                    case E_USER_DEPRECATED:
                        throw new UserDeprecatedException($err_msg, 0, $err_severity, $err_file, $err_line);
                }
            }
        );
    }


    /**
     * Handle unwanted termination of the script
     */
    public static function shutdown()
    {
        $isError = false;

        if ($error = error_get_last()) {
            switch($error['type']){
                case E_ERROR:
                case E_CORE_ERROR:
                case E_COMPILE_ERROR:
                case E_USER_ERROR:
                    $isError = true;
                    break;
            }
        }

        if ($isError) {
            var_dump($error);
        }
    }
}


class WarningException extends SynergyErrorException
{

}


class ParseException extends SynergyErrorException
{

}


class NoticeException extends SynergyErrorException
{

}


class CoreErrorException extends SynergyErrorException
{

}


class CoreWarningException extends SynergyErrorException
{

}


class CompileErrorException extends SynergyErrorException
{

}


class CompileWarningException extends SynergyErrorException
{

}


class UserErrorException extends SynergyErrorException
{

}


class UserWarningException extends SynergyErrorException
{

}


class UserNoticeException extends SynergyErrorException
{

}


class StrictException extends SynergyErrorException
{

}


class RecoverableErrorException extends SynergyErrorException
{

}


class DeprecatedException extends SynergyErrorException
{

}


class UserDeprecatedException extends SynergyErrorException
{

}
