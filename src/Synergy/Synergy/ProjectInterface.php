<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Synergy;

use Psr\Log\LoggerInterface;
use ChrisNoden\Synergy\Type\ProjectType;

/**
 * Class ProjectInterface
 *
 * @category ChrisNoden\Synergy\Construct
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
interface ProjectInterface 
{

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger);


    /**
     * Set the Logger for the project
     *
     * @param LoggerInterface $logger
     *
     * @return $this
     */
    public function setLogger(LoggerInterface $logger);


    /**
     * Set the config filename
     *
     * @param string $filename absolute filename to the config file
     *
     * @return $this
     */
    public function setConfigFile($filename);


    /**
     * What project type is this
     *
     * @return ProjectType
     */
    public function getType();


    /**
     * @return $this
     */
    public function run();
}
 