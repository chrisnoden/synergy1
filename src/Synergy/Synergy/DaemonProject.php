<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Synergy;

use ChrisNoden\Synergy\Controller\ControllerRouter;
use ChrisNoden\Synergy\Request\RequestFactory;
use ChrisNoden\Synergy\Type\ProjectType;
use Psr\Log\LoggerInterface;
use ChrisNoden\Synergy\Request\ConsoleRequest;

/**
 * Class DaemonProject
 *
 * @category ChrisNoden\Synergy\Synergy
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class DaemonProject extends ConsoleProject implements ProjectInterface
{


    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->setType(ProjectType::DAEMON_PROJECT());
        $this->setLogger($logger);
        Config::setLogger($logger);
    }


    /**
     * @return $this
     */
    public function run()
    {
        /** @var ConsoleRequest request */
        $this->request = RequestFactory::create(ProjectType::DAEMON_PROJECT(), $this->logger);

        $this->runBootstrap();

        $cr = new ControllerRouter();
        $controller = $cr->parseControllerString($this->request->getArguments()->node(0));

        if (is_array($controller) && isset($controller['className'])) {
            $className = $controller['className'];
            $methodName = $controller['methodName'];
            $class = new $className();
            if (method_exists($class, 'setRequest')) {
                call_user_func(array($class, 'setRequest'), $this->request);
            }
            $response = call_user_func(array($class, $methodName));
        }
    }
}
