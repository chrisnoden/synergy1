<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Synergy;

use ChrisNoden\Synergy\Exception\SynergyFileNotReadableException;
use ChrisNoden\Synergy\Parser\YamlParser;
use ChrisNoden\Synergy\Type\Singleton;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Class Config
 *
 * @category ChrisNoden\Synergy\Synergy
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class Config extends Singleton
{
    /** @var array */
    protected static $config = array();
    /** @var array */
    protected static $vars = array();
    /** @var bool the config vars need parsing and replacing */
    protected static $dirty = true;
    /** @var LoggerInterface */
    protected static $logger;


    /**
     * Create a Config object from the given file
     *
     * @param string $filename
     *
     * @returns Config
     * @throws SynergyFileNotReadableException
     */
    public static function createFromFile($filename)
    {
        if (!is_readable($filename)) {
            throw new SynergyFileNotReadableException($filename . ' file not readable');
        }
        self::setConfig(YamlParser::YAMLLoad($filename));
        self::addVar('config.dir', dirname($filename));
        self::addVar('config.basename', basename($filename));
        self::addVar('config.filename', $filename);

        return self::$instance;
    }


    protected static function setDefaultVars()
    {
        self::addVar('synergy.dir', dirname(__DIR__));
        self::addVar('synergy.examples.dir', dirname(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR . 'examples');
    }


    /**
     * Directory containing the Synergy1 library
     *
     * @return string
     */
    public static function getSynergyLibraryDir()
    {
        return dirname(__DIR__);
    }


    /**
     * Directory that contains the Synergy examples
     *
     * @return string
     */
    public static function getSynergyExamplesDir()
    {
        return dirname(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR . 'examples';
    }


    /**
     * Directory that apache is serving web files from
     *
     * @return string
     */
    public static function getWebHostRootDir()
    {
        return $_SERVER['DOCUMENT_ROOT'];
    }


    /**
     * Set the config array
     *
     * @param array $config
     *
     * @return Config
     */
    public static function setConfig(Array $config)
    {
        self::$config = $config;
        self::$dirty = true;

        self::setDefaultVars();

        return self::getInstance();
    }


    /**
     * Replace vars in the config with actual values
     */
    protected static function fixConfigData()
    {
        array_walk_recursive(self::$config, 'self::configReplaceVars');

        self::$dirty = false;
    }


    /**
     * @param LoggerInterface $logger
     *
     * @return Config
     */
    public static function setLogger(LoggerInterface $logger)
    {
        self::$logger = $logger;

        return self::getInstance();
    }


    /**
     * @return LoggerInterface|NullLogger
     */
    public static function getLogger()
    {
        if (!(self::$logger instanceof LoggerInterface)) {
            self::$logger = new NullLogger();
        }

        return self::$logger;
    }


    /**
     * @return LoggerInterface|NullLogger
     */
    public static function logger()
    {
        if (!(self::$logger instanceof LoggerInterface)) {
            self::$logger = new NullLogger();
        }

        return self::$logger;
    }


    /**
     * Get the value of the config key
     *
     * @param string $key colon delimited string for the key item from the config
     *
     * @return mixed|bool false if the key is not found
     */
    public static function get($key)
    {
        if (self::$dirty) {
            self::fixConfigData();
        }
        $arr = explode(':', $key);
        $refArray = self::$config;
        foreach ($arr as $item)
        {
            if (isset($refArray[$item])) {
                $refArray = $refArray[$item];
            } else {
                return false;
            }
        }

        return $refArray;
    }


    /**
     * @param mixed $val
     * @param string $key
     */
    protected static function configReplaceVars(&$val, $key)
    {
        if (!is_string($val)) {
            return;
        }
        foreach (self::$vars as $var => $repl) {
            $pattern = '/\%'.$var.'\%/';
            $val = preg_replace($pattern, $repl, $val);
        }
    }


    /**
     * Add a var for substitution in the config
     *
     * @param string $name
     * @param string $value
     *
     * @return Config
     */
    public static function addVar($name, $value)
    {
        self::$vars[$name] = $value;
        self::$dirty = true;

        return self::getInstance();
    }


    /**
     * Add a collection of vars (key => value pairs)
     *
     * @param array $vars
     *
     * @return Config
     */
    public static function addVars(Array $vars)
    {
        self::$vars = array_merge(self::$vars, $vars);

        return self::getInstance();
    }


    /**
     * Return the config var
     *
     * @return array
     */
    public static function asArray()
    {
        if (self::$dirty) {
            self::fixConfigData();
        }

        return self::$config;
    }


    /**
     * Flatten the entire config, merging keys with a colon delimiter
     * with : (colon) delimiters
     * eg synergy:routes
     *
     * @return array
     */
   public static function flatten()
   {
        if (self::$dirty) {
            self::fixConfigData();
        }

        return self::flattenArray(array_merge(self::$vars, self::$config));
   }


    /**
     * Recursive method to flatten the array using colon delimiters
     *
     * @param array $arr
     * @param array $stub
     *
     * @return array
     */
    protected static function flattenArray($arr, $stub = array())
    {
        $result = array();
        foreach ($arr as $key => $val) {
            if (is_array($val)) {
                $newkey = implode(':', $stub);
                $result = array_merge($result, self::flattenArray($val, array_merge($stub, array($key))));
            } else {
                $key = implode(':', array_merge($stub, array($key)));
                $result[$key] = $val;
            }
        }

        return $result;
    }
}
