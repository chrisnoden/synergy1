<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Synergy;

use ChrisNoden\Synergy\Exception\SynergyException;
use ChrisNoden\Synergy\Type\ProjectType;
use Psr\Log\LoggerInterface;

/**
 * Class ProjectFactory
 *
 * @category ChrisNoden\Synergy\Construct
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class ProjectFactory
{

    /**
     * @param ProjectType     $type
     * @param string          $configFilename absolute filename to the config.yml file
     * @param string          $cacheDirectory path of a directory we can use for temp and cache files
     * @param LoggerInterface $logger         Logger object (must implement Psr\Log\LoggerInterface)
     *
     * @return WebProject
     * @throws \ChrisNoden\Synergy\Exception\SynergyException
     */
    public static function create(
        ProjectType $type,
        $configFilename,
        $cacheDirectory,
        LoggerInterface $logger
    ) {
        switch ($type) {
            case ProjectType::WEB_PROJECT():
                $project = new WebProject($logger);
                break;

            case ProjectType::CONSOLE_PROJECT():
                $project = new ConsoleProject($logger);
                break;

            default:
                throw new SynergyException('Unsupported ProjectType '.$type->key());
        }

        // Set the config file
        $project
            ->setConfigFile($configFilename)
            ->setCacheDirectory($cacheDirectory);

        return $project;
    }
}
