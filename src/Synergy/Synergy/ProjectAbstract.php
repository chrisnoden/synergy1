<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Synergy;

use ChrisNoden\Synergy\Exception\SynergyFileNotReadableException;
use ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException;
use ChrisNoden\Synergy\Tools\Tools;
use ChrisNoden\Synergy\Type\ProjectType;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use ChrisNoden\Synergy\Construct\SynergyContainer;

/**
 * Class ProjectAbstract
 *
 * @category ChrisNoden\Synergy\Construct
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
abstract class ProjectAbstract
{

    use LoggerAwareTrait;

    /** @var SynergyContainer */
    protected $c;
    /** @var ProjectType */
    protected $type;
    /** @var string */
    protected $config_filename;
    /** @var Config */
    protected $config;
    /** @var string */
    protected $cache_directory;


    /**
     * @return $this
     */
    abstract public function run();


    /**
     * Call any bootstrap file declared in the config
     *
     * @return void
     */
    protected function runBootstrap()
    {
        $bootstrap = Config::get('synergy:bootstrap');
        if ($bootstrap && file_exists($bootstrap)) {
            @include_once($bootstrap);
        }
    }


    /**
     * Set the Logger for the project
     *
     * @param LoggerInterface $logger
     *
     * @return $this
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;

        return $this;
    }


    /**
     * Set the project type
     *
     * @param ProjectType $type
     *
     * @return $this
     */
    protected function setType(ProjectType $type)
    {
        $this->type = $type;

        return $this;
    }


    /**
     * What project type is this
     *
     * @return ProjectType
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Set the config filename
     *
     * @param string $filename absolute filename to the config file
     *
     * @return $this
     * @throws SynergyFileNotReadableException if the filename is not readable by the Synergy process
     */
    public function setConfigFile($filename)
    {
        $this->config = Config::createFromFile($filename);

        return $this;
    }


    /**
     * Set the directory to use for cache and temp files
     * If not set then Synergy will attempt to use the current OS defined temp dir
     *
     * @param string $directory
     *
     * @return $this
     * @throws SynergyInvalidArgumentException if the directory is not writeable by the Synergy process
     */
    public function setCacheDirectory($directory)
    {
        if (!file_exists($directory)) {
            Tools::mkdir($directory);
        }
        if (!is_writable($directory)) {
            throw new SynergyInvalidArgumentException($directory . ' is not writeable');
        }
        $this->cache_directory = $directory;

        return $this;
    }
}
