<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Synergy;

use ChrisNoden\Synergy\Exception\SynergyException;
use ChrisNoden\Synergy\Exception\SynergyFileNotReadableException;
use ChrisNoden\Synergy\Request\RequestFactory;
use ChrisNoden\Synergy\Routing\Router;
use ChrisNoden\Synergy\Type\ProjectType;
use ChrisNoden\Synergy\View\ResponseInterface;
use Psr\Log\LoggerInterface;
use ChrisNoden\Synergy\View\Response;
use ChrisNoden\Synergy\Request\WebRequest;
use ChrisNoden\Synergy\Routing\Route;

/**
 * Class WebProject
 *
 * @category ChrisNoden\Synergy\Construct
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class WebProject extends ProjectAbstract implements ProjectInterface
{
    /** @var WebRequest */
    protected $request;
    /** @var Router */
    protected $router;
    /** @var Route */
    protected $route;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->setType(ProjectType::WEB_PROJECT());
        $this->setLogger($logger);
        Config::setLogger($logger);
    }


    /**
     * @return $this
     * @throws SynergyException
     */
    public function run()
    {
        // Set the routes file
        if ($this->config->get('synergy:routes')) {
            $this->setRouteFile($this->config->get('synergy:routes'));
        }

        if (is_null($this->request)) {
            $this->request = RequestFactory::create($this->type, $this->logger);
        }

        $this->runBootstrap();

        /** @var \ChrisNoden\Synergy\Routing\Router $router */
        $this->router = Router::createFromFile($this->config->get('synergy:routes'), $this->logger);
        $this->route = $this->router->match($this->request);

        /** @var Response $response */
        $response = $this->route->launchAction();
        if ($response instanceof ResponseInterface) {
            $response->setCacheDirectory($this->cache_directory);
            try {
                $response->send();
            } catch (\Exception $ex) {
                var_dump($ex);
            }
        } elseif (is_string($response)) {
            print((string)$response);
        } else {
            throw new SynergyException('Your Controller must return a ResponseInterface response');
        }

        return $this;
    }


    /**
     * @param string $filename
     *
     * @throws SynergyFileNotReadableException if the filename is not readable by the Synergy process
     *
     * @return $this
     */
    protected function setRouteFile($filename)
    {
        if (!is_readable($filename)) {
            throw new SynergyFileNotReadableException($filename . ' file not readable');
        }

        $this->router = Router::createFromFile($filename, $this->logger);

        return $this;
    }


    /**
     * Set the Request to be used
     *
     * @param WebRequest $request
     *
     * @return $this
     */
    public function setRequest(WebRequest $request)
    {
        $this->request = $request;

        return $this;
    }
}
