<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Synergy;

use ChrisNoden\Synergy\Controller\ControllerRouter;
use ChrisNoden\Synergy\Request\ConsoleRequest;
use ChrisNoden\Synergy\Request\RequestFactory;
use ChrisNoden\Synergy\Type\ProjectType;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

/**
 * Class ConsoleProject
 *
 * @category ChrisNoden\Synergy\Synergy
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class ConsoleProject extends ProjectAbstract implements ProjectInterface
{
    /** @var ConsoleRequest */
    protected $request;
    /** @var bool */
    protected static $ignoreSignals = false;
    /** @var bool */
    protected static $exit = false;


    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        if (PHP_SAPI === 'cli') {
            declare(ticks = 1);
            register_tick_function(array(&$this, "checkExit"));
            $this->registerSignalHandler();
        }

        $this->setType(ProjectType::CONSOLE_PROJECT());
        $this->setLogger($logger);
    }


    /**
     * @return $this
     */
    public function run()
    {
        /** @var ConsoleRequest request */
        $this->request = RequestFactory::create(ProjectType::CONSOLE_PROJECT(), $this->logger);
        // Set up any logging to the console
        $this->setConsoleLogging();
        $this->runBootstrap();
        Config::setLogger($this->logger);

        $cr = new ControllerRouter();
        $controller = $cr->parseControllerString($this->request->getArguments()->node(0));

        if (is_array($controller) && isset($controller['className'])) {
            $className = $controller['className'];
            $methodName = $controller['methodName'];
            $class = new $className();
            if (method_exists($class, 'setLogger')) {
                call_user_func(array($class, 'setLogger'), $this->logger);
            }
            if (method_exists($class, 'setRequest')) {
                call_user_func(array($class, 'setRequest'), $this->request);
            }
            $response = call_user_func(array($class, $methodName));
        }
    }


    /**
     * Configure how much logging (if any) is sent to the console
     */
    protected function setConsoleLogging()
    {
        if (PHP_SAPI === 'cli' && class_exists('\Monolog\Logger') && method_exists($this->logger, 'pushHandler')) {
            if ($this->request->get('vvv')) {
                $this->logger->pushHandler(
                    new StreamHandler(
                        "php://stdout",
                        Logger::DEBUG
                    )
                );
            } elseif ($this->request->get('vv')) {
                $this->logger->pushHandler(
                    new StreamHandler(
                        "php://stdout",
                        Logger::INFO
                    )
                );
            } elseif ($this->request->get('v')) {
                $this->logger->pushHandler(
                    new StreamHandler(
                        "php://stdout",
                        Logger::NOTICE
                    )
                );
            } else {
                $this->logger->pushHandler(
                    new StreamHandler(
                        "php://stderr",
                        Logger::ALERT
                    )
                );
            }
        }
    }
    

    /**
     * Registers a signal handler method to respond
     * to any kill signals
     */
    protected function registerSignalHandler()
    {
        if (function_exists('pcntl_signal') && PHP_SAPI === 'cli') {
            // ignore these signals
            pcntl_signal(SIGTSTP, SIG_IGN);
            pcntl_signal(SIGTTOU, SIG_IGN);
            pcntl_signal(SIGTTIN, SIG_IGN);
            // trap these signals
            pcntl_signal(SIGHUP, array(&$this, "handleHupSignal"));
            pcntl_signal(SIGTERM, array(&$this, "handleSignals"));
            pcntl_signal(SIGINT, array(&$this, "handleSignals"));
            pcntl_signal(SIGABRT, array(&$this, "handleSignals"));
        }
    }


    /**
     * Respond to a signal
     *
     * @param $signal
     */
    public function handleSignals($signal)
    {
        $signals = array(
            'UNKNOWN'   => 0,
            'SIGHUP'    => SIGHUP,
            'SIGINT'    => SIGINT,
            'SIGQUIT'   => SIGQUIT,
            'SIGILL'    => SIGILL,
            'SIGTRAP'   => SIGTRAP,
            'SIGABRT'   => SIGABRT,
            'SIGBUS'    => SIGBUS,
            'SIGFPE'    => SIGFPE,
            'SIGUSR1'   => SIGUSR1,
            'SIGSEGV'   => SIGSEGV,
            'SIGUSR2'   => SIGUSR2,
            'SIGPIPE'   => SIGPIPE,
            'SIGALRM'   => SIGALRM,
            'SIGTERM'   => SIGTERM,
            'SIGCHLD'   => SIGCHLD,
            'SIGCONT'   => SIGCONT,
            'SIGTSTP'   => SIGTSTP,
            'SIGTTIN'   => SIGTTIN,
            'SIGTTOU'   => SIGTTOU,
            'SIGURG'    => SIGURG,
            'SIGXCPU'   => SIGXCPU,
            'SIGXFSZ'   => SIGXFSZ,
            'SIGVTALRM' => SIGVTALRM,
            'SIGPROF'   => SIGPROF,
            'SIGWINCH'  => SIGWINCH,
            'SIGIO'     => SIGIO,
            'SIGSYS'    => SIGSYS,
        );

        $signame = array_search($signal, $signals);
        if (!$signame) {
            $signame = 'UNKNOWN';
        }
        if (self::$ignoreSignals !== true) {
            $this->logger->critical(
                sprintf('Exiting : %s signal received', $signame)
            );
            exit;
        } else {
            $this->logger->critical(
                sprintf('%s signal received : Exit queued', $signame)
            );
            self::$exit = true;
        }
    }


    /**
     * Process a SIGHUP
     *
     * @param $signal
     */
    public function handleHupSignal($signal)
    {

    }


    /**
     * If an exit has been requested and we have removed the block
     * then we can now exit
     */
    public function checkExit()
    {
        if (self::$exit === true && self::$ignoreSignals !== true) {
            exit;
        }
    }


    /**
     * Set the Request to be used
     *
     * @param ConsoleRequest $request
     *
     * @return $this
     */
    public function setRequest(ConsoleRequest $request)
    {
        $this->request = $request;

        return $this;
    }


    /**
     * Catch signals and hold them until later
     */
    public static function ignoreSignals()
    {
        self::$ignoreSignals = true;
    }


    /**
     * Accept signals, process any that have been held
     */
    public static function acceptSignals()
    {
        self::$ignoreSignals = false;
    }
}
