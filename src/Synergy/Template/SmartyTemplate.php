<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Template;

use ChrisNoden\Synergy\Exception\ErrorHandler;
use ChrisNoden\Synergy\Exception\SynergyTemplateConfigurationException;
use ChrisNoden\Synergy\Synergy\Config;
use ChrisNoden\Synergy\Tools\Tools;
use Psr\Log\LoggerInterface;

/**
 * Class SmartyTemplate
 * Handle Smarty 3.x template responses
 *
 * @category ChrisNoden\Synergy\View
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class SmartyTemplate extends TemplateAbstract implements TemplateInterface
{
    /**
     * @var \Smarty
     */
    private $loader;
    /** @var array */
    private $plugin_dirs = array();


    /**
     * Initialise Smarty
     *
     * @return void
     */
    protected function initTemplateEngine()
    {
        $this->loader = new \Smarty();
        $this->loader->muteExpectedErrors();
        $this->loader->setTemplateDir($this->getTemplateDir());

        if (Config::get('dev') && Config::get('dev') == 'yes') {
            $this->cache = false;
        }
        $this->initSmartyCache();

        if (Config::get('synergy:smarty_plugin_dir')) {
            $this->addPluginDir(Config::get('synergy:smarty_plugin_dir'));
        }

        foreach ($this->plugin_dirs as $dir) {
            $this->loader->addPluginsDir($dir);
        }

        if ($this->logger instanceof LoggerInterface) {
            $this->logger->debug("Smarty Cache Dir: ".$this->loader->getCacheDir());
        }
    }


    /**
     * template render output
     *
     * @return string template render output
     * @throws SynergyTemplateConfigurationException
     * @throws \Exception
     */
    public function getRender()
    {
        ErrorHandler::register();

        if (!($this->loader instanceof \Smarty)) {
            $this->initTemplateEngine();
        }

        if (isset($this->content)) {
            $this->assignSmartyVariables();
            try {
                $ret = $this->loader->fetch('eval:'.$this->content);
                return $ret;
            } catch (\Exception $ex) {
                throw $ex;
            }
        } else if (isset($this->templateFile)) {
            $this->assignSmartyVariables();
            try {
                if ($this->cache === false) {
                    $this->loader->clearCache($this->templateFile, $this->cache_identifier);
                }
                $ret = $this->loader->fetch($this->templateFile, $this->cache_identifier);
                return $ret;
            } catch (\Exception $ex) {
                throw $ex;
            }
        } else {
            throw new SynergyTemplateConfigurationException(
                sprintf(
                    'Invalid call to %s without setting templateFile',
                    __METHOD__
                )
            );
        }
    }


    /**
     * Add a Smarty plugin directory
     *
     * @param string $dir valid directory
     */
    public function addPluginDir($dir)
    {
        if (is_dir($dir) && is_readable($dir)) {
            $this->plugin_dirs[] = $dir;
        }
    }


    /**
     * Prepares the cache folder for Smarty
     *
     * @return void
     */
    private function initSmartyCache()
    {
        if ($this->cache === false) {
//            $this->logger->alert('DEV MODE : CACHING DISABLED');
            $this->loader->setCompileDir(sys_get_temp_dir());
            $this->loader->caching = \Smarty::CACHING_OFF;
            return;
        }

        if (!is_dir($this->cacheDir)) {
            Tools::mkdir($this->cacheDir, true);
        }

        // compiled templates dir
        $path
            = $this->cacheDir .
            DIRECTORY_SEPARATOR .
            'templates_c' .
            DIRECTORY_SEPARATOR;
        if (!is_dir($path)) {
            Tools::mkdir($path, false);
        }
        $this->loader->setCompileDir($path);

        // cache dir
        $path
            = $this->cacheDir .
            DIRECTORY_SEPARATOR .
            'cache' .
            DIRECTORY_SEPARATOR;
        if (!is_dir($path)) {
            Tools::mkdir($path, false);
        }
        $this->loader->setCacheDir($path);

        // configs dir
        $path
            = $this->cacheDir .
            DIRECTORY_SEPARATOR .
            'configs' .
            DIRECTORY_SEPARATOR;
        if (!is_dir($path)) {
            Tools::mkdir($path, false);
        }
        $this->loader->setConfigDir($path);
    }


    /**
     * Set the variables so Smarty can access them
     *
     * @return void
     */
    protected function assignSmartyVariables()
    {
        $this->loader->assign($this->getParameters());
    }
}
