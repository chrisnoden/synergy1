<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Template;

use ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException;

/**
 * Class TemplateInterface
 *
 * @category ChrisNoden\Synergy\Template
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
interface TemplateInterface
{

    /**
     * The fully rendered output
     *
     * @return string
     */
    public function __toString();


    /**
     * The fully rendered output
     * @return string
     */
    public function getRender();


    /**
     * Location of the root templates directory
     *
     * @param string $directory absolute location of the web templates directory
     *
     * @return $this
     */
    public function setTemplateDir($directory);


    /**
     * Location of the templates directory used by this object
     * A concatenation of the projectTemplateDir and templateDir
     *
     * @return string location of the web templates
     */
    public function getTemplateDir();


    /**
     * Should be relative to the TemplateDir
     *
     * @param string $filename
     *
     * @return $this
     */
    public function setTemplateFile($filename);


    /**
     * relative filename of the template
     *
     * @return string relative filename of the template
     */
    public function getTemplateFile();


    /**
     * Turn on the template engine's own cache
     *
     * @param bool $status true = on
     *
     * @return $this
     */
    public function setCacheStatus($status);


    /**
     * Path to a writeable directory for the template engine's own cache files
     *
     * @param string $directory
     *
     * @return $this
     */
    public function setCacheDir($directory);


    /**
     * @param string $id
     *
     * @throws \ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException
     * @return $this
     */
    public function setCacheIdentifier($id);


    /**
     * add (merge) an array of template params in key=>value pairs
     *
     * @param array $parameters
     *
     * @return mixed
     */
    public function setParameters(Array $parameters);


    /**
     * Set the template content to a string value rather than parsing a file
     *
     * @param string $content
     *
     * @return $this
     * @throws SynergyInvalidArgumentException if the content is invalid
     */
    public function setContent($content);


    /**
     * add a parameter for the template
     *
     * @param $name
     * @param $value
     *
     * @return $this
     */
    public function addParameter($name, $value);


    /**
     * the parameters/variables for the template
     *
     * @return array associative array of variables/parameters
     */
    public function getParameters();
}
