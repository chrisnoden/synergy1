<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Template;

use ChrisNoden\Synergy\Exception\SynergyInvalidArgumentException;
use ChrisNoden\Synergy\Exception\SynergyTemplateConfigurationException;
use ChrisNoden\Synergy\Synergy\Config;
use ChrisNoden\Synergy\Tools\Tools;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;

/**
 * Class TemplateAbstract
 *
 * @category ChrisNoden\Synergy\View
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
abstract class TemplateAbstract
{
    use LoggerAwareTrait;

    /** @var string path to the root template dir */
    protected $templateDir;
    /** @var string relative filename of the template on the filesystem */
    protected $templateFile;
    /** @var array associative array of parameters to pass to the template */
    protected $parameters = array();
    /** @var string path to the cache directory */
    protected $cacheDir;
    /** @var bool should we cache the template render */
    protected $cache = false;
    /** @var string value to create separate caches (eg by user) */
    protected $cache_identifier;
    /** @var string */
    protected $content;


    public function __construct()
    {
        $this->logger = Config::getLogger();
    }


    /**
     * The fully rendered output
     *
     * @return string
     */
    public function __toString()
    {
        try {
            return (string) $this->getRender();
        } catch (\Exception $ex) {
            $message = sprintf(
                '<div style="font-size:28px;">Template Rendering Error</div><br><div style="font-size:14px;">%s</div><br><tt>Template: %s</tt>',
                $ex->getMessage(),
                $this->getTemplateFile()
            );
            if ($this->logger instanceof LoggerInterface) {
                $this->logger->critical(sprintf('%s with template %s', $ex->getMessage(), $this->getTemplateFile()));
            }
            return $message;
        }
    }


    /**
     * The fully rendered output
     *
     * @return string
     */
    abstract public function getRender();


    /**
     * Location of the web templates directory
     *
     * @param string $dir absolute location of the web templates directory
     *
     * @return $this
     * @throws SynergyInvalidArgumentException if the directory is not valid
     */
    public function setTemplateDir($dir)
    {
        if (strlen($dir) == 0 || !is_dir($dir)) {
            throw new SynergyInvalidArgumentException('Valid directory path required by setTemplateDir()');
        }
        if (!is_readable($dir)) {
            throw new SynergyInvalidArgumentException('Template dir '.$dir.' not readable');
        }
        $this->templateDir = $dir;

        return $this;
    }


    /**
     * Location of the templates directory
     *
     * @return string rppt location of the web templates
     */
    public function getTemplateDir()
    {
        return $this->templateDir;
    }


    /**
     * Tests that the template file exists
     *
     * @throws SynergyTemplateConfigurationException
     * @return void
     */
    protected function testTemplateFile()
    {
        if (!isset($this->templateFile)) {
            throw new SynergyTemplateConfigurationException(
                'templateFile not set'
            );
        }
        if (!is_dir($this->getTemplateDir())) {
            throw new SynergyTemplateConfigurationException(
                'templateDir not set or not valid'
            );
        }

        $testFile = $this->getTemplateDir() . DIRECTORY_SEPARATOR . $this->templateFile;
        if (!file_exists($testFile)) {
            throw new SynergyTemplateConfigurationException(
                sprintf("Template File %s not found", $testFile)
            );
        } else {
            if (!is_readable($testFile)) {
                throw new SynergyTemplateConfigurationException(
                    sprintf("Template File %s not readable", $testFile)
                );
            }
        }
    }


    /**
     * Filename of the template which can be relative to the templateDir or absolute
     *
     * @param string $filename relative filename of the template
     *
     * @return $this
     */
    public function setTemplateFile($filename)
    {
        $this->templateFile = preg_replace('/^(\/){1,}/', '', $filename);

        return $this;
    }


    /**
     * relative filename of the template
     *
     * @return string relative filename of the template
     */
    public function getTemplateFile()
    {
        return $this->templateFile;
    }


    /**
     * add parameters for the template (variables)
     *
     * @param array $parameters parameters to pass to template
     *
     * @return $this
     */
    public function setParameters(Array $parameters)
    {
        $this->parameters = array_merge($this->parameters, $parameters);

        return $this;
    }


    /**
     * the parameters/variables for the template
     *
     * @return array associative array of variables/parameters
     */
    public function getParameters()
    {
        return array_merge(array('config' => Config::asArray()), $this->parameters);
    }


    /**
     * add a parameter for the template
     *
     * @param $name
     * @param $value
     *
     * @return $this
     */
    public function addParameter($name, $value)
    {
        $this->parameters[$name] = $value;

        return $this;
    }


    /**
     * Location of the template cache directory
     *
     * @param string $directory absolute location of the template cache directory
     *
     * @throws SynergyTemplateConfigurationException
     * @return $this
     */
    public function setCacheDir($directory)
    {
        if (!is_dir($directory) && !Tools::mkdir($directory)) {
            throw new SynergyTemplateConfigurationException(
                sprintf("Invalid directory, %s", $directory)
            );
        } elseif (!is_readable($directory)) {
            throw new SynergyTemplateConfigurationException(
                sprintf("Directory %s not readable", $directory)
            );
        }
        $this->cacheDir = $directory;

        return $this;
    }


    /**
     * Location of the template cache directory
     *
     * @return string location of the web templates
     */
    public function getCacheDir()
    {
        return $this->cacheDir;
    }


    /**
     * Enable the Smarty cache
     *
     * @param bool $status
     *
     * @return $this
     */
    public function setCacheStatus($status)
    {
        if ($status === true) {
            $this->cache = true;
        } else {
            $this->cache = false;
        }

        return $this;
    }


    /**
     * @param string $id
     *
     * @throws SynergyInvalidArgumentException
     *
     * @return $this
     */
    public function setCacheIdentifier($id)
    {
        if (!is_string($id) && strlen((string) $id) == 0) {
            throw new SynergyInvalidArgumentException('cache id must be a string');
        }

        $this->cache_identifier = (string) $id;

        return $this;
    }


    /**
     * Set the template content to a string value rather than parsing a file
     *
     * The template engine will parse the string as a template
     *
     * @param string $content
     *
     * @return $this
     * @throws SynergyInvalidArgumentException if the content is invalid
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
}
