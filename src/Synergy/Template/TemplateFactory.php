<?php
/**
 * Created by Chris Noden using PhpStorm.
 * 
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
  *
 * @category  Class
 * @package   synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

namespace ChrisNoden\Synergy\Template;

use ChrisNoden\Synergy\Exception\SynergyException;
use ChrisNoden\Synergy\Synergy\Config;
use ChrisNoden\Synergy\Type\TemplateType;

/**
 * Class TemplateFactory
 *
 * @category ChrisNoden\Synergy\Template
 * @package  synergy1
 * @author   Chris Noden <chris.noden@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */
class TemplateFactory
{

    /**
     * Create a new Template object
     *
     * @param TemplateType $type
     * @param string       $filename
     * @param null|string  $template_root_dir
     *
     * @return TemplateInterface
     * @throws SynergyException thrown if the Template class is not valid
     */
    public static function create(TemplateType $type, $filename, $template_root_dir = null)
    {
        $classname = __NAMESPACE__ . '\\' . $type->value();

        $tpl = new $classname();
        if ($tpl instanceof TemplateInterface) {
            if (is_null($template_root_dir)) {
                if (Config::get('synergy:template_dir')) {
                    $template_root_dir = Config::get('synergy:template_dir');
                } else {
                    /**
                     * Set the template root dir to be the folder that the file lives in
                     * and adjust the filename to be relative to that folder
                     */
                    $template_root_dir = dirname($filename);
                    if ($template_root_dir == '.') {
                        $template_root_dir = './';
                    } else {
                        $filename = substr($filename, strlen($template_root_dir));
                    }
                }
            }
            $tpl->setTemplateDir($template_root_dir);
            $tpl->setTemplateFile($filename);
            return $tpl;
        } else {
            throw new SynergyException('TemplateType '.$type->value().' does not implement TemplateInterface');
        }
    }
}
