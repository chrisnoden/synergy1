# Database command list

## Build the Model classes

After updating the schema.xml you need to update the Model classes. The skeleton sub-classes
are NOT updated during this process so as not to remove any customisation you have made.

    $ vendor/bin/propel model:build --config-dir="config/db" --output-dir="src/Model" --schema-dir="config/db" --disable-namespace-auto-package -vv
    

## Create the runtime connection config.php file

    $ vendor/bin/propel config:convert --config-dir="config/db" --output-dir="config/db" -vvv
    
    
# Create the SQL to generate the tables

    $ vendor/bin/propel sql:build --config-dir="config/db" --schema-dir="config/db" --output-dir="config/db/sql" -vv
    
    
# Insert the SQL

    $ vendor/bin/propel sql:insert --config-dir="config/db" --sql-dir="config/db/sql" -vv
