<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  File
 * @package   synergy1
 * @url       https://bitbucket.org/chrisnoden/synergy1
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2014 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */


/**
 * The possible values are here: http://uk.php.net/manual/en/timezones.php
 */
if (!ini_get('date.timezone')) {
    date_default_timezone_set('Europe/London');
}

/**
 * Optional - Set our locale for string/character functions
 * NB This does not work on all systems so you may need to remove it
 */
setlocale(LC_ALL, 'en_GB');


// Load the composer autoloader
require_once('../vendor/autoload.php');

// Register our Error Handling
\ChrisNoden\Synergy\Exception\ErrorHandler::register();

if (class_exists('\Monolog\Logger')) {
    // Create a MonoLog logger to handle all log output (debug or above)
    $processor = new \Monolog\Processor\IntrospectionProcessor();
    $logger = new \Monolog\Logger('my_logger');
    $logger->pushProcessor($processor);
    $logger->pushHandler(new \Monolog\Handler\StreamHandler('../logs/webservice.log', \Monolog\Logger::NOTICE));
} else {
    $logger = new \Psr\Log\NullLogger();
}

// Need to set our App Dir (root of our project)
\ChrisNoden\Synergy\Synergy\Config::addVar('app.dir', dirname(__DIR__));

// Create our Synergy Web Project
$project = \ChrisNoden\Synergy\Synergy\ProjectFactory::create(
    \ChrisNoden\Synergy\Type\ProjectType::WEB_PROJECT(),
    dirname(__DIR__) . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.yml',
    sys_get_temp_dir(),
    $logger
);


// Run the project
$project->run();
