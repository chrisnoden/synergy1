<?php
/**
 * Created by Chris Noden using PhpStorm.
 *
 * PHP version 5
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category  File
 * @package   mse
 * @author    Chris Noden <chris.noden@gmail.com>
 * @copyright 2015 Chris Noden
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 */

if (!ini_get('date.timezone')) {
    date_default_timezone_set('Europe/London');
}

/**
 * Optional - Set our locale for string/character functions
 * NB This does not work on all systems so you may need to remove it
 */
setlocale(LC_ALL, 'en_GB');

// Propel config
if (file_exists(dirname(__DIR__) . '/config/db/config.php')) {
    require_once(dirname(__DIR__) . '/config/db/config.php');
}
