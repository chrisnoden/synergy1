# Synergy v1
==============================

## Lightweight MVC framework for rapid template-driven web deployment

Synergy is an alternative PHP framework intended to promote easy, fast delivery of web services without having to spend weeks learning how it works.

It's not intended as a replacement to the amazingly powerful, popular and difficult to master frameworks like Symfony, Yii or Zend. It does, however, provide some features that those big boys don't offer.

Synergy was borne out of my own frustration at trying to learn one of the above-mentioned big frameworks and at realising that they didn't provide the features I thought were important.

### Highlights
 * Fast, simple and powerful routing
 * * Match on host (www.mysite.com)
 * * Match on path (/foo/bar)
 * * Match SSL or non-SSL scheme
 * * Match HTTP method (GET , HEAD, POST, PUT, DELETE, OPTIONS)
 * * Match on device (mobile, tablet, desktop, console)
 * * Routes can also be used to match console (CLI) commands to Controllers
 * Easily pass options to your controller from the Route config
 * All route parameters that don't match a *Mapping* are made available to your controller
 * Client info is available to your controller (what type of device, browser platform, browser type and version)
 * Use Smarty or Twig templates (or plain old HTML) for your Views
 * Use any Model/ORM of your choice (I like Propel2)
 * Use any PSR-3 logger (or no logger if you're a rebel)
 * Very lightweight codebase compatible with PHP 5.4+
 * Unit-Test driven development


## Config Vars for Substitution

 * %config.dir% : dir that your config.yml file is in
 * %synergy.dir% : dir that the Synergy classes are in


## How To Use

The easy start is to use Composer and use the Synery install scripts. An example composer.json file:

    {
        "name": "myproject/project",
        "type": "project",
        "description": "My Web Project",
        "license": "proprietary",
        "extra":
        {
            "namespace": "Developer\\ProjectNamespace"
        },
        "require": {
            "php": ">=5.4.13",
            "chrisnoden/synergy1": "dev-master",
            "smarty/smarty": "3.1.*",
            "monolog/monolog": "1.7.*"
        },
        "autoload": {
            "psr-4": { "MyProject\\Project\\": "src/" }
        },
        "scripts": {
            "post-update-cmd": "ChrisNoden\\Synergy\\Construct\\Install::postUpdate",
            "post-install-cmd": "ChrisNoden\\Synergy\\Construct\\Install::postInstall"
        }
    }

It's important that you set your namespace in the "namespace" parameter. This helps the Synergy1
install scripts configure the starting environment.

Create a composer.json file in the root directory of your project and then run

    $ composer install

This will fetch all the libraries from the 'require' block of the composer.json and, if all is
successful, will then run the install scripts from Synergy which will configure the directories
and create a test page.

Then you just have to create an apache conf file which links to the web folder at the root of your project

The Synergy1 install scripts create a recommended folder structure for you and create an initial
config file, route file and Controller.
